# antlr-monaco

> :warning: **Warning:** The project is still experimental

Tools to integrate [ANTLR4](https://www.antlr.org/) grammars in the [Monaco Editor](https://microsoft.github.io/monaco-editor/).

## Installation

With NPM:

```
npm install antlr-monaco
```

With Yarn:

```
yarn add antlr-monaco
```

### Dependencies

This library depends on [antlr4ts](https://github.com/tunnelvisionlabs/antlr4ts), [antlr4-c3](https://github.com/mike-lischke/antlr4-c3) and [monaco-editor](https://microsoft.github.io/monaco-editor/) which are not bundled with it.

## Usage

The library has 3 main modules:

### Grammar

The base of the project. It has a completion engine (based on [antlr-c3](https://github.com/mike-lischke/antlr4-c3)) that can be configured to suggest tokens and rules.

### Monaco

The integration of the grammar into the [Monaco Editor](https://microsoft.github.io/monaco-editor/).
These are the implemented components:

- [`CompletionItemProvider`](https://microsoft.github.io/monaco-editor/docs.html#interfaces/languages.CompletionItemProvider.html).

### Test

Some functions to reduce the boilerplate of unit tests.

## Examples

The `examples` folder includes these languages:

- DPM-XL

## Collaboration

## License

Apache 2.0

## Authors
