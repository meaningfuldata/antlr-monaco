import * as monaco from 'monaco-editor'
import { languageId, initializeLanguage } from './'
import './style.css'

initializeLanguage()

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <h1>ANTLR + Monaco</h1>
    <div id="editor">
    </div>
  </div>
`

const $editor = document.querySelector('#editor') as HTMLElement

monaco.editor.create($editor, {
  value: '',
  language: languageId
})
