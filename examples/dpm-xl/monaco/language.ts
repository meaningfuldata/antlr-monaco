import * as monaco from 'monaco-editor'
import { CompletionItemProvider } from '@/monaco/providers/completion-item-provider'
import { DpmXlGrammar } from '../grammar'

export const languageId = 'dpm-xl'

let isInitialized = false

export const initializeLanguage = () => {
  if (!isInitialized) {
    isInitialized = true

    if (typeof Worker === 'undefined') {
      // FIXME: error message
      throw new Error('Web Workers are mandatory')
    }

    // window.MonacoEnvironment = {
    //   getWorker(workerId, label) {
    //     console.log('WORKDER', workerId, label)
    //
    //     switch (label) {
    //       case 'editorWorkerService':
    //         return new Worker(
    //           new URL(
    //             'monaco-editor/esm/vs/editor/editor.worker',
    //             import.meta.url
    //           )
    //         )
    //       case languageId:
    //         // TODO: URL should be a configurable parameter
    //         const worker = new Worker(
    //           new URL(`../../workers/${languageId}.worker`, import.meta.url)
    //         )
    //         // TODO: remove unless necessary
    //         worker.onmessage = ({ data }) => {
    //           console.log(`page got message: ${data}`)
    //         }
    //         worker.postMessage('hello')
    //         return worker
    //       default:
    //         // TODO
    //         throw new Error('TODO')
    //     }
    //   }
    // }

    monaco.languages.onLanguage(languageId, () =>
      monaco.editor.createWebWorker({
        moduleId: `./${languageId}.worker`
      })
    )

    const dpmXlGrammar = new DpmXlGrammar()

    monaco.languages.register({ id: languageId })
    monaco.languages.registerCompletionItemProvider(
      languageId,
      new CompletionItemProvider(dpmXlGrammar)
    )
  }
}
