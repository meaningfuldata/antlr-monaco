import { ScopedSymbol, SymbolTable, Symbol } from 'antlr4-c3'
import { RuleContext } from 'antlr4ts'
import { AbstractParseTreeVisitor } from 'antlr4ts/tree'
import {
  ColElemContext,
  ExprWithoutAssignmentContext,
  RootContext,
  RowArgContext,
  RowElemContext,
  RowHandlerContext,
  StatementContext,
  TableReferenceContext
} from './antlr/DpmXlParser'
import { DpmXlParserVisitor } from './antlr/DpmXlParserVisitor'

export class RootSymbol extends ScopedSymbol { }
export class ExpressionSymbol extends ScopedSymbol { }
export class SelectionSymbol extends ScopedSymbol { }

export class CellSymbol extends Symbol {
  constructor(public name: string, public context: RuleContext) {
    super(name)
  }
}

const SYMBOL_TABLE_NAME = 'dpm-xl'

/**
 * Visitor specialised to build the symbol table.
 */
export class DpmXlSymbolTableVisitor
  extends AbstractParseTreeVisitor<SymbolTable>
  implements DpmXlParserVisitor<SymbolTable>
{
  // The scope allows child symbols
  protected scope!: ScopedSymbol

  constructor(
    protected readonly symbolTable = new SymbolTable(SYMBOL_TABLE_NAME, {})
  ) {
    super()
  }

  protected defaultResult(): SymbolTable {
    return this.symbolTable
  }

  visitRoot(context: RootContext): SymbolTable {
    this.scope = this.symbolTable.addNewSymbolOfType(
      RootSymbol,
      undefined,
      'root',
      context
    )
    return this.visitChildren(context)
  }

  visitStatement(context: StatementContext): SymbolTable {
    this.scope = this.symbolTable.addNewSymbolOfType(
      ExpressionSymbol,
      this.scope,
      'statement',
      context
    )
    return this.visitChildren(context)
  }

  visitExprWithoutAssignment(
    context: ExprWithoutAssignmentContext
  ): SymbolTable {
    this.scope = this.symbolTable.addNewSymbolOfType(
      ExpressionSymbol,
      this.scope,
      'exprWithoutAssignmentContext',
      context
    )
    return this.visitChildren(context)
  }

  visitRowArg(context: RowArgContext): SymbolTable {
    this.symbolTable.addNewSymbolOfType(
      SelectionSymbol,
      this.scope,
      'rowArg',
      context
    )
    return this.visitChildren(context)
  }

  visitRowHandler(context: RowHandlerContext): SymbolTable {
    this.symbolTable.addNewSymbolOfType(
      SelectionSymbol,
      this.scope,
      'rowHandler',
      context
    )
    return this.visitChildren(context)
  }

  visitTableReference(context: TableReferenceContext): SymbolTable {
    this.symbolTable.addNewSymbolOfType(
      CellSymbol,
      this.scope,
      'tableReference',
      context
    )
    return this.visitChildren(context)
  }

  visitColElem(context: ColElemContext): SymbolTable {
    this.symbolTable.addNewSymbolOfType(
      CellSymbol,
      this.scope,
      'colElem',
      context
    )
    return this.visitChildren(context)
  }

  visitRowElem(context: RowElemContext): SymbolTable {
    this.symbolTable.addNewSymbolOfType(
      CellSymbol,
      this.scope,
      'rowElem',
      context
    )
    return this.visitChildren(context)
  }
}
