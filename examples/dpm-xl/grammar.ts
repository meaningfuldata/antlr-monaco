import { DpmXlLexer } from './antlr/DpmXlLexer'
import { DpmXlParser } from './antlr/DpmXlParser'
import { Grammar } from '@/grammar/'
import { DpmXlSymbolTableVisitor } from './symbol-table-visitor'

export class DpmXlGrammar extends Grammar {
  Lexer = DpmXlLexer
  Parser = DpmXlParser
  Visitor = DpmXlSymbolTableVisitor
}
