// Generated from ./examples/dpm-xl/antlr/DpmXlParser.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";

import { RowArgContext } from "./DpmXlParser";
import { ColArgContext } from "./DpmXlParser";
import { SheetArgContext } from "./DpmXlParser";
import { IntervalArgContext } from "./DpmXlParser";
import { DefaultArgContext } from "./DpmXlParser";
import { AggregateFunctionsContext } from "./DpmXlParser";
import { NumericFunctionsContext } from "./DpmXlParser";
import { ComparisonFunctionsContext } from "./DpmXlParser";
import { FilterFunctionsContext } from "./DpmXlParser";
import { ConditionalFunctionsContext } from "./DpmXlParser";
import { TimeFunctionsContext } from "./DpmXlParser";
import { StringFunctionsContext } from "./DpmXlParser";
import { MatchExprContext } from "./DpmXlParser";
import { IsnullExprContext } from "./DpmXlParser";
import { UnaryStringFunctionContext } from "./DpmXlParser";
import { TimeShiftFunctionContext } from "./DpmXlParser";
import { WhereExprContext } from "./DpmXlParser";
import { GetExprContext } from "./DpmXlParser";
import { RenameExprContext } from "./DpmXlParser";
import { TableRefContext } from "./DpmXlParser";
import { CompRefContext } from "./DpmXlParser";
import { UnaryNumericFunctionsContext } from "./DpmXlParser";
import { BinaryNumericFunctionsContext } from "./DpmXlParser";
import { ComplexNumericFunctionsContext } from "./DpmXlParser";
import { ExprWithoutPartialSelectionContext } from "./DpmXlParser";
import { ExprWithSelectionContext } from "./DpmXlParser";
import { ExprWithoutAssignmentContext } from "./DpmXlParser";
import { AssignmentExprContext } from "./DpmXlParser";
import { ParExprContext } from "./DpmXlParser";
import { FuncExprContext } from "./DpmXlParser";
import { ClauseExprContext } from "./DpmXlParser";
import { UnaryExprContext } from "./DpmXlParser";
import { NotExprContext } from "./DpmXlParser";
import { NumericExprContext } from "./DpmXlParser";
import { ConcatExprContext } from "./DpmXlParser";
import { CompExprContext } from "./DpmXlParser";
import { InExprContext } from "./DpmXlParser";
import { BoolExprContext } from "./DpmXlParser";
import { IfExprContext } from "./DpmXlParser";
import { ItemReferenceExprContext } from "./DpmXlParser";
import { PropertyReferenceExprContext } from "./DpmXlParser";
import { KeyNamesExprContext } from "./DpmXlParser";
import { LiteralExprContext } from "./DpmXlParser";
import { SelectExprContext } from "./DpmXlParser";
import { NvlFunctionContext } from "./DpmXlParser";
import { CommonAggrOpContext } from "./DpmXlParser";
import { RootContext } from "./DpmXlParser";
import { StatementsContext } from "./DpmXlParser";
import { StatementContext } from "./DpmXlParser";
import { PersistentExpressionContext } from "./DpmXlParser";
import { ExpressionWithoutAssignmentContext } from "./DpmXlParser";
import { SelectionStartContext } from "./DpmXlParser";
import { SelectionEndContext } from "./DpmXlParser";
import { PartialSelectionContext } from "./DpmXlParser";
import { TemporaryAssignmentExpressionContext } from "./DpmXlParser";
import { PersistentAssignmentExpressionContext } from "./DpmXlParser";
import { ExpressionContext } from "./DpmXlParser";
import { SetOperandContext } from "./DpmXlParser";
import { SetElementsContext } from "./DpmXlParser";
import { FunctionsContext } from "./DpmXlParser";
import { NumericOperatorsContext } from "./DpmXlParser";
import { ComparisonFunctionOperatorsContext } from "./DpmXlParser";
import { FilterOperatorsContext } from "./DpmXlParser";
import { TimeOperatorsContext } from "./DpmXlParser";
import { ConditionalOperatorsContext } from "./DpmXlParser";
import { StringOperatorsContext } from "./DpmXlParser";
import { AggregateOperatorsContext } from "./DpmXlParser";
import { GroupingClauseContext } from "./DpmXlParser";
import { ItemSignatureContext } from "./DpmXlParser";
import { ItemReferenceContext } from "./DpmXlParser";
import { ColElemContext } from "./DpmXlParser";
import { SheetElemContext } from "./DpmXlParser";
import { RowElemSingleContext } from "./DpmXlParser";
import { RowElemRangeContext } from "./DpmXlParser";
import { RowElemAllContext } from "./DpmXlParser";
import { RowElemContext } from "./DpmXlParser";
import { RowHandlerContext } from "./DpmXlParser";
import { ColHandlerContext } from "./DpmXlParser";
import { SheetHandlerContext } from "./DpmXlParser";
import { IntervalContext } from "./DpmXlParser";
import { DefaultWithLiteralContext } from "./DpmXlParser";
import { ArgumentContext } from "./DpmXlParser";
import { SelectContext } from "./DpmXlParser";
import { SelectOperandContext } from "./DpmXlParser";
import { VarIDContext } from "./DpmXlParser";
import { CellRefContext } from "./DpmXlParser";
import { PreconditionElemContext } from "./DpmXlParser";
import { VarRefContext } from "./DpmXlParser";
import { OperationRefContext } from "./DpmXlParser";
import { CellAddressContext } from "./DpmXlParser";
import { TableReferenceSingleContext } from "./DpmXlParser";
import { TableGroupReferenceContext } from "./DpmXlParser";
import { TableReferenceContext } from "./DpmXlParser";
import { ClauseOperatorsContext } from "./DpmXlParser";
import { RenameClauseContext } from "./DpmXlParser";
import { ComparisonOperatorsContext } from "./DpmXlParser";
import { LiteralContext } from "./DpmXlParser";
import { KeyNamesContext } from "./DpmXlParser";
import { PropertyReferenceContext } from "./DpmXlParser";
import { PropertyCodeContext } from "./DpmXlParser";
import { TemporaryIdentifierContext } from "./DpmXlParser";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `DpmXlParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface DpmXlParserVisitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by the `rowArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowArg?: (ctx: RowArgContext) => Result;

	/**
	 * Visit a parse tree produced by the `colArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitColArg?: (ctx: ColArgContext) => Result;

	/**
	 * Visit a parse tree produced by the `sheetArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSheetArg?: (ctx: SheetArgContext) => Result;

	/**
	 * Visit a parse tree produced by the `intervalArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIntervalArg?: (ctx: IntervalArgContext) => Result;

	/**
	 * Visit a parse tree produced by the `defaultArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDefaultArg?: (ctx: DefaultArgContext) => Result;

	/**
	 * Visit a parse tree produced by the `aggregateFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAggregateFunctions?: (ctx: AggregateFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `numericFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNumericFunctions?: (ctx: NumericFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `comparisonFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComparisonFunctions?: (ctx: ComparisonFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `filterFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFilterFunctions?: (ctx: FilterFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `conditionalFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitConditionalFunctions?: (ctx: ConditionalFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `timeFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTimeFunctions?: (ctx: TimeFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `stringFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStringFunctions?: (ctx: StringFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `matchExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMatchExpr?: (ctx: MatchExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `isnullExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIsnullExpr?: (ctx: IsnullExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `unaryStringFunction`
	 * labeled alternative in `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUnaryStringFunction?: (ctx: UnaryStringFunctionContext) => Result;

	/**
	 * Visit a parse tree produced by the `timeShiftFunction`
	 * labeled alternative in `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTimeShiftFunction?: (ctx: TimeShiftFunctionContext) => Result;

	/**
	 * Visit a parse tree produced by the `whereExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitWhereExpr?: (ctx: WhereExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `getExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGetExpr?: (ctx: GetExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `renameExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRenameExpr?: (ctx: RenameExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `tableRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableRef?: (ctx: TableRefContext) => Result;

	/**
	 * Visit a parse tree produced by the `compRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCompRef?: (ctx: CompRefContext) => Result;

	/**
	 * Visit a parse tree produced by the `unaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUnaryNumericFunctions?: (ctx: UnaryNumericFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `binaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBinaryNumericFunctions?: (ctx: BinaryNumericFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `complexNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComplexNumericFunctions?: (ctx: ComplexNumericFunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by the `exprWithoutPartialSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExprWithoutPartialSelection?: (ctx: ExprWithoutPartialSelectionContext) => Result;

	/**
	 * Visit a parse tree produced by the `exprWithSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExprWithSelection?: (ctx: ExprWithSelectionContext) => Result;

	/**
	 * Visit a parse tree produced by the `exprWithoutAssignment`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExprWithoutAssignment?: (ctx: ExprWithoutAssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by the `assignmentExpr`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssignmentExpr?: (ctx: AssignmentExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `parExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParExpr?: (ctx: ParExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `funcExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFuncExpr?: (ctx: FuncExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `clauseExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitClauseExpr?: (ctx: ClauseExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `unaryExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUnaryExpr?: (ctx: UnaryExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `notExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNotExpr?: (ctx: NotExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `numericExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNumericExpr?: (ctx: NumericExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `concatExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitConcatExpr?: (ctx: ConcatExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `compExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCompExpr?: (ctx: CompExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `inExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInExpr?: (ctx: InExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `boolExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBoolExpr?: (ctx: BoolExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `ifExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIfExpr?: (ctx: IfExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `itemReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitItemReferenceExpr?: (ctx: ItemReferenceExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `propertyReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPropertyReferenceExpr?: (ctx: PropertyReferenceExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `keyNamesExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitKeyNamesExpr?: (ctx: KeyNamesExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `literalExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLiteralExpr?: (ctx: LiteralExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `selectExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSelectExpr?: (ctx: SelectExprContext) => Result;

	/**
	 * Visit a parse tree produced by the `nvlFunction`
	 * labeled alternative in `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNvlFunction?: (ctx: NvlFunctionContext) => Result;

	/**
	 * Visit a parse tree produced by the `commonAggrOp`
	 * labeled alternative in `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommonAggrOp?: (ctx: CommonAggrOpContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.root`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRoot?: (ctx: RootContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.statements`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStatements?: (ctx: StatementsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStatement?: (ctx: StatementContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.persistentExpression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPersistentExpression?: (ctx: PersistentExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpressionWithoutAssignment?: (ctx: ExpressionWithoutAssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.selectionStart`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSelectionStart?: (ctx: SelectionStartContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.selectionEnd`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSelectionEnd?: (ctx: SelectionEndContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.partialSelection`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartialSelection?: (ctx: PartialSelectionContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.temporaryAssignmentExpression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemporaryAssignmentExpression?: (ctx: TemporaryAssignmentExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.persistentAssignmentExpression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPersistentAssignmentExpression?: (ctx: PersistentAssignmentExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpression?: (ctx: ExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.setOperand`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSetOperand?: (ctx: SetOperandContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.setElements`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSetElements?: (ctx: SetElementsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFunctions?: (ctx: FunctionsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNumericOperators?: (ctx: NumericOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComparisonFunctionOperators?: (ctx: ComparisonFunctionOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.filterOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFilterOperators?: (ctx: FilterOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTimeOperators?: (ctx: TimeOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitConditionalOperators?: (ctx: ConditionalOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStringOperators?: (ctx: StringOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAggregateOperators?: (ctx: AggregateOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.groupingClause`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGroupingClause?: (ctx: GroupingClauseContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.itemSignature`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitItemSignature?: (ctx: ItemSignatureContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.itemReference`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitItemReference?: (ctx: ItemReferenceContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.colElem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitColElem?: (ctx: ColElemContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.sheetElem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSheetElem?: (ctx: SheetElemContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.rowElemSingle`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowElemSingle?: (ctx: RowElemSingleContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.rowElemRange`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowElemRange?: (ctx: RowElemRangeContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.rowElemAll`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowElemAll?: (ctx: RowElemAllContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.rowElem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowElem?: (ctx: RowElemContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.rowHandler`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRowHandler?: (ctx: RowHandlerContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.colHandler`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitColHandler?: (ctx: ColHandlerContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.sheetHandler`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSheetHandler?: (ctx: SheetHandlerContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.interval`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterval?: (ctx: IntervalContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.defaultWithLiteral`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDefaultWithLiteral?: (ctx: DefaultWithLiteralContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitArgument?: (ctx: ArgumentContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.select`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSelect?: (ctx: SelectContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.selectOperand`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSelectOperand?: (ctx: SelectOperandContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.varID`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarID?: (ctx: VarIDContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.cellRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCellRef?: (ctx: CellRefContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.preconditionElem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPreconditionElem?: (ctx: PreconditionElemContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.varRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarRef?: (ctx: VarRefContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.operationRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitOperationRef?: (ctx: OperationRefContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCellAddress?: (ctx: CellAddressContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.tableReferenceSingle`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableReferenceSingle?: (ctx: TableReferenceSingleContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.tableGroupReference`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableGroupReference?: (ctx: TableGroupReferenceContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.tableReference`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableReference?: (ctx: TableReferenceContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitClauseOperators?: (ctx: ClauseOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.renameClause`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRenameClause?: (ctx: RenameClauseContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.comparisonOperators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComparisonOperators?: (ctx: ComparisonOperatorsContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.literal`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLiteral?: (ctx: LiteralContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.keyNames`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitKeyNames?: (ctx: KeyNamesContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.propertyReference`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPropertyReference?: (ctx: PropertyReferenceContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.propertyCode`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPropertyCode?: (ctx: PropertyCodeContext) => Result;

	/**
	 * Visit a parse tree produced by `DpmXlParser.temporaryIdentifier`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemporaryIdentifier?: (ctx: TemporaryIdentifierContext) => Result;
}

