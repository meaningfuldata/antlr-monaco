// Generated from ./examples/dpm-xl/antlr/DpmXlParser.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";

import { RowArgContext } from "./DpmXlParser";
import { ColArgContext } from "./DpmXlParser";
import { SheetArgContext } from "./DpmXlParser";
import { IntervalArgContext } from "./DpmXlParser";
import { DefaultArgContext } from "./DpmXlParser";
import { AggregateFunctionsContext } from "./DpmXlParser";
import { NumericFunctionsContext } from "./DpmXlParser";
import { ComparisonFunctionsContext } from "./DpmXlParser";
import { FilterFunctionsContext } from "./DpmXlParser";
import { ConditionalFunctionsContext } from "./DpmXlParser";
import { TimeFunctionsContext } from "./DpmXlParser";
import { StringFunctionsContext } from "./DpmXlParser";
import { MatchExprContext } from "./DpmXlParser";
import { IsnullExprContext } from "./DpmXlParser";
import { UnaryStringFunctionContext } from "./DpmXlParser";
import { TimeShiftFunctionContext } from "./DpmXlParser";
import { WhereExprContext } from "./DpmXlParser";
import { GetExprContext } from "./DpmXlParser";
import { RenameExprContext } from "./DpmXlParser";
import { TableRefContext } from "./DpmXlParser";
import { CompRefContext } from "./DpmXlParser";
import { UnaryNumericFunctionsContext } from "./DpmXlParser";
import { BinaryNumericFunctionsContext } from "./DpmXlParser";
import { ComplexNumericFunctionsContext } from "./DpmXlParser";
import { ExprWithoutPartialSelectionContext } from "./DpmXlParser";
import { ExprWithSelectionContext } from "./DpmXlParser";
import { ExprWithoutAssignmentContext } from "./DpmXlParser";
import { AssignmentExprContext } from "./DpmXlParser";
import { ParExprContext } from "./DpmXlParser";
import { FuncExprContext } from "./DpmXlParser";
import { ClauseExprContext } from "./DpmXlParser";
import { UnaryExprContext } from "./DpmXlParser";
import { NotExprContext } from "./DpmXlParser";
import { NumericExprContext } from "./DpmXlParser";
import { ConcatExprContext } from "./DpmXlParser";
import { CompExprContext } from "./DpmXlParser";
import { InExprContext } from "./DpmXlParser";
import { BoolExprContext } from "./DpmXlParser";
import { IfExprContext } from "./DpmXlParser";
import { ItemReferenceExprContext } from "./DpmXlParser";
import { PropertyReferenceExprContext } from "./DpmXlParser";
import { KeyNamesExprContext } from "./DpmXlParser";
import { LiteralExprContext } from "./DpmXlParser";
import { SelectExprContext } from "./DpmXlParser";
import { NvlFunctionContext } from "./DpmXlParser";
import { CommonAggrOpContext } from "./DpmXlParser";
import { RootContext } from "./DpmXlParser";
import { StatementsContext } from "./DpmXlParser";
import { StatementContext } from "./DpmXlParser";
import { PersistentExpressionContext } from "./DpmXlParser";
import { ExpressionWithoutAssignmentContext } from "./DpmXlParser";
import { SelectionStartContext } from "./DpmXlParser";
import { SelectionEndContext } from "./DpmXlParser";
import { PartialSelectionContext } from "./DpmXlParser";
import { TemporaryAssignmentExpressionContext } from "./DpmXlParser";
import { PersistentAssignmentExpressionContext } from "./DpmXlParser";
import { ExpressionContext } from "./DpmXlParser";
import { SetOperandContext } from "./DpmXlParser";
import { SetElementsContext } from "./DpmXlParser";
import { FunctionsContext } from "./DpmXlParser";
import { NumericOperatorsContext } from "./DpmXlParser";
import { ComparisonFunctionOperatorsContext } from "./DpmXlParser";
import { FilterOperatorsContext } from "./DpmXlParser";
import { TimeOperatorsContext } from "./DpmXlParser";
import { ConditionalOperatorsContext } from "./DpmXlParser";
import { StringOperatorsContext } from "./DpmXlParser";
import { AggregateOperatorsContext } from "./DpmXlParser";
import { GroupingClauseContext } from "./DpmXlParser";
import { ItemSignatureContext } from "./DpmXlParser";
import { ItemReferenceContext } from "./DpmXlParser";
import { ColElemContext } from "./DpmXlParser";
import { SheetElemContext } from "./DpmXlParser";
import { RowElemSingleContext } from "./DpmXlParser";
import { RowElemRangeContext } from "./DpmXlParser";
import { RowElemAllContext } from "./DpmXlParser";
import { RowElemContext } from "./DpmXlParser";
import { RowHandlerContext } from "./DpmXlParser";
import { ColHandlerContext } from "./DpmXlParser";
import { SheetHandlerContext } from "./DpmXlParser";
import { IntervalContext } from "./DpmXlParser";
import { DefaultWithLiteralContext } from "./DpmXlParser";
import { ArgumentContext } from "./DpmXlParser";
import { SelectContext } from "./DpmXlParser";
import { SelectOperandContext } from "./DpmXlParser";
import { VarIDContext } from "./DpmXlParser";
import { CellRefContext } from "./DpmXlParser";
import { PreconditionElemContext } from "./DpmXlParser";
import { VarRefContext } from "./DpmXlParser";
import { OperationRefContext } from "./DpmXlParser";
import { CellAddressContext } from "./DpmXlParser";
import { TableReferenceSingleContext } from "./DpmXlParser";
import { TableGroupReferenceContext } from "./DpmXlParser";
import { TableReferenceContext } from "./DpmXlParser";
import { ClauseOperatorsContext } from "./DpmXlParser";
import { RenameClauseContext } from "./DpmXlParser";
import { ComparisonOperatorsContext } from "./DpmXlParser";
import { LiteralContext } from "./DpmXlParser";
import { KeyNamesContext } from "./DpmXlParser";
import { PropertyReferenceContext } from "./DpmXlParser";
import { PropertyCodeContext } from "./DpmXlParser";
import { TemporaryIdentifierContext } from "./DpmXlParser";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `DpmXlParser`.
 */
export interface DpmXlParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the `rowArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterRowArg?: (ctx: RowArgContext) => void;
	/**
	 * Exit a parse tree produced by the `rowArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitRowArg?: (ctx: RowArgContext) => void;

	/**
	 * Enter a parse tree produced by the `colArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterColArg?: (ctx: ColArgContext) => void;
	/**
	 * Exit a parse tree produced by the `colArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitColArg?: (ctx: ColArgContext) => void;

	/**
	 * Enter a parse tree produced by the `sheetArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterSheetArg?: (ctx: SheetArgContext) => void;
	/**
	 * Exit a parse tree produced by the `sheetArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitSheetArg?: (ctx: SheetArgContext) => void;

	/**
	 * Enter a parse tree produced by the `intervalArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterIntervalArg?: (ctx: IntervalArgContext) => void;
	/**
	 * Exit a parse tree produced by the `intervalArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitIntervalArg?: (ctx: IntervalArgContext) => void;

	/**
	 * Enter a parse tree produced by the `defaultArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterDefaultArg?: (ctx: DefaultArgContext) => void;
	/**
	 * Exit a parse tree produced by the `defaultArg`
	 * labeled alternative in `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitDefaultArg?: (ctx: DefaultArgContext) => void;

	/**
	 * Enter a parse tree produced by the `aggregateFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterAggregateFunctions?: (ctx: AggregateFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `aggregateFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitAggregateFunctions?: (ctx: AggregateFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `numericFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterNumericFunctions?: (ctx: NumericFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `numericFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitNumericFunctions?: (ctx: NumericFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `comparisonFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterComparisonFunctions?: (ctx: ComparisonFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `comparisonFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitComparisonFunctions?: (ctx: ComparisonFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `filterFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterFilterFunctions?: (ctx: FilterFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `filterFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitFilterFunctions?: (ctx: FilterFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `conditionalFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterConditionalFunctions?: (ctx: ConditionalFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `conditionalFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitConditionalFunctions?: (ctx: ConditionalFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `timeFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterTimeFunctions?: (ctx: TimeFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `timeFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitTimeFunctions?: (ctx: TimeFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `stringFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterStringFunctions?: (ctx: StringFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `stringFunctions`
	 * labeled alternative in `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitStringFunctions?: (ctx: StringFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `matchExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	enterMatchExpr?: (ctx: MatchExprContext) => void;
	/**
	 * Exit a parse tree produced by the `matchExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	exitMatchExpr?: (ctx: MatchExprContext) => void;

	/**
	 * Enter a parse tree produced by the `isnullExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	enterIsnullExpr?: (ctx: IsnullExprContext) => void;
	/**
	 * Exit a parse tree produced by the `isnullExpr`
	 * labeled alternative in `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	exitIsnullExpr?: (ctx: IsnullExprContext) => void;

	/**
	 * Enter a parse tree produced by the `unaryStringFunction`
	 * labeled alternative in `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 */
	enterUnaryStringFunction?: (ctx: UnaryStringFunctionContext) => void;
	/**
	 * Exit a parse tree produced by the `unaryStringFunction`
	 * labeled alternative in `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 */
	exitUnaryStringFunction?: (ctx: UnaryStringFunctionContext) => void;

	/**
	 * Enter a parse tree produced by the `timeShiftFunction`
	 * labeled alternative in `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 */
	enterTimeShiftFunction?: (ctx: TimeShiftFunctionContext) => void;
	/**
	 * Exit a parse tree produced by the `timeShiftFunction`
	 * labeled alternative in `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 */
	exitTimeShiftFunction?: (ctx: TimeShiftFunctionContext) => void;

	/**
	 * Enter a parse tree produced by the `whereExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	enterWhereExpr?: (ctx: WhereExprContext) => void;
	/**
	 * Exit a parse tree produced by the `whereExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	exitWhereExpr?: (ctx: WhereExprContext) => void;

	/**
	 * Enter a parse tree produced by the `getExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	enterGetExpr?: (ctx: GetExprContext) => void;
	/**
	 * Exit a parse tree produced by the `getExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	exitGetExpr?: (ctx: GetExprContext) => void;

	/**
	 * Enter a parse tree produced by the `renameExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	enterRenameExpr?: (ctx: RenameExprContext) => void;
	/**
	 * Exit a parse tree produced by the `renameExpr`
	 * labeled alternative in `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	exitRenameExpr?: (ctx: RenameExprContext) => void;

	/**
	 * Enter a parse tree produced by the `tableRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	enterTableRef?: (ctx: TableRefContext) => void;
	/**
	 * Exit a parse tree produced by the `tableRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	exitTableRef?: (ctx: TableRefContext) => void;

	/**
	 * Enter a parse tree produced by the `compRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	enterCompRef?: (ctx: CompRefContext) => void;
	/**
	 * Exit a parse tree produced by the `compRef`
	 * labeled alternative in `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	exitCompRef?: (ctx: CompRefContext) => void;

	/**
	 * Enter a parse tree produced by the `unaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	enterUnaryNumericFunctions?: (ctx: UnaryNumericFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `unaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	exitUnaryNumericFunctions?: (ctx: UnaryNumericFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `binaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	enterBinaryNumericFunctions?: (ctx: BinaryNumericFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `binaryNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	exitBinaryNumericFunctions?: (ctx: BinaryNumericFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `complexNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	enterComplexNumericFunctions?: (ctx: ComplexNumericFunctionsContext) => void;
	/**
	 * Exit a parse tree produced by the `complexNumericFunctions`
	 * labeled alternative in `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	exitComplexNumericFunctions?: (ctx: ComplexNumericFunctionsContext) => void;

	/**
	 * Enter a parse tree produced by the `exprWithoutPartialSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	enterExprWithoutPartialSelection?: (ctx: ExprWithoutPartialSelectionContext) => void;
	/**
	 * Exit a parse tree produced by the `exprWithoutPartialSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	exitExprWithoutPartialSelection?: (ctx: ExprWithoutPartialSelectionContext) => void;

	/**
	 * Enter a parse tree produced by the `exprWithSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	enterExprWithSelection?: (ctx: ExprWithSelectionContext) => void;
	/**
	 * Exit a parse tree produced by the `exprWithSelection`
	 * labeled alternative in `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	exitExprWithSelection?: (ctx: ExprWithSelectionContext) => void;

	/**
	 * Enter a parse tree produced by the `exprWithoutAssignment`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	enterExprWithoutAssignment?: (ctx: ExprWithoutAssignmentContext) => void;
	/**
	 * Exit a parse tree produced by the `exprWithoutAssignment`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	exitExprWithoutAssignment?: (ctx: ExprWithoutAssignmentContext) => void;

	/**
	 * Enter a parse tree produced by the `assignmentExpr`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	enterAssignmentExpr?: (ctx: AssignmentExprContext) => void;
	/**
	 * Exit a parse tree produced by the `assignmentExpr`
	 * labeled alternative in `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	exitAssignmentExpr?: (ctx: AssignmentExprContext) => void;

	/**
	 * Enter a parse tree produced by the `parExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterParExpr?: (ctx: ParExprContext) => void;
	/**
	 * Exit a parse tree produced by the `parExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitParExpr?: (ctx: ParExprContext) => void;

	/**
	 * Enter a parse tree produced by the `funcExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterFuncExpr?: (ctx: FuncExprContext) => void;
	/**
	 * Exit a parse tree produced by the `funcExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitFuncExpr?: (ctx: FuncExprContext) => void;

	/**
	 * Enter a parse tree produced by the `clauseExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterClauseExpr?: (ctx: ClauseExprContext) => void;
	/**
	 * Exit a parse tree produced by the `clauseExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitClauseExpr?: (ctx: ClauseExprContext) => void;

	/**
	 * Enter a parse tree produced by the `unaryExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterUnaryExpr?: (ctx: UnaryExprContext) => void;
	/**
	 * Exit a parse tree produced by the `unaryExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitUnaryExpr?: (ctx: UnaryExprContext) => void;

	/**
	 * Enter a parse tree produced by the `notExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterNotExpr?: (ctx: NotExprContext) => void;
	/**
	 * Exit a parse tree produced by the `notExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitNotExpr?: (ctx: NotExprContext) => void;

	/**
	 * Enter a parse tree produced by the `numericExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterNumericExpr?: (ctx: NumericExprContext) => void;
	/**
	 * Exit a parse tree produced by the `numericExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitNumericExpr?: (ctx: NumericExprContext) => void;

	/**
	 * Enter a parse tree produced by the `concatExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterConcatExpr?: (ctx: ConcatExprContext) => void;
	/**
	 * Exit a parse tree produced by the `concatExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitConcatExpr?: (ctx: ConcatExprContext) => void;

	/**
	 * Enter a parse tree produced by the `compExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterCompExpr?: (ctx: CompExprContext) => void;
	/**
	 * Exit a parse tree produced by the `compExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitCompExpr?: (ctx: CompExprContext) => void;

	/**
	 * Enter a parse tree produced by the `inExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterInExpr?: (ctx: InExprContext) => void;
	/**
	 * Exit a parse tree produced by the `inExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitInExpr?: (ctx: InExprContext) => void;

	/**
	 * Enter a parse tree produced by the `boolExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterBoolExpr?: (ctx: BoolExprContext) => void;
	/**
	 * Exit a parse tree produced by the `boolExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitBoolExpr?: (ctx: BoolExprContext) => void;

	/**
	 * Enter a parse tree produced by the `ifExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterIfExpr?: (ctx: IfExprContext) => void;
	/**
	 * Exit a parse tree produced by the `ifExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitIfExpr?: (ctx: IfExprContext) => void;

	/**
	 * Enter a parse tree produced by the `itemReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterItemReferenceExpr?: (ctx: ItemReferenceExprContext) => void;
	/**
	 * Exit a parse tree produced by the `itemReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitItemReferenceExpr?: (ctx: ItemReferenceExprContext) => void;

	/**
	 * Enter a parse tree produced by the `propertyReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterPropertyReferenceExpr?: (ctx: PropertyReferenceExprContext) => void;
	/**
	 * Exit a parse tree produced by the `propertyReferenceExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitPropertyReferenceExpr?: (ctx: PropertyReferenceExprContext) => void;

	/**
	 * Enter a parse tree produced by the `keyNamesExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterKeyNamesExpr?: (ctx: KeyNamesExprContext) => void;
	/**
	 * Exit a parse tree produced by the `keyNamesExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitKeyNamesExpr?: (ctx: KeyNamesExprContext) => void;

	/**
	 * Enter a parse tree produced by the `literalExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterLiteralExpr?: (ctx: LiteralExprContext) => void;
	/**
	 * Exit a parse tree produced by the `literalExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitLiteralExpr?: (ctx: LiteralExprContext) => void;

	/**
	 * Enter a parse tree produced by the `selectExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterSelectExpr?: (ctx: SelectExprContext) => void;
	/**
	 * Exit a parse tree produced by the `selectExpr`
	 * labeled alternative in `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitSelectExpr?: (ctx: SelectExprContext) => void;

	/**
	 * Enter a parse tree produced by the `nvlFunction`
	 * labeled alternative in `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 */
	enterNvlFunction?: (ctx: NvlFunctionContext) => void;
	/**
	 * Exit a parse tree produced by the `nvlFunction`
	 * labeled alternative in `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 */
	exitNvlFunction?: (ctx: NvlFunctionContext) => void;

	/**
	 * Enter a parse tree produced by the `commonAggrOp`
	 * labeled alternative in `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 */
	enterCommonAggrOp?: (ctx: CommonAggrOpContext) => void;
	/**
	 * Exit a parse tree produced by the `commonAggrOp`
	 * labeled alternative in `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 */
	exitCommonAggrOp?: (ctx: CommonAggrOpContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.root`.
	 * @param ctx the parse tree
	 */
	enterRoot?: (ctx: RootContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.root`.
	 * @param ctx the parse tree
	 */
	exitRoot?: (ctx: RootContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.statements`.
	 * @param ctx the parse tree
	 */
	enterStatements?: (ctx: StatementsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.statements`.
	 * @param ctx the parse tree
	 */
	exitStatements?: (ctx: StatementsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	enterStatement?: (ctx: StatementContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.statement`.
	 * @param ctx the parse tree
	 */
	exitStatement?: (ctx: StatementContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.persistentExpression`.
	 * @param ctx the parse tree
	 */
	enterPersistentExpression?: (ctx: PersistentExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.persistentExpression`.
	 * @param ctx the parse tree
	 */
	exitPersistentExpression?: (ctx: PersistentExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	enterExpressionWithoutAssignment?: (ctx: ExpressionWithoutAssignmentContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.expressionWithoutAssignment`.
	 * @param ctx the parse tree
	 */
	exitExpressionWithoutAssignment?: (ctx: ExpressionWithoutAssignmentContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.selectionStart`.
	 * @param ctx the parse tree
	 */
	enterSelectionStart?: (ctx: SelectionStartContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.selectionStart`.
	 * @param ctx the parse tree
	 */
	exitSelectionStart?: (ctx: SelectionStartContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.selectionEnd`.
	 * @param ctx the parse tree
	 */
	enterSelectionEnd?: (ctx: SelectionEndContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.selectionEnd`.
	 * @param ctx the parse tree
	 */
	exitSelectionEnd?: (ctx: SelectionEndContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.partialSelection`.
	 * @param ctx the parse tree
	 */
	enterPartialSelection?: (ctx: PartialSelectionContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.partialSelection`.
	 * @param ctx the parse tree
	 */
	exitPartialSelection?: (ctx: PartialSelectionContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.temporaryAssignmentExpression`.
	 * @param ctx the parse tree
	 */
	enterTemporaryAssignmentExpression?: (ctx: TemporaryAssignmentExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.temporaryAssignmentExpression`.
	 * @param ctx the parse tree
	 */
	exitTemporaryAssignmentExpression?: (ctx: TemporaryAssignmentExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.persistentAssignmentExpression`.
	 * @param ctx the parse tree
	 */
	enterPersistentAssignmentExpression?: (ctx: PersistentAssignmentExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.persistentAssignmentExpression`.
	 * @param ctx the parse tree
	 */
	exitPersistentAssignmentExpression?: (ctx: PersistentAssignmentExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	enterExpression?: (ctx: ExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.expression`.
	 * @param ctx the parse tree
	 */
	exitExpression?: (ctx: ExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.setOperand`.
	 * @param ctx the parse tree
	 */
	enterSetOperand?: (ctx: SetOperandContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.setOperand`.
	 * @param ctx the parse tree
	 */
	exitSetOperand?: (ctx: SetOperandContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.setElements`.
	 * @param ctx the parse tree
	 */
	enterSetElements?: (ctx: SetElementsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.setElements`.
	 * @param ctx the parse tree
	 */
	exitSetElements?: (ctx: SetElementsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	enterFunctions?: (ctx: FunctionsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.functions`.
	 * @param ctx the parse tree
	 */
	exitFunctions?: (ctx: FunctionsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	enterNumericOperators?: (ctx: NumericOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.numericOperators`.
	 * @param ctx the parse tree
	 */
	exitNumericOperators?: (ctx: NumericOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	enterComparisonFunctionOperators?: (ctx: ComparisonFunctionOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.comparisonFunctionOperators`.
	 * @param ctx the parse tree
	 */
	exitComparisonFunctionOperators?: (ctx: ComparisonFunctionOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.filterOperators`.
	 * @param ctx the parse tree
	 */
	enterFilterOperators?: (ctx: FilterOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.filterOperators`.
	 * @param ctx the parse tree
	 */
	exitFilterOperators?: (ctx: FilterOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 */
	enterTimeOperators?: (ctx: TimeOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.timeOperators`.
	 * @param ctx the parse tree
	 */
	exitTimeOperators?: (ctx: TimeOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 */
	enterConditionalOperators?: (ctx: ConditionalOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.conditionalOperators`.
	 * @param ctx the parse tree
	 */
	exitConditionalOperators?: (ctx: ConditionalOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 */
	enterStringOperators?: (ctx: StringOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.stringOperators`.
	 * @param ctx the parse tree
	 */
	exitStringOperators?: (ctx: StringOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 */
	enterAggregateOperators?: (ctx: AggregateOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.aggregateOperators`.
	 * @param ctx the parse tree
	 */
	exitAggregateOperators?: (ctx: AggregateOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.groupingClause`.
	 * @param ctx the parse tree
	 */
	enterGroupingClause?: (ctx: GroupingClauseContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.groupingClause`.
	 * @param ctx the parse tree
	 */
	exitGroupingClause?: (ctx: GroupingClauseContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.itemSignature`.
	 * @param ctx the parse tree
	 */
	enterItemSignature?: (ctx: ItemSignatureContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.itemSignature`.
	 * @param ctx the parse tree
	 */
	exitItemSignature?: (ctx: ItemSignatureContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.itemReference`.
	 * @param ctx the parse tree
	 */
	enterItemReference?: (ctx: ItemReferenceContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.itemReference`.
	 * @param ctx the parse tree
	 */
	exitItemReference?: (ctx: ItemReferenceContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.colElem`.
	 * @param ctx the parse tree
	 */
	enterColElem?: (ctx: ColElemContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.colElem`.
	 * @param ctx the parse tree
	 */
	exitColElem?: (ctx: ColElemContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.sheetElem`.
	 * @param ctx the parse tree
	 */
	enterSheetElem?: (ctx: SheetElemContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.sheetElem`.
	 * @param ctx the parse tree
	 */
	exitSheetElem?: (ctx: SheetElemContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.rowElemSingle`.
	 * @param ctx the parse tree
	 */
	enterRowElemSingle?: (ctx: RowElemSingleContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.rowElemSingle`.
	 * @param ctx the parse tree
	 */
	exitRowElemSingle?: (ctx: RowElemSingleContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.rowElemRange`.
	 * @param ctx the parse tree
	 */
	enterRowElemRange?: (ctx: RowElemRangeContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.rowElemRange`.
	 * @param ctx the parse tree
	 */
	exitRowElemRange?: (ctx: RowElemRangeContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.rowElemAll`.
	 * @param ctx the parse tree
	 */
	enterRowElemAll?: (ctx: RowElemAllContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.rowElemAll`.
	 * @param ctx the parse tree
	 */
	exitRowElemAll?: (ctx: RowElemAllContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.rowElem`.
	 * @param ctx the parse tree
	 */
	enterRowElem?: (ctx: RowElemContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.rowElem`.
	 * @param ctx the parse tree
	 */
	exitRowElem?: (ctx: RowElemContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.rowHandler`.
	 * @param ctx the parse tree
	 */
	enterRowHandler?: (ctx: RowHandlerContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.rowHandler`.
	 * @param ctx the parse tree
	 */
	exitRowHandler?: (ctx: RowHandlerContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.colHandler`.
	 * @param ctx the parse tree
	 */
	enterColHandler?: (ctx: ColHandlerContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.colHandler`.
	 * @param ctx the parse tree
	 */
	exitColHandler?: (ctx: ColHandlerContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.sheetHandler`.
	 * @param ctx the parse tree
	 */
	enterSheetHandler?: (ctx: SheetHandlerContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.sheetHandler`.
	 * @param ctx the parse tree
	 */
	exitSheetHandler?: (ctx: SheetHandlerContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.interval`.
	 * @param ctx the parse tree
	 */
	enterInterval?: (ctx: IntervalContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.interval`.
	 * @param ctx the parse tree
	 */
	exitInterval?: (ctx: IntervalContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.defaultWithLiteral`.
	 * @param ctx the parse tree
	 */
	enterDefaultWithLiteral?: (ctx: DefaultWithLiteralContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.defaultWithLiteral`.
	 * @param ctx the parse tree
	 */
	exitDefaultWithLiteral?: (ctx: DefaultWithLiteralContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	enterArgument?: (ctx: ArgumentContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.argument`.
	 * @param ctx the parse tree
	 */
	exitArgument?: (ctx: ArgumentContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.select`.
	 * @param ctx the parse tree
	 */
	enterSelect?: (ctx: SelectContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.select`.
	 * @param ctx the parse tree
	 */
	exitSelect?: (ctx: SelectContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.selectOperand`.
	 * @param ctx the parse tree
	 */
	enterSelectOperand?: (ctx: SelectOperandContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.selectOperand`.
	 * @param ctx the parse tree
	 */
	exitSelectOperand?: (ctx: SelectOperandContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.varID`.
	 * @param ctx the parse tree
	 */
	enterVarID?: (ctx: VarIDContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.varID`.
	 * @param ctx the parse tree
	 */
	exitVarID?: (ctx: VarIDContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.cellRef`.
	 * @param ctx the parse tree
	 */
	enterCellRef?: (ctx: CellRefContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.cellRef`.
	 * @param ctx the parse tree
	 */
	exitCellRef?: (ctx: CellRefContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.preconditionElem`.
	 * @param ctx the parse tree
	 */
	enterPreconditionElem?: (ctx: PreconditionElemContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.preconditionElem`.
	 * @param ctx the parse tree
	 */
	exitPreconditionElem?: (ctx: PreconditionElemContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.varRef`.
	 * @param ctx the parse tree
	 */
	enterVarRef?: (ctx: VarRefContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.varRef`.
	 * @param ctx the parse tree
	 */
	exitVarRef?: (ctx: VarRefContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.operationRef`.
	 * @param ctx the parse tree
	 */
	enterOperationRef?: (ctx: OperationRefContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.operationRef`.
	 * @param ctx the parse tree
	 */
	exitOperationRef?: (ctx: OperationRefContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	enterCellAddress?: (ctx: CellAddressContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.cellAddress`.
	 * @param ctx the parse tree
	 */
	exitCellAddress?: (ctx: CellAddressContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.tableReferenceSingle`.
	 * @param ctx the parse tree
	 */
	enterTableReferenceSingle?: (ctx: TableReferenceSingleContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.tableReferenceSingle`.
	 * @param ctx the parse tree
	 */
	exitTableReferenceSingle?: (ctx: TableReferenceSingleContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.tableGroupReference`.
	 * @param ctx the parse tree
	 */
	enterTableGroupReference?: (ctx: TableGroupReferenceContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.tableGroupReference`.
	 * @param ctx the parse tree
	 */
	exitTableGroupReference?: (ctx: TableGroupReferenceContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.tableReference`.
	 * @param ctx the parse tree
	 */
	enterTableReference?: (ctx: TableReferenceContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.tableReference`.
	 * @param ctx the parse tree
	 */
	exitTableReference?: (ctx: TableReferenceContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	enterClauseOperators?: (ctx: ClauseOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.clauseOperators`.
	 * @param ctx the parse tree
	 */
	exitClauseOperators?: (ctx: ClauseOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.renameClause`.
	 * @param ctx the parse tree
	 */
	enterRenameClause?: (ctx: RenameClauseContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.renameClause`.
	 * @param ctx the parse tree
	 */
	exitRenameClause?: (ctx: RenameClauseContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.comparisonOperators`.
	 * @param ctx the parse tree
	 */
	enterComparisonOperators?: (ctx: ComparisonOperatorsContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.comparisonOperators`.
	 * @param ctx the parse tree
	 */
	exitComparisonOperators?: (ctx: ComparisonOperatorsContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.literal`.
	 * @param ctx the parse tree
	 */
	enterLiteral?: (ctx: LiteralContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.literal`.
	 * @param ctx the parse tree
	 */
	exitLiteral?: (ctx: LiteralContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.keyNames`.
	 * @param ctx the parse tree
	 */
	enterKeyNames?: (ctx: KeyNamesContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.keyNames`.
	 * @param ctx the parse tree
	 */
	exitKeyNames?: (ctx: KeyNamesContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.propertyReference`.
	 * @param ctx the parse tree
	 */
	enterPropertyReference?: (ctx: PropertyReferenceContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.propertyReference`.
	 * @param ctx the parse tree
	 */
	exitPropertyReference?: (ctx: PropertyReferenceContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.propertyCode`.
	 * @param ctx the parse tree
	 */
	enterPropertyCode?: (ctx: PropertyCodeContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.propertyCode`.
	 * @param ctx the parse tree
	 */
	exitPropertyCode?: (ctx: PropertyCodeContext) => void;

	/**
	 * Enter a parse tree produced by `DpmXlParser.temporaryIdentifier`.
	 * @param ctx the parse tree
	 */
	enterTemporaryIdentifier?: (ctx: TemporaryIdentifierContext) => void;
	/**
	 * Exit a parse tree produced by `DpmXlParser.temporaryIdentifier`.
	 * @param ctx the parse tree
	 */
	exitTemporaryIdentifier?: (ctx: TemporaryIdentifierContext) => void;
}

