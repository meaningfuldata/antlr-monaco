// Generated from ./examples/dpm-xl/antlr/DpmXlParser.g4 by ANTLR 4.9.0-SNAPSHOT


import { ATN } from "antlr4ts/atn/ATN";
import { ATNDeserializer } from "antlr4ts/atn/ATNDeserializer";
import { FailedPredicateException } from "antlr4ts/FailedPredicateException";
import { NotNull } from "antlr4ts/Decorators";
import { NoViableAltException } from "antlr4ts/NoViableAltException";
import { Override } from "antlr4ts/Decorators";
import { Parser } from "antlr4ts/Parser";
import { ParserRuleContext } from "antlr4ts/ParserRuleContext";
import { ParserATNSimulator } from "antlr4ts/atn/ParserATNSimulator";
import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";
import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";
import { RecognitionException } from "antlr4ts/RecognitionException";
import { RuleContext } from "antlr4ts/RuleContext";
//import { RuleVersion } from "antlr4ts/RuleVersion";
import { TerminalNode } from "antlr4ts/tree/TerminalNode";
import { Token } from "antlr4ts/Token";
import { TokenStream } from "antlr4ts/TokenStream";
import { Vocabulary } from "antlr4ts/Vocabulary";
import { VocabularyImpl } from "antlr4ts/VocabularyImpl";

import * as Utils from "antlr4ts/misc/Utils";

import { DpmXlParserListener } from "./DpmXlParserListener";
import { DpmXlParserVisitor } from "./DpmXlParserVisitor";


export class DpmXlParser extends Parser {
	public static readonly BOOLEAN_LITERAL = 1;
	public static readonly AND = 2;
	public static readonly OR = 3;
	public static readonly XOR = 4;
	public static readonly NOT = 5;
	public static readonly ASSIGN = 6;
	public static readonly PERSISTENT_ASSIGN = 7;
	public static readonly EQ = 8;
	public static readonly NE = 9;
	public static readonly LT = 10;
	public static readonly LE = 11;
	public static readonly GT = 12;
	public static readonly GE = 13;
	public static readonly MATCH = 14;
	public static readonly WITH = 15;
	public static readonly PLUS = 16;
	public static readonly MINUS = 17;
	public static readonly MULT = 18;
	public static readonly DIV = 19;
	public static readonly MAX_AGGR = 20;
	public static readonly MIN_AGGR = 21;
	public static readonly SUM = 22;
	public static readonly COUNT = 23;
	public static readonly AVG = 24;
	public static readonly MEDIAN = 25;
	public static readonly GROUP_BY = 26;
	public static readonly ABS = 27;
	public static readonly ISNULL = 28;
	public static readonly EXP = 29;
	public static readonly LN = 30;
	public static readonly SQRT = 31;
	public static readonly POWER = 32;
	public static readonly LOG = 33;
	public static readonly MAX = 34;
	public static readonly MIN = 35;
	public static readonly IN = 36;
	public static readonly COMMA = 37;
	public static readonly COLON = 38;
	public static readonly LPAREN = 39;
	public static readonly RPAREN = 40;
	public static readonly CURLY_BRACKET_LEFT = 41;
	public static readonly CURLY_BRACKET_RIGHT = 42;
	public static readonly SQUARE_BRACKET_LEFT = 43;
	public static readonly SQUARE_BRACKET_RIGHT = 44;
	public static readonly IF = 45;
	public static readonly ENDIF = 46;
	public static readonly THEN = 47;
	public static readonly ELSE = 48;
	public static readonly NVL = 49;
	public static readonly FILTER = 50;
	public static readonly WHERE = 51;
	public static readonly GET = 52;
	public static readonly RENAME = 53;
	public static readonly TO = 54;
	public static readonly TIME_SHIFT = 55;
	public static readonly LEN = 56;
	public static readonly CONCAT = 57;
	public static readonly ROW_HEADING = 58;
	public static readonly COL_HEADING = 59;
	public static readonly SHEET_HEADING = 60;
	public static readonly TIME_PERIOD = 61;
	public static readonly ITEM_SIGNATURE = 62;
	public static readonly EOL = 63;
	public static readonly INTEGER_LITERAL = 64;
	public static readonly DECIMAL_LITERAL = 65;
	public static readonly PERCENT_LITERAL = 66;
	public static readonly STRING_LITERAL = 67;
	public static readonly EMPTY_LITERAL = 68;
	public static readonly CODE = 69;
	public static readonly DATE_LITERAL = 70;
	public static readonly TIME_INTERVAL_LITERAL = 71;
	public static readonly TIME_PERIOD_LITERAL = 72;
	public static readonly WS = 73;
	public static readonly INTERVAL = 74;
	public static readonly DEFAULT = 75;
	public static readonly ROW_PREFIX = 76;
	public static readonly COL_PREFIX = 77;
	public static readonly SHEET_PREFIX = 78;
	public static readonly TABLE_PREFIX = 79;
	public static readonly TABLE_GROUP_PREFIX = 80;
	public static readonly ROW = 81;
	public static readonly ROW_RANGE = 82;
	public static readonly ROW_ALL = 83;
	public static readonly COL = 84;
	public static readonly COL_RANGE = 85;
	public static readonly COL_ALL = 86;
	public static readonly SHEET = 87;
	public static readonly SHEET_RANGE = 88;
	public static readonly SHEET_ALL = 89;
	public static readonly TABLE_REFERENCE = 90;
	public static readonly TABLE_GROUP_REFERENCE = 91;
	public static readonly VAR_REFERENCE = 92;
	public static readonly OPERATION_REFERENCE = 93;
	public static readonly PRECONDITION_ELEMENT = 94;
	public static readonly SELECTION_MODE_WS = 95;
	public static readonly SET_OPERAND_MODE_WS = 96;
	public static readonly RULE_root = 0;
	public static readonly RULE_statements = 1;
	public static readonly RULE_statement = 2;
	public static readonly RULE_persistentExpression = 3;
	public static readonly RULE_expressionWithoutAssignment = 4;
	public static readonly RULE_selectionStart = 5;
	public static readonly RULE_selectionEnd = 6;
	public static readonly RULE_partialSelection = 7;
	public static readonly RULE_temporaryAssignmentExpression = 8;
	public static readonly RULE_persistentAssignmentExpression = 9;
	public static readonly RULE_expression = 10;
	public static readonly RULE_setOperand = 11;
	public static readonly RULE_setElements = 12;
	public static readonly RULE_functions = 13;
	public static readonly RULE_numericOperators = 14;
	public static readonly RULE_comparisonFunctionOperators = 15;
	public static readonly RULE_filterOperators = 16;
	public static readonly RULE_timeOperators = 17;
	public static readonly RULE_conditionalOperators = 18;
	public static readonly RULE_stringOperators = 19;
	public static readonly RULE_aggregateOperators = 20;
	public static readonly RULE_groupingClause = 21;
	public static readonly RULE_itemSignature = 22;
	public static readonly RULE_itemReference = 23;
	public static readonly RULE_colElem = 24;
	public static readonly RULE_sheetElem = 25;
	public static readonly RULE_rowElemSingle = 26;
	public static readonly RULE_rowElemRange = 27;
	public static readonly RULE_rowElemAll = 28;
	public static readonly RULE_rowElem = 29;
	public static readonly RULE_rowHandler = 30;
	public static readonly RULE_colHandler = 31;
	public static readonly RULE_sheetHandler = 32;
	public static readonly RULE_interval = 33;
	public static readonly RULE_defaultWithLiteral = 34;
	public static readonly RULE_argument = 35;
	public static readonly RULE_select = 36;
	public static readonly RULE_selectOperand = 37;
	public static readonly RULE_varID = 38;
	public static readonly RULE_cellRef = 39;
	public static readonly RULE_preconditionElem = 40;
	public static readonly RULE_varRef = 41;
	public static readonly RULE_operationRef = 42;
	public static readonly RULE_cellAddress = 43;
	public static readonly RULE_tableReferenceSingle = 44;
	public static readonly RULE_tableGroupReference = 45;
	public static readonly RULE_tableReference = 46;
	public static readonly RULE_clauseOperators = 47;
	public static readonly RULE_renameClause = 48;
	public static readonly RULE_comparisonOperators = 49;
	public static readonly RULE_literal = 50;
	public static readonly RULE_keyNames = 51;
	public static readonly RULE_propertyReference = 52;
	public static readonly RULE_propertyCode = 53;
	public static readonly RULE_temporaryIdentifier = 54;
	// tslint:disable:no-trailing-whitespace
	public static readonly ruleNames: string[] = [
		"root", "statements", "statement", "persistentExpression", "expressionWithoutAssignment", 
		"selectionStart", "selectionEnd", "partialSelection", "temporaryAssignmentExpression", 
		"persistentAssignmentExpression", "expression", "setOperand", "setElements", 
		"functions", "numericOperators", "comparisonFunctionOperators", "filterOperators", 
		"timeOperators", "conditionalOperators", "stringOperators", "aggregateOperators", 
		"groupingClause", "itemSignature", "itemReference", "colElem", "sheetElem", 
		"rowElemSingle", "rowElemRange", "rowElemAll", "rowElem", "rowHandler", 
		"colHandler", "sheetHandler", "interval", "defaultWithLiteral", "argument", 
		"select", "selectOperand", "varID", "cellRef", "preconditionElem", "varRef", 
		"operationRef", "cellAddress", "tableReferenceSingle", "tableGroupReference", 
		"tableReference", "clauseOperators", "renameClause", "comparisonOperators", 
		"literal", "keyNames", "propertyReference", "propertyCode", "temporaryIdentifier",
	];

	private static readonly _LITERAL_NAMES: Array<string | undefined> = [
		undefined, undefined, "'and'", "'or'", "'xor'", "'not'", "':='", "'<-'", 
		"'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'match'", "'with'", "'+'", 
		"'-'", "'*'", "'/'", "'max_aggr'", "'min_aggr'", "'sum'", "'count'", "'avg'", 
		"'median'", "'group by'", "'abs'", "'isnull'", "'exp'", "'ln'", "'sqrt'", 
		"'power'", "'log'", "'max'", "'min'", "'in'", "','", "':'", "'('", "')'", 
		"'{'", "'}'", "'['", "']'", "'if'", "'endif'", "'then'", "'else'", "'nvl'", 
		"'filter'", "'where'", "'get'", "'rename'", "'to'", "'time_shift'", "'len'", 
		"'&'", "'r'", "'c'", "'s'", undefined, undefined, "';'", undefined, undefined, 
		undefined, undefined, undefined, undefined, undefined, undefined, undefined, 
		undefined, "'interval'", "'default'", undefined, undefined, undefined, 
		"'t'", "'g'",
	];
	private static readonly _SYMBOLIC_NAMES: Array<string | undefined> = [
		undefined, "BOOLEAN_LITERAL", "AND", "OR", "XOR", "NOT", "ASSIGN", "PERSISTENT_ASSIGN", 
		"EQ", "NE", "LT", "LE", "GT", "GE", "MATCH", "WITH", "PLUS", "MINUS", 
		"MULT", "DIV", "MAX_AGGR", "MIN_AGGR", "SUM", "COUNT", "AVG", "MEDIAN", 
		"GROUP_BY", "ABS", "ISNULL", "EXP", "LN", "SQRT", "POWER", "LOG", "MAX", 
		"MIN", "IN", "COMMA", "COLON", "LPAREN", "RPAREN", "CURLY_BRACKET_LEFT", 
		"CURLY_BRACKET_RIGHT", "SQUARE_BRACKET_LEFT", "SQUARE_BRACKET_RIGHT", 
		"IF", "ENDIF", "THEN", "ELSE", "NVL", "FILTER", "WHERE", "GET", "RENAME", 
		"TO", "TIME_SHIFT", "LEN", "CONCAT", "ROW_HEADING", "COL_HEADING", "SHEET_HEADING", 
		"TIME_PERIOD", "ITEM_SIGNATURE", "EOL", "INTEGER_LITERAL", "DECIMAL_LITERAL", 
		"PERCENT_LITERAL", "STRING_LITERAL", "EMPTY_LITERAL", "CODE", "DATE_LITERAL", 
		"TIME_INTERVAL_LITERAL", "TIME_PERIOD_LITERAL", "WS", "INTERVAL", "DEFAULT", 
		"ROW_PREFIX", "COL_PREFIX", "SHEET_PREFIX", "TABLE_PREFIX", "TABLE_GROUP_PREFIX", 
		"ROW", "ROW_RANGE", "ROW_ALL", "COL", "COL_RANGE", "COL_ALL", "SHEET", 
		"SHEET_RANGE", "SHEET_ALL", "TABLE_REFERENCE", "TABLE_GROUP_REFERENCE", 
		"VAR_REFERENCE", "OPERATION_REFERENCE", "PRECONDITION_ELEMENT", "SELECTION_MODE_WS", 
		"SET_OPERAND_MODE_WS",
	];
	public static readonly VOCABULARY: Vocabulary = new VocabularyImpl(DpmXlParser._LITERAL_NAMES, DpmXlParser._SYMBOLIC_NAMES, []);

	// @Override
	// @NotNull
	public get vocabulary(): Vocabulary {
		return DpmXlParser.VOCABULARY;
	}
	// tslint:enable:no-trailing-whitespace

	// @Override
	public get grammarFileName(): string { return "DpmXlParser.g4"; }

	// @Override
	public get ruleNames(): string[] { return DpmXlParser.ruleNames; }

	// @Override
	public get serializedATN(): string { return DpmXlParser._serializedATN; }

	protected createFailedPredicateException(predicate?: string, message?: string): FailedPredicateException {
		return new FailedPredicateException(this, predicate, message);
	}

	constructor(input: TokenStream) {
		super(input);
		this._interp = new ParserATNSimulator(DpmXlParser._ATN, this);
	}
	// @RuleVersion(0)
	public root(): RootContext {
		let _localctx: RootContext = new RootContext(this._ctx, this.state);
		this.enterRule(_localctx, 0, DpmXlParser.RULE_root);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 110;
			this.statement();
			this.state = 116;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 1, this._ctx) ) {
			case 1:
				{
				{
				this.state = 111;
				this.match(DpmXlParser.EOL);
				this.state = 112;
				this.statements();
				}
				}
				break;

			case 2:
				{
				this.state = 114;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === DpmXlParser.EOL) {
					{
					this.state = 113;
					this.match(DpmXlParser.EOL);
					}
				}

				}
				break;
			}
			this.state = 118;
			this.match(DpmXlParser.EOF);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public statements(): StatementsContext {
		let _localctx: StatementsContext = new StatementsContext(this._ctx, this.state);
		this.enterRule(_localctx, 2, DpmXlParser.RULE_statements);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 123;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			do {
				{
				{
				this.state = 120;
				this.statement();
				this.state = 121;
				this.match(DpmXlParser.EOL);
				}
				}
				this.state = 125;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			} while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << DpmXlParser.BOOLEAN_LITERAL) | (1 << DpmXlParser.NOT) | (1 << DpmXlParser.MATCH) | (1 << DpmXlParser.WITH) | (1 << DpmXlParser.PLUS) | (1 << DpmXlParser.MINUS) | (1 << DpmXlParser.MAX_AGGR) | (1 << DpmXlParser.MIN_AGGR) | (1 << DpmXlParser.SUM) | (1 << DpmXlParser.COUNT) | (1 << DpmXlParser.AVG) | (1 << DpmXlParser.MEDIAN) | (1 << DpmXlParser.ABS) | (1 << DpmXlParser.ISNULL) | (1 << DpmXlParser.EXP) | (1 << DpmXlParser.LN) | (1 << DpmXlParser.SQRT))) !== 0) || ((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (DpmXlParser.POWER - 32)) | (1 << (DpmXlParser.LOG - 32)) | (1 << (DpmXlParser.MAX - 32)) | (1 << (DpmXlParser.MIN - 32)) | (1 << (DpmXlParser.LPAREN - 32)) | (1 << (DpmXlParser.CURLY_BRACKET_LEFT - 32)) | (1 << (DpmXlParser.SQUARE_BRACKET_LEFT - 32)) | (1 << (DpmXlParser.IF - 32)) | (1 << (DpmXlParser.NVL - 32)) | (1 << (DpmXlParser.FILTER - 32)) | (1 << (DpmXlParser.TIME_SHIFT - 32)) | (1 << (DpmXlParser.LEN - 32)) | (1 << (DpmXlParser.ROW_HEADING - 32)) | (1 << (DpmXlParser.COL_HEADING - 32)) | (1 << (DpmXlParser.SHEET_HEADING - 32)))) !== 0) || ((((_la - 64)) & ~0x1F) === 0 && ((1 << (_la - 64)) & ((1 << (DpmXlParser.INTEGER_LITERAL - 64)) | (1 << (DpmXlParser.DECIMAL_LITERAL - 64)) | (1 << (DpmXlParser.PERCENT_LITERAL - 64)) | (1 << (DpmXlParser.STRING_LITERAL - 64)) | (1 << (DpmXlParser.EMPTY_LITERAL - 64)) | (1 << (DpmXlParser.CODE - 64)) | (1 << (DpmXlParser.DATE_LITERAL - 64)) | (1 << (DpmXlParser.TIME_INTERVAL_LITERAL - 64)) | (1 << (DpmXlParser.TIME_PERIOD_LITERAL - 64)))) !== 0));
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public statement(): StatementContext {
		let _localctx: StatementContext = new StatementContext(this._ctx, this.state);
		this.enterRule(_localctx, 4, DpmXlParser.RULE_statement);
		try {
			this.state = 129;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 3, this._ctx) ) {
			case 1:
				_localctx = new ExprWithoutAssignmentContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 127;
				this.expressionWithoutAssignment();
				}
				break;

			case 2:
				_localctx = new AssignmentExprContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 128;
				this.temporaryAssignmentExpression();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public persistentExpression(): PersistentExpressionContext {
		let _localctx: PersistentExpressionContext = new PersistentExpressionContext(this._ctx, this.state);
		this.enterRule(_localctx, 6, DpmXlParser.RULE_persistentExpression);
		try {
			this.state = 133;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 4, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 131;
				this.persistentAssignmentExpression();
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 132;
				this.expressionWithoutAssignment();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public expressionWithoutAssignment(): ExpressionWithoutAssignmentContext {
		let _localctx: ExpressionWithoutAssignmentContext = new ExpressionWithoutAssignmentContext(this._ctx, this.state);
		this.enterRule(_localctx, 8, DpmXlParser.RULE_expressionWithoutAssignment);
		try {
			this.state = 141;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.BOOLEAN_LITERAL:
			case DpmXlParser.NOT:
			case DpmXlParser.MATCH:
			case DpmXlParser.PLUS:
			case DpmXlParser.MINUS:
			case DpmXlParser.MAX_AGGR:
			case DpmXlParser.MIN_AGGR:
			case DpmXlParser.SUM:
			case DpmXlParser.COUNT:
			case DpmXlParser.AVG:
			case DpmXlParser.MEDIAN:
			case DpmXlParser.ABS:
			case DpmXlParser.ISNULL:
			case DpmXlParser.EXP:
			case DpmXlParser.LN:
			case DpmXlParser.SQRT:
			case DpmXlParser.POWER:
			case DpmXlParser.LOG:
			case DpmXlParser.MAX:
			case DpmXlParser.MIN:
			case DpmXlParser.LPAREN:
			case DpmXlParser.CURLY_BRACKET_LEFT:
			case DpmXlParser.SQUARE_BRACKET_LEFT:
			case DpmXlParser.IF:
			case DpmXlParser.NVL:
			case DpmXlParser.FILTER:
			case DpmXlParser.TIME_SHIFT:
			case DpmXlParser.LEN:
			case DpmXlParser.ROW_HEADING:
			case DpmXlParser.COL_HEADING:
			case DpmXlParser.SHEET_HEADING:
			case DpmXlParser.INTEGER_LITERAL:
			case DpmXlParser.DECIMAL_LITERAL:
			case DpmXlParser.PERCENT_LITERAL:
			case DpmXlParser.STRING_LITERAL:
			case DpmXlParser.EMPTY_LITERAL:
			case DpmXlParser.CODE:
			case DpmXlParser.DATE_LITERAL:
			case DpmXlParser.TIME_INTERVAL_LITERAL:
			case DpmXlParser.TIME_PERIOD_LITERAL:
				_localctx = new ExprWithoutPartialSelectionContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 135;
				this.expression(0);
				}
				break;
			case DpmXlParser.WITH:
				_localctx = new ExprWithSelectionContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 136;
				this.match(DpmXlParser.WITH);
				this.state = 137;
				this.partialSelection();
				this.state = 138;
				this.match(DpmXlParser.COLON);
				this.state = 139;
				this.expression(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public selectionStart(): SelectionStartContext {
		let _localctx: SelectionStartContext = new SelectionStartContext(this._ctx, this.state);
		this.enterRule(_localctx, 10, DpmXlParser.RULE_selectionStart);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 143;
			this.match(DpmXlParser.CURLY_BRACKET_LEFT);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public selectionEnd(): SelectionEndContext {
		let _localctx: SelectionEndContext = new SelectionEndContext(this._ctx, this.state);
		this.enterRule(_localctx, 12, DpmXlParser.RULE_selectionEnd);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 145;
			this.match(DpmXlParser.CURLY_BRACKET_RIGHT);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public partialSelection(): PartialSelectionContext {
		let _localctx: PartialSelectionContext = new PartialSelectionContext(this._ctx, this.state);
		this.enterRule(_localctx, 14, DpmXlParser.RULE_partialSelection);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 147;
			this.selectionStart();
			this.state = 148;
			this.cellRef();
			this.state = 149;
			this.selectionEnd();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public temporaryAssignmentExpression(): TemporaryAssignmentExpressionContext {
		let _localctx: TemporaryAssignmentExpressionContext = new TemporaryAssignmentExpressionContext(this._ctx, this.state);
		this.enterRule(_localctx, 16, DpmXlParser.RULE_temporaryAssignmentExpression);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 151;
			this.temporaryIdentifier();
			this.state = 152;
			this.match(DpmXlParser.ASSIGN);
			this.state = 153;
			this.persistentExpression();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public persistentAssignmentExpression(): PersistentAssignmentExpressionContext {
		let _localctx: PersistentAssignmentExpressionContext = new PersistentAssignmentExpressionContext(this._ctx, this.state);
		this.enterRule(_localctx, 18, DpmXlParser.RULE_persistentAssignmentExpression);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 155;
			this.varID();
			this.state = 156;
			this.match(DpmXlParser.PERSISTENT_ASSIGN);
			this.state = 157;
			this.expressionWithoutAssignment();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public expression(): ExpressionContext;
	public expression(_p: number): ExpressionContext;
	// @RuleVersion(0)
	public expression(_p?: number): ExpressionContext {
		if (_p === undefined) {
			_p = 0;
		}

		let _parentctx: ParserRuleContext = this._ctx;
		let _parentState: number = this.state;
		let _localctx: ExpressionContext = new ExpressionContext(this._ctx, _parentState);
		let _prevctx: ExpressionContext = _localctx;
		let _startState: number = 20;
		this.enterRecursionRule(_localctx, 20, DpmXlParser.RULE_expression, _p);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 187;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 7, this._ctx) ) {
			case 1:
				{
				_localctx = new ParExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;

				this.state = 160;
				this.match(DpmXlParser.LPAREN);
				this.state = 161;
				this.expression(0);
				this.state = 162;
				this.match(DpmXlParser.RPAREN);
				}
				break;

			case 2:
				{
				_localctx = new FuncExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 164;
				this.functions();
				}
				break;

			case 3:
				{
				_localctx = new UnaryExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 165;
				(_localctx as UnaryExprContext)._op = this._input.LT(1);
				_la = this._input.LA(1);
				if (!(_la === DpmXlParser.PLUS || _la === DpmXlParser.MINUS)) {
					(_localctx as UnaryExprContext)._op = this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 166;
				this.expression(15);
				}
				break;

			case 4:
				{
				_localctx = new NotExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 167;
				(_localctx as NotExprContext)._op = this.match(DpmXlParser.NOT);
				this.state = 168;
				this.match(DpmXlParser.LPAREN);
				this.state = 169;
				this.expression(0);
				this.state = 170;
				this.match(DpmXlParser.RPAREN);
				}
				break;

			case 5:
				{
				_localctx = new IfExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 172;
				this.match(DpmXlParser.IF);
				this.state = 173;
				(_localctx as IfExprContext)._conditionalExpr = this.expression(0);
				this.state = 174;
				this.match(DpmXlParser.THEN);
				this.state = 175;
				(_localctx as IfExprContext)._thenExpr = this.expression(0);
				this.state = 178;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === DpmXlParser.ELSE) {
					{
					this.state = 176;
					this.match(DpmXlParser.ELSE);
					this.state = 177;
					(_localctx as IfExprContext)._elseExpr = this.expression(0);
					}
				}

				this.state = 180;
				this.match(DpmXlParser.ENDIF);
				}
				break;

			case 6:
				{
				_localctx = new ItemReferenceExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 182;
				this.itemReference();
				}
				break;

			case 7:
				{
				_localctx = new PropertyReferenceExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 183;
				this.propertyReference();
				}
				break;

			case 8:
				{
				_localctx = new KeyNamesExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 184;
				this.keyNames();
				}
				break;

			case 9:
				{
				_localctx = new LiteralExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 185;
				this.literal();
				}
				break;

			case 10:
				{
				_localctx = new SelectExprContext(_localctx);
				this._ctx = _localctx;
				_prevctx = _localctx;
				this.state = 186;
				this.select();
				}
				break;
			}
			this._ctx._stop = this._input.tryLT(-1);
			this.state = 218;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 9, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					if (this._parseListeners != null) {
						this.triggerExitRuleEvent();
					}
					_prevctx = _localctx;
					{
					this.state = 216;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input, 8, this._ctx) ) {
					case 1:
						{
						_localctx = new NumericExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as NumericExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 189;
						if (!(this.precpred(this._ctx, 13))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 13)");
						}
						this.state = 190;
						(_localctx as NumericExprContext)._op = this._input.LT(1);
						_la = this._input.LA(1);
						if (!(_la === DpmXlParser.MULT || _la === DpmXlParser.DIV)) {
							(_localctx as NumericExprContext)._op = this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 191;
						(_localctx as NumericExprContext)._right = this.expression(14);
						}
						break;

					case 2:
						{
						_localctx = new NumericExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as NumericExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 192;
						if (!(this.precpred(this._ctx, 12))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 12)");
						}
						this.state = 193;
						(_localctx as NumericExprContext)._op = this._input.LT(1);
						_la = this._input.LA(1);
						if (!(_la === DpmXlParser.PLUS || _la === DpmXlParser.MINUS)) {
							(_localctx as NumericExprContext)._op = this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 194;
						(_localctx as NumericExprContext)._right = this.expression(13);
						}
						break;

					case 3:
						{
						_localctx = new ConcatExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as ConcatExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 195;
						if (!(this.precpred(this._ctx, 11))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 11)");
						}
						this.state = 196;
						(_localctx as ConcatExprContext)._op = this.match(DpmXlParser.CONCAT);
						this.state = 197;
						(_localctx as ConcatExprContext)._right = this.expression(12);
						}
						break;

					case 4:
						{
						_localctx = new CompExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as CompExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 198;
						if (!(this.precpred(this._ctx, 10))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 10)");
						}
						this.state = 199;
						(_localctx as CompExprContext)._op = this.comparisonOperators();
						this.state = 200;
						(_localctx as CompExprContext)._right = this.expression(11);
						}
						break;

					case 5:
						{
						_localctx = new BoolExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as BoolExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 202;
						if (!(this.precpred(this._ctx, 8))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 8)");
						}
						this.state = 203;
						(_localctx as BoolExprContext)._op = this.match(DpmXlParser.AND);
						this.state = 204;
						(_localctx as BoolExprContext)._right = this.expression(9);
						}
						break;

					case 6:
						{
						_localctx = new BoolExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as BoolExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 205;
						if (!(this.precpred(this._ctx, 7))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 7)");
						}
						this.state = 206;
						(_localctx as BoolExprContext)._op = this._input.LT(1);
						_la = this._input.LA(1);
						if (!(_la === DpmXlParser.OR || _la === DpmXlParser.XOR)) {
							(_localctx as BoolExprContext)._op = this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 207;
						(_localctx as BoolExprContext)._right = this.expression(8);
						}
						break;

					case 7:
						{
						_localctx = new ClauseExprContext(new ExpressionContext(_parentctx, _parentState));
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 208;
						if (!(this.precpred(this._ctx, 16))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 16)");
						}
						this.state = 209;
						this.match(DpmXlParser.SQUARE_BRACKET_LEFT);
						this.state = 210;
						this.clauseOperators();
						this.state = 211;
						this.match(DpmXlParser.SQUARE_BRACKET_RIGHT);
						}
						break;

					case 8:
						{
						_localctx = new InExprContext(new ExpressionContext(_parentctx, _parentState));
						(_localctx as InExprContext)._left = _prevctx;
						this.pushNewRecursionContext(_localctx, _startState, DpmXlParser.RULE_expression);
						this.state = 213;
						if (!(this.precpred(this._ctx, 9))) {
							throw this.createFailedPredicateException("this.precpred(this._ctx, 9)");
						}
						this.state = 214;
						(_localctx as InExprContext)._op = this.match(DpmXlParser.IN);
						this.state = 215;
						this.setOperand();
						}
						break;
					}
					}
				}
				this.state = 220;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 9, this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public setOperand(): SetOperandContext {
		let _localctx: SetOperandContext = new SetOperandContext(this._ctx, this.state);
		this.enterRule(_localctx, 22, DpmXlParser.RULE_setOperand);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 221;
			this.selectionStart();
			this.state = 222;
			this.setElements();
			this.state = 223;
			this.selectionEnd();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public setElements(): SetElementsContext {
		let _localctx: SetElementsContext = new SetElementsContext(this._ctx, this.state);
		this.enterRule(_localctx, 24, DpmXlParser.RULE_setElements);
		let _la: number;
		try {
			this.state = 241;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.SQUARE_BRACKET_LEFT:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 225;
				this.itemReference();
				this.state = 230;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 226;
					this.match(DpmXlParser.COMMA);
					this.state = 227;
					this.itemReference();
					}
					}
					this.state = 232;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				break;
			case DpmXlParser.BOOLEAN_LITERAL:
			case DpmXlParser.INTEGER_LITERAL:
			case DpmXlParser.DECIMAL_LITERAL:
			case DpmXlParser.PERCENT_LITERAL:
			case DpmXlParser.STRING_LITERAL:
			case DpmXlParser.EMPTY_LITERAL:
			case DpmXlParser.DATE_LITERAL:
			case DpmXlParser.TIME_INTERVAL_LITERAL:
			case DpmXlParser.TIME_PERIOD_LITERAL:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 233;
				this.literal();
				this.state = 238;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 234;
					this.match(DpmXlParser.COMMA);
					this.state = 235;
					this.literal();
					}
					}
					this.state = 240;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public functions(): FunctionsContext {
		let _localctx: FunctionsContext = new FunctionsContext(this._ctx, this.state);
		this.enterRule(_localctx, 26, DpmXlParser.RULE_functions);
		try {
			this.state = 250;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.MAX_AGGR:
			case DpmXlParser.MIN_AGGR:
			case DpmXlParser.SUM:
			case DpmXlParser.COUNT:
			case DpmXlParser.AVG:
			case DpmXlParser.MEDIAN:
				_localctx = new AggregateFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 243;
				this.aggregateOperators();
				}
				break;
			case DpmXlParser.ABS:
			case DpmXlParser.EXP:
			case DpmXlParser.LN:
			case DpmXlParser.SQRT:
			case DpmXlParser.POWER:
			case DpmXlParser.LOG:
			case DpmXlParser.MAX:
			case DpmXlParser.MIN:
				_localctx = new NumericFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 244;
				this.numericOperators();
				}
				break;
			case DpmXlParser.MATCH:
			case DpmXlParser.ISNULL:
				_localctx = new ComparisonFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 245;
				this.comparisonFunctionOperators();
				}
				break;
			case DpmXlParser.FILTER:
				_localctx = new FilterFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 246;
				this.filterOperators();
				}
				break;
			case DpmXlParser.NVL:
				_localctx = new ConditionalFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 247;
				this.conditionalOperators();
				}
				break;
			case DpmXlParser.TIME_SHIFT:
				_localctx = new TimeFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 248;
				this.timeOperators();
				}
				break;
			case DpmXlParser.LEN:
				_localctx = new StringFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 7);
				{
				this.state = 249;
				this.stringOperators();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public numericOperators(): NumericOperatorsContext {
		let _localctx: NumericOperatorsContext = new NumericOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 28, DpmXlParser.RULE_numericOperators);
		let _la: number;
		try {
			this.state = 275;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.ABS:
			case DpmXlParser.EXP:
			case DpmXlParser.LN:
			case DpmXlParser.SQRT:
				_localctx = new UnaryNumericFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 252;
				(_localctx as UnaryNumericFunctionsContext)._op = this._input.LT(1);
				_la = this._input.LA(1);
				if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << DpmXlParser.ABS) | (1 << DpmXlParser.EXP) | (1 << DpmXlParser.LN) | (1 << DpmXlParser.SQRT))) !== 0))) {
					(_localctx as UnaryNumericFunctionsContext)._op = this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 253;
				this.match(DpmXlParser.LPAREN);
				this.state = 254;
				this.expression(0);
				this.state = 255;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			case DpmXlParser.POWER:
			case DpmXlParser.LOG:
				_localctx = new BinaryNumericFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 257;
				(_localctx as BinaryNumericFunctionsContext)._op = this._input.LT(1);
				_la = this._input.LA(1);
				if (!(_la === DpmXlParser.POWER || _la === DpmXlParser.LOG)) {
					(_localctx as BinaryNumericFunctionsContext)._op = this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 258;
				this.match(DpmXlParser.LPAREN);
				this.state = 259;
				(_localctx as BinaryNumericFunctionsContext)._left = this.expression(0);
				this.state = 260;
				this.match(DpmXlParser.COMMA);
				this.state = 261;
				(_localctx as BinaryNumericFunctionsContext)._right = this.expression(0);
				this.state = 262;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			case DpmXlParser.MAX:
			case DpmXlParser.MIN:
				_localctx = new ComplexNumericFunctionsContext(_localctx);
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 264;
				(_localctx as ComplexNumericFunctionsContext)._op = this._input.LT(1);
				_la = this._input.LA(1);
				if (!(_la === DpmXlParser.MAX || _la === DpmXlParser.MIN)) {
					(_localctx as ComplexNumericFunctionsContext)._op = this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 265;
				this.match(DpmXlParser.LPAREN);
				this.state = 266;
				this.expression(0);
				this.state = 269;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				do {
					{
					{
					this.state = 267;
					this.match(DpmXlParser.COMMA);
					this.state = 268;
					this.expression(0);
					}
					}
					this.state = 271;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				} while (_la === DpmXlParser.COMMA);
				this.state = 273;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public comparisonFunctionOperators(): ComparisonFunctionOperatorsContext {
		let _localctx: ComparisonFunctionOperatorsContext = new ComparisonFunctionOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 30, DpmXlParser.RULE_comparisonFunctionOperators);
		try {
			this.state = 289;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.MATCH:
				_localctx = new MatchExprContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 277;
				this.match(DpmXlParser.MATCH);
				this.state = 278;
				this.match(DpmXlParser.LPAREN);
				this.state = 279;
				this.expression(0);
				this.state = 280;
				this.match(DpmXlParser.COMMA);
				this.state = 281;
				this.literal();
				this.state = 282;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			case DpmXlParser.ISNULL:
				_localctx = new IsnullExprContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 284;
				this.match(DpmXlParser.ISNULL);
				this.state = 285;
				this.match(DpmXlParser.LPAREN);
				this.state = 286;
				this.expression(0);
				this.state = 287;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public filterOperators(): FilterOperatorsContext {
		let _localctx: FilterOperatorsContext = new FilterOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 32, DpmXlParser.RULE_filterOperators);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 291;
			this.match(DpmXlParser.FILTER);
			this.state = 292;
			this.match(DpmXlParser.LPAREN);
			this.state = 293;
			this.expression(0);
			this.state = 294;
			this.match(DpmXlParser.COMMA);
			this.state = 295;
			this.expression(0);
			this.state = 296;
			this.match(DpmXlParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public timeOperators(): TimeOperatorsContext {
		let _localctx: TimeOperatorsContext = new TimeOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 34, DpmXlParser.RULE_timeOperators);
		let _la: number;
		try {
			_localctx = new TimeShiftFunctionContext(_localctx);
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 298;
			this.match(DpmXlParser.TIME_SHIFT);
			this.state = 299;
			this.match(DpmXlParser.LPAREN);
			this.state = 300;
			this.expression(0);
			this.state = 301;
			this.match(DpmXlParser.COMMA);
			this.state = 302;
			this.match(DpmXlParser.TIME_PERIOD);
			this.state = 303;
			this.match(DpmXlParser.COMMA);
			this.state = 304;
			this.match(DpmXlParser.INTEGER_LITERAL);
			this.state = 307;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === DpmXlParser.COMMA) {
				{
				this.state = 305;
				this.match(DpmXlParser.COMMA);
				this.state = 306;
				this.propertyCode();
				}
			}

			this.state = 309;
			this.match(DpmXlParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public conditionalOperators(): ConditionalOperatorsContext {
		let _localctx: ConditionalOperatorsContext = new ConditionalOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 36, DpmXlParser.RULE_conditionalOperators);
		try {
			_localctx = new NvlFunctionContext(_localctx);
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 311;
			this.match(DpmXlParser.NVL);
			this.state = 312;
			this.match(DpmXlParser.LPAREN);
			this.state = 313;
			this.expression(0);
			this.state = 314;
			this.match(DpmXlParser.COMMA);
			this.state = 315;
			this.expression(0);
			this.state = 316;
			this.match(DpmXlParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public stringOperators(): StringOperatorsContext {
		let _localctx: StringOperatorsContext = new StringOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 38, DpmXlParser.RULE_stringOperators);
		try {
			_localctx = new UnaryStringFunctionContext(_localctx);
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 318;
			this.match(DpmXlParser.LEN);
			this.state = 319;
			this.match(DpmXlParser.LPAREN);
			this.state = 320;
			this.expression(0);
			this.state = 321;
			this.match(DpmXlParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public aggregateOperators(): AggregateOperatorsContext {
		let _localctx: AggregateOperatorsContext = new AggregateOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 40, DpmXlParser.RULE_aggregateOperators);
		let _la: number;
		try {
			_localctx = new CommonAggrOpContext(_localctx);
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 323;
			(_localctx as CommonAggrOpContext)._op = this._input.LT(1);
			_la = this._input.LA(1);
			if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << DpmXlParser.MAX_AGGR) | (1 << DpmXlParser.MIN_AGGR) | (1 << DpmXlParser.SUM) | (1 << DpmXlParser.COUNT) | (1 << DpmXlParser.AVG) | (1 << DpmXlParser.MEDIAN))) !== 0))) {
				(_localctx as CommonAggrOpContext)._op = this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			this.state = 324;
			this.match(DpmXlParser.LPAREN);
			this.state = 325;
			this.expression(0);
			this.state = 327;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === DpmXlParser.GROUP_BY) {
				{
				this.state = 326;
				this.groupingClause();
				}
			}

			this.state = 329;
			this.match(DpmXlParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public groupingClause(): GroupingClauseContext {
		let _localctx: GroupingClauseContext = new GroupingClauseContext(this._ctx, this.state);
		this.enterRule(_localctx, 42, DpmXlParser.RULE_groupingClause);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 331;
			this.match(DpmXlParser.GROUP_BY);
			this.state = 332;
			this.keyNames();
			this.state = 337;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la === DpmXlParser.COMMA) {
				{
				{
				this.state = 333;
				this.match(DpmXlParser.COMMA);
				this.state = 334;
				this.keyNames();
				}
				}
				this.state = 339;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public itemSignature(): ItemSignatureContext {
		let _localctx: ItemSignatureContext = new ItemSignatureContext(this._ctx, this.state);
		this.enterRule(_localctx, 44, DpmXlParser.RULE_itemSignature);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 340;
			this.match(DpmXlParser.ITEM_SIGNATURE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public itemReference(): ItemReferenceContext {
		let _localctx: ItemReferenceContext = new ItemReferenceContext(this._ctx, this.state);
		this.enterRule(_localctx, 46, DpmXlParser.RULE_itemReference);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 342;
			this.match(DpmXlParser.SQUARE_BRACKET_LEFT);
			this.state = 343;
			this.itemSignature();
			this.state = 344;
			this.match(DpmXlParser.SQUARE_BRACKET_RIGHT);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public colElem(): ColElemContext {
		let _localctx: ColElemContext = new ColElemContext(this._ctx, this.state);
		this.enterRule(_localctx, 48, DpmXlParser.RULE_colElem);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 346;
			_la = this._input.LA(1);
			if (!(((((_la - 84)) & ~0x1F) === 0 && ((1 << (_la - 84)) & ((1 << (DpmXlParser.COL - 84)) | (1 << (DpmXlParser.COL_RANGE - 84)) | (1 << (DpmXlParser.COL_ALL - 84)))) !== 0))) {
			this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public sheetElem(): SheetElemContext {
		let _localctx: SheetElemContext = new SheetElemContext(this._ctx, this.state);
		this.enterRule(_localctx, 50, DpmXlParser.RULE_sheetElem);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 348;
			_la = this._input.LA(1);
			if (!(((((_la - 87)) & ~0x1F) === 0 && ((1 << (_la - 87)) & ((1 << (DpmXlParser.SHEET - 87)) | (1 << (DpmXlParser.SHEET_RANGE - 87)) | (1 << (DpmXlParser.SHEET_ALL - 87)))) !== 0))) {
			this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public rowElemSingle(): RowElemSingleContext {
		let _localctx: RowElemSingleContext = new RowElemSingleContext(this._ctx, this.state);
		this.enterRule(_localctx, 52, DpmXlParser.RULE_rowElemSingle);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 350;
			this.match(DpmXlParser.ROW);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public rowElemRange(): RowElemRangeContext {
		let _localctx: RowElemRangeContext = new RowElemRangeContext(this._ctx, this.state);
		this.enterRule(_localctx, 54, DpmXlParser.RULE_rowElemRange);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 352;
			this.match(DpmXlParser.ROW_RANGE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public rowElemAll(): RowElemAllContext {
		let _localctx: RowElemAllContext = new RowElemAllContext(this._ctx, this.state);
		this.enterRule(_localctx, 56, DpmXlParser.RULE_rowElemAll);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 354;
			this.match(DpmXlParser.ROW_ALL);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public rowElem(): RowElemContext {
		let _localctx: RowElemContext = new RowElemContext(this._ctx, this.state);
		this.enterRule(_localctx, 58, DpmXlParser.RULE_rowElem);
		try {
			this.state = 359;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.ROW:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 356;
				this.rowElemSingle();
				}
				break;
			case DpmXlParser.ROW_RANGE:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 357;
				this.rowElemRange();
				}
				break;
			case DpmXlParser.ROW_ALL:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 358;
				this.rowElemAll();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public rowHandler(): RowHandlerContext {
		let _localctx: RowHandlerContext = new RowHandlerContext(this._ctx, this.state);
		this.enterRule(_localctx, 60, DpmXlParser.RULE_rowHandler);
		let _la: number;
		try {
			this.state = 373;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.ROW:
			case DpmXlParser.ROW_RANGE:
			case DpmXlParser.ROW_ALL:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 361;
				this.rowElem();
				}
				break;
			case DpmXlParser.LPAREN:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 362;
				this.match(DpmXlParser.LPAREN);
				this.state = 363;
				this.rowElemSingle();
				this.state = 368;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 364;
					this.match(DpmXlParser.COMMA);
					this.state = 365;
					this.rowElemSingle();
					}
					}
					this.state = 370;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				this.state = 371;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public colHandler(): ColHandlerContext {
		let _localctx: ColHandlerContext = new ColHandlerContext(this._ctx, this.state);
		this.enterRule(_localctx, 62, DpmXlParser.RULE_colHandler);
		let _la: number;
		try {
			this.state = 386;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.COL:
			case DpmXlParser.COL_RANGE:
			case DpmXlParser.COL_ALL:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 375;
				this.colElem();
				}
				break;
			case DpmXlParser.LPAREN:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 376;
				this.match(DpmXlParser.LPAREN);
				this.state = 377;
				this.match(DpmXlParser.COL);
				this.state = 382;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 378;
					this.match(DpmXlParser.COMMA);
					this.state = 379;
					this.match(DpmXlParser.COL);
					}
					}
					this.state = 384;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				this.state = 385;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public sheetHandler(): SheetHandlerContext {
		let _localctx: SheetHandlerContext = new SheetHandlerContext(this._ctx, this.state);
		this.enterRule(_localctx, 64, DpmXlParser.RULE_sheetHandler);
		let _la: number;
		try {
			this.state = 399;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.SHEET:
			case DpmXlParser.SHEET_RANGE:
			case DpmXlParser.SHEET_ALL:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 388;
				this.sheetElem();
				}
				break;
			case DpmXlParser.LPAREN:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 389;
				this.match(DpmXlParser.LPAREN);
				this.state = 390;
				this.match(DpmXlParser.SHEET);
				this.state = 395;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 391;
					this.match(DpmXlParser.COMMA);
					this.state = 392;
					this.match(DpmXlParser.SHEET);
					}
					}
					this.state = 397;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				this.state = 398;
				this.match(DpmXlParser.RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public interval(): IntervalContext {
		let _localctx: IntervalContext = new IntervalContext(this._ctx, this.state);
		this.enterRule(_localctx, 66, DpmXlParser.RULE_interval);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 401;
			this.match(DpmXlParser.INTERVAL);
			this.state = 402;
			this.match(DpmXlParser.COLON);
			this.state = 403;
			this.match(DpmXlParser.BOOLEAN_LITERAL);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public defaultWithLiteral(): DefaultWithLiteralContext {
		let _localctx: DefaultWithLiteralContext = new DefaultWithLiteralContext(this._ctx, this.state);
		this.enterRule(_localctx, 68, DpmXlParser.RULE_defaultWithLiteral);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 405;
			this.match(DpmXlParser.DEFAULT);
			this.state = 406;
			this.match(DpmXlParser.COLON);
			this.state = 407;
			this.literal();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public argument(): ArgumentContext {
		let _localctx: ArgumentContext = new ArgumentContext(this._ctx, this.state);
		this.enterRule(_localctx, 70, DpmXlParser.RULE_argument);
		try {
			this.state = 414;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 27, this._ctx) ) {
			case 1:
				_localctx = new RowArgContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 409;
				this.rowHandler();
				}
				break;

			case 2:
				_localctx = new ColArgContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 410;
				this.colHandler();
				}
				break;

			case 3:
				_localctx = new SheetArgContext(_localctx);
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 411;
				this.sheetHandler();
				}
				break;

			case 4:
				_localctx = new IntervalArgContext(_localctx);
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 412;
				this.interval();
				}
				break;

			case 5:
				_localctx = new DefaultArgContext(_localctx);
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 413;
				this.defaultWithLiteral();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public select(): SelectContext {
		let _localctx: SelectContext = new SelectContext(this._ctx, this.state);
		this.enterRule(_localctx, 72, DpmXlParser.RULE_select);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 416;
			this.selectionStart();
			this.state = 417;
			this.selectOperand();
			this.state = 418;
			this.selectionEnd();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public selectOperand(): SelectOperandContext {
		let _localctx: SelectOperandContext = new SelectOperandContext(this._ctx, this.state);
		this.enterRule(_localctx, 74, DpmXlParser.RULE_selectOperand);
		try {
			this.state = 424;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.LPAREN:
			case DpmXlParser.INTERVAL:
			case DpmXlParser.DEFAULT:
			case DpmXlParser.ROW:
			case DpmXlParser.ROW_RANGE:
			case DpmXlParser.ROW_ALL:
			case DpmXlParser.COL:
			case DpmXlParser.COL_RANGE:
			case DpmXlParser.COL_ALL:
			case DpmXlParser.SHEET:
			case DpmXlParser.SHEET_RANGE:
			case DpmXlParser.SHEET_ALL:
			case DpmXlParser.TABLE_REFERENCE:
			case DpmXlParser.TABLE_GROUP_REFERENCE:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 420;
				this.cellRef();
				}
				break;
			case DpmXlParser.VAR_REFERENCE:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 421;
				this.varRef();
				}
				break;
			case DpmXlParser.OPERATION_REFERENCE:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 422;
				this.operationRef();
				}
				break;
			case DpmXlParser.PRECONDITION_ELEMENT:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 423;
				this.preconditionElem();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varID(): VarIDContext {
		let _localctx: VarIDContext = new VarIDContext(this._ctx, this.state);
		this.enterRule(_localctx, 76, DpmXlParser.RULE_varID);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 426;
			this.selectionStart();
			this.state = 427;
			this.varRef();
			this.state = 428;
			this.selectionEnd();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public cellRef(): CellRefContext {
		let _localctx: CellRefContext = new CellRefContext(this._ctx, this.state);
		this.enterRule(_localctx, 78, DpmXlParser.RULE_cellRef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 430;
			_localctx._address = this.cellAddress();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public preconditionElem(): PreconditionElemContext {
		let _localctx: PreconditionElemContext = new PreconditionElemContext(this._ctx, this.state);
		this.enterRule(_localctx, 80, DpmXlParser.RULE_preconditionElem);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 432;
			this.match(DpmXlParser.PRECONDITION_ELEMENT);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varRef(): VarRefContext {
		let _localctx: VarRefContext = new VarRefContext(this._ctx, this.state);
		this.enterRule(_localctx, 82, DpmXlParser.RULE_varRef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 434;
			this.match(DpmXlParser.VAR_REFERENCE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public operationRef(): OperationRefContext {
		let _localctx: OperationRefContext = new OperationRefContext(this._ctx, this.state);
		this.enterRule(_localctx, 84, DpmXlParser.RULE_operationRef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 436;
			this.match(DpmXlParser.OPERATION_REFERENCE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public cellAddress(): CellAddressContext {
		let _localctx: CellAddressContext = new CellAddressContext(this._ctx, this.state);
		this.enterRule(_localctx, 86, DpmXlParser.RULE_cellAddress);
		let _la: number;
		try {
			this.state = 454;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.TABLE_REFERENCE:
			case DpmXlParser.TABLE_GROUP_REFERENCE:
				_localctx = new TableRefContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 438;
				this.tableReference();
				this.state = 443;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 439;
					this.match(DpmXlParser.COMMA);
					this.state = 440;
					this.argument();
					}
					}
					this.state = 445;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				break;
			case DpmXlParser.LPAREN:
			case DpmXlParser.INTERVAL:
			case DpmXlParser.DEFAULT:
			case DpmXlParser.ROW:
			case DpmXlParser.ROW_RANGE:
			case DpmXlParser.ROW_ALL:
			case DpmXlParser.COL:
			case DpmXlParser.COL_RANGE:
			case DpmXlParser.COL_ALL:
			case DpmXlParser.SHEET:
			case DpmXlParser.SHEET_RANGE:
			case DpmXlParser.SHEET_ALL:
				_localctx = new CompRefContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 446;
				this.argument();
				this.state = 451;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 447;
					this.match(DpmXlParser.COMMA);
					this.state = 448;
					this.argument();
					}
					}
					this.state = 453;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public tableReferenceSingle(): TableReferenceSingleContext {
		let _localctx: TableReferenceSingleContext = new TableReferenceSingleContext(this._ctx, this.state);
		this.enterRule(_localctx, 88, DpmXlParser.RULE_tableReferenceSingle);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 456;
			this.match(DpmXlParser.TABLE_REFERENCE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public tableGroupReference(): TableGroupReferenceContext {
		let _localctx: TableGroupReferenceContext = new TableGroupReferenceContext(this._ctx, this.state);
		this.enterRule(_localctx, 90, DpmXlParser.RULE_tableGroupReference);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 458;
			this.match(DpmXlParser.TABLE_GROUP_REFERENCE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public tableReference(): TableReferenceContext {
		let _localctx: TableReferenceContext = new TableReferenceContext(this._ctx, this.state);
		this.enterRule(_localctx, 92, DpmXlParser.RULE_tableReference);
		try {
			this.state = 462;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.TABLE_REFERENCE:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 460;
				this.tableReferenceSingle();
				}
				break;
			case DpmXlParser.TABLE_GROUP_REFERENCE:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 461;
				this.tableGroupReference();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public clauseOperators(): ClauseOperatorsContext {
		let _localctx: ClauseOperatorsContext = new ClauseOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 94, DpmXlParser.RULE_clauseOperators);
		let _la: number;
		try {
			this.state = 477;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.WHERE:
				_localctx = new WhereExprContext(_localctx);
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 464;
				this.match(DpmXlParser.WHERE);
				this.state = 465;
				this.expression(0);
				}
				break;
			case DpmXlParser.GET:
				_localctx = new GetExprContext(_localctx);
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 466;
				this.match(DpmXlParser.GET);
				this.state = 467;
				this.keyNames();
				}
				break;
			case DpmXlParser.RENAME:
				_localctx = new RenameExprContext(_localctx);
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 468;
				this.match(DpmXlParser.RENAME);
				this.state = 469;
				this.renameClause();
				this.state = 474;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === DpmXlParser.COMMA) {
					{
					{
					this.state = 470;
					this.match(DpmXlParser.COMMA);
					this.state = 471;
					this.renameClause();
					}
					}
					this.state = 476;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public renameClause(): RenameClauseContext {
		let _localctx: RenameClauseContext = new RenameClauseContext(this._ctx, this.state);
		this.enterRule(_localctx, 96, DpmXlParser.RULE_renameClause);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 479;
			this.keyNames();
			this.state = 480;
			this.match(DpmXlParser.TO);
			this.state = 481;
			this.keyNames();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public comparisonOperators(): ComparisonOperatorsContext {
		let _localctx: ComparisonOperatorsContext = new ComparisonOperatorsContext(this._ctx, this.state);
		this.enterRule(_localctx, 98, DpmXlParser.RULE_comparisonOperators);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 483;
			_la = this._input.LA(1);
			if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << DpmXlParser.EQ) | (1 << DpmXlParser.NE) | (1 << DpmXlParser.LT) | (1 << DpmXlParser.LE) | (1 << DpmXlParser.GT) | (1 << DpmXlParser.GE))) !== 0))) {
			this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public literal(): LiteralContext {
		let _localctx: LiteralContext = new LiteralContext(this._ctx, this.state);
		this.enterRule(_localctx, 100, DpmXlParser.RULE_literal);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 485;
			_la = this._input.LA(1);
			if (!(_la === DpmXlParser.BOOLEAN_LITERAL || ((((_la - 64)) & ~0x1F) === 0 && ((1 << (_la - 64)) & ((1 << (DpmXlParser.INTEGER_LITERAL - 64)) | (1 << (DpmXlParser.DECIMAL_LITERAL - 64)) | (1 << (DpmXlParser.PERCENT_LITERAL - 64)) | (1 << (DpmXlParser.STRING_LITERAL - 64)) | (1 << (DpmXlParser.EMPTY_LITERAL - 64)) | (1 << (DpmXlParser.DATE_LITERAL - 64)) | (1 << (DpmXlParser.TIME_INTERVAL_LITERAL - 64)) | (1 << (DpmXlParser.TIME_PERIOD_LITERAL - 64)))) !== 0))) {
			this._errHandler.recoverInline(this);
			} else {
				if (this._input.LA(1) === Token.EOF) {
					this.matchedEOF = true;
				}

				this._errHandler.reportMatch(this);
				this.consume();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public keyNames(): KeyNamesContext {
		let _localctx: KeyNamesContext = new KeyNamesContext(this._ctx, this.state);
		this.enterRule(_localctx, 102, DpmXlParser.RULE_keyNames);
		try {
			this.state = 491;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case DpmXlParser.ROW_HEADING:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 487;
				this.match(DpmXlParser.ROW_HEADING);
				}
				break;
			case DpmXlParser.COL_HEADING:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 488;
				this.match(DpmXlParser.COL_HEADING);
				}
				break;
			case DpmXlParser.SHEET_HEADING:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 489;
				this.match(DpmXlParser.SHEET_HEADING);
				}
				break;
			case DpmXlParser.CODE:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 490;
				this.propertyCode();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public propertyReference(): PropertyReferenceContext {
		let _localctx: PropertyReferenceContext = new PropertyReferenceContext(this._ctx, this.state);
		this.enterRule(_localctx, 104, DpmXlParser.RULE_propertyReference);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 493;
			this.match(DpmXlParser.SQUARE_BRACKET_LEFT);
			this.state = 494;
			this.propertyCode();
			this.state = 495;
			this.match(DpmXlParser.SQUARE_BRACKET_RIGHT);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public propertyCode(): PropertyCodeContext {
		let _localctx: PropertyCodeContext = new PropertyCodeContext(this._ctx, this.state);
		this.enterRule(_localctx, 106, DpmXlParser.RULE_propertyCode);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 497;
			this.match(DpmXlParser.CODE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public temporaryIdentifier(): TemporaryIdentifierContext {
		let _localctx: TemporaryIdentifierContext = new TemporaryIdentifierContext(this._ctx, this.state);
		this.enterRule(_localctx, 108, DpmXlParser.RULE_temporaryIdentifier);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 499;
			this.match(DpmXlParser.CODE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public sempred(_localctx: RuleContext, ruleIndex: number, predIndex: number): boolean {
		switch (ruleIndex) {
		case 10:
			return this.expression_sempred(_localctx as ExpressionContext, predIndex);
		}
		return true;
	}
	private expression_sempred(_localctx: ExpressionContext, predIndex: number): boolean {
		switch (predIndex) {
		case 0:
			return this.precpred(this._ctx, 13);

		case 1:
			return this.precpred(this._ctx, 12);

		case 2:
			return this.precpred(this._ctx, 11);

		case 3:
			return this.precpred(this._ctx, 10);

		case 4:
			return this.precpred(this._ctx, 8);

		case 5:
			return this.precpred(this._ctx, 7);

		case 6:
			return this.precpred(this._ctx, 16);

		case 7:
			return this.precpred(this._ctx, 9);
		}
		return true;
	}

	public static readonly _serializedATN: string =
		"\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x03b\u01F8\x04\x02" +
		"\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04\x07" +
		"\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x04\v\t\v\x04\f\t\f\x04\r\t\r\x04" +
		"\x0E\t\x0E\x04\x0F\t\x0F\x04\x10\t\x10\x04\x11\t\x11\x04\x12\t\x12\x04" +
		"\x13\t\x13\x04\x14\t\x14\x04\x15\t\x15\x04\x16\t\x16\x04\x17\t\x17\x04" +
		"\x18\t\x18\x04\x19\t\x19\x04\x1A\t\x1A\x04\x1B\t\x1B\x04\x1C\t\x1C\x04" +
		"\x1D\t\x1D\x04\x1E\t\x1E\x04\x1F\t\x1F\x04 \t \x04!\t!\x04\"\t\"\x04#" +
		"\t#\x04$\t$\x04%\t%\x04&\t&\x04\'\t\'\x04(\t(\x04)\t)\x04*\t*\x04+\t+" +
		"\x04,\t,\x04-\t-\x04.\t.\x04/\t/\x040\t0\x041\t1\x042\t2\x043\t3\x044" +
		"\t4\x045\t5\x046\t6\x047\t7\x048\t8\x03\x02\x03\x02\x03\x02\x03\x02\x05" +
		"\x02u\n\x02\x05\x02w\n\x02\x03\x02\x03\x02\x03\x03\x03\x03\x03\x03\x06" +
		"\x03~\n\x03\r\x03\x0E\x03\x7F\x03\x04\x03\x04\x05\x04\x84\n\x04\x03\x05" +
		"\x03\x05\x05\x05\x88\n\x05\x03\x06\x03\x06\x03\x06\x03\x06\x03\x06\x03" +
		"\x06\x05\x06\x90\n\x06\x03\x07\x03\x07\x03\b\x03\b\x03\t\x03\t\x03\t\x03" +
		"\t\x03\n\x03\n\x03\n\x03\n\x03\v\x03\v\x03\v\x03\v\x03\f\x03\f\x03\f\x03" +
		"\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03" +
		"\f\x03\f\x03\f\x03\f\x05\f\xB5\n\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f" +
		"\x03\f\x05\f\xBE\n\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03" +
		"\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03" +
		"\f\x03\f\x03\f\x03\f\x03\f\x03\f\x03\f\x07\f\xDB\n\f\f\f\x0E\f\xDE\v\f" +
		"\x03\r\x03\r\x03\r\x03\r\x03\x0E\x03\x0E\x03\x0E\x07\x0E\xE7\n\x0E\f\x0E" +
		"\x0E\x0E\xEA\v\x0E\x03\x0E\x03\x0E\x03\x0E\x07\x0E\xEF\n\x0E\f\x0E\x0E" +
		"\x0E\xF2\v\x0E\x05\x0E\xF4\n\x0E\x03\x0F\x03\x0F\x03\x0F\x03\x0F\x03\x0F" +
		"\x03\x0F\x03\x0F\x05\x0F\xFD\n\x0F\x03\x10\x03\x10\x03\x10\x03\x10\x03" +
		"\x10\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10\x03" +
		"\x10\x03\x10\x03\x10\x03\x10\x06\x10\u0110\n\x10\r\x10\x0E\x10\u0111\x03" +
		"\x10\x03\x10\x05\x10\u0116\n\x10\x03\x11\x03\x11\x03\x11\x03\x11\x03\x11" +
		"\x03\x11\x03\x11\x03\x11\x03\x11\x03\x11\x03\x11\x03\x11\x05\x11\u0124" +
		"\n\x11\x03\x12\x03\x12\x03\x12\x03\x12\x03\x12\x03\x12\x03\x12\x03\x13" +
		"\x03\x13\x03\x13\x03\x13\x03\x13\x03\x13\x03\x13\x03\x13\x03\x13\x05\x13" +
		"\u0136\n\x13\x03\x13\x03\x13\x03\x14\x03\x14\x03\x14\x03\x14\x03\x14\x03" +
		"\x14\x03\x14\x03\x15\x03\x15\x03\x15\x03\x15\x03\x15\x03\x16\x03\x16\x03" +
		"\x16\x03\x16\x05\x16\u014A\n\x16\x03\x16\x03\x16\x03\x17\x03\x17\x03\x17" +
		"\x03\x17\x07\x17\u0152\n\x17\f\x17\x0E\x17\u0155\v\x17\x03\x18\x03\x18" +
		"\x03\x19\x03\x19\x03\x19\x03\x19\x03\x1A\x03\x1A\x03\x1B\x03\x1B\x03\x1C" +
		"\x03\x1C\x03\x1D\x03\x1D\x03\x1E\x03\x1E\x03\x1F\x03\x1F\x03\x1F\x05\x1F" +
		"\u016A\n\x1F\x03 \x03 \x03 \x03 \x03 \x07 \u0171\n \f \x0E \u0174\v \x03" +
		" \x03 \x05 \u0178\n \x03!\x03!\x03!\x03!\x03!\x07!\u017F\n!\f!\x0E!\u0182" +
		"\v!\x03!\x05!\u0185\n!\x03\"\x03\"\x03\"\x03\"\x03\"\x07\"\u018C\n\"\f" +
		"\"\x0E\"\u018F\v\"\x03\"\x05\"\u0192\n\"\x03#\x03#\x03#\x03#\x03$\x03" +
		"$\x03$\x03$\x03%\x03%\x03%\x03%\x03%\x05%\u01A1\n%\x03&\x03&\x03&\x03" +
		"&\x03\'\x03\'\x03\'\x03\'\x05\'\u01AB\n\'\x03(\x03(\x03(\x03(\x03)\x03" +
		")\x03*\x03*\x03+\x03+\x03,\x03,\x03-\x03-\x03-\x07-\u01BC\n-\f-\x0E-\u01BF" +
		"\v-\x03-\x03-\x03-\x07-\u01C4\n-\f-\x0E-\u01C7\v-\x05-\u01C9\n-\x03.\x03" +
		".\x03/\x03/\x030\x030\x050\u01D1\n0\x031\x031\x031\x031\x031\x031\x03" +
		"1\x031\x071\u01DB\n1\f1\x0E1\u01DE\v1\x051\u01E0\n1\x032\x032\x032\x03" +
		"2\x033\x033\x034\x034\x035\x035\x035\x035\x055\u01EE\n5\x036\x036\x03" +
		"6\x036\x037\x037\x038\x038\x038\x02\x02\x03\x169\x02\x02\x04\x02\x06\x02" +
		"\b\x02\n\x02\f\x02\x0E\x02\x10\x02\x12\x02\x14\x02\x16\x02\x18\x02\x1A" +
		"\x02\x1C\x02\x1E\x02 \x02\"\x02$\x02&\x02(\x02*\x02,\x02.\x020\x022\x02" +
		"4\x026\x028\x02:\x02<\x02>\x02@\x02B\x02D\x02F\x02H\x02J\x02L\x02N\x02" +
		"P\x02R\x02T\x02V\x02X\x02Z\x02\\\x02^\x02`\x02b\x02d\x02f\x02h\x02j\x02" +
		"l\x02n\x02\x02\r\x03\x02\x12\x13\x03\x02\x14\x15\x03\x02\x05\x06\x04\x02" +
		"\x1D\x1D\x1F!\x03\x02\"#\x03\x02$%\x03\x02\x16\x1B\x03\x02VX\x03\x02Y" +
		"[\x03\x02\n\x0F\x05\x02\x03\x03BFHJ\x02\u0201\x02p\x03\x02\x02\x02\x04" +
		"}\x03\x02\x02\x02\x06\x83\x03\x02\x02\x02\b\x87\x03\x02\x02\x02\n\x8F" +
		"\x03\x02\x02\x02\f\x91\x03\x02\x02\x02\x0E\x93\x03\x02\x02\x02\x10\x95" +
		"\x03\x02\x02\x02\x12\x99\x03\x02\x02\x02\x14\x9D\x03\x02\x02\x02\x16\xBD" +
		"\x03\x02\x02\x02\x18\xDF\x03\x02\x02\x02\x1A\xF3\x03\x02\x02\x02\x1C\xFC" +
		"\x03\x02\x02\x02\x1E\u0115\x03\x02\x02\x02 \u0123\x03\x02\x02\x02\"\u0125" +
		"\x03\x02\x02\x02$\u012C\x03\x02\x02\x02&\u0139\x03\x02\x02\x02(\u0140" +
		"\x03\x02\x02\x02*\u0145\x03\x02\x02\x02,\u014D\x03\x02\x02\x02.\u0156" +
		"\x03\x02\x02\x020\u0158\x03\x02\x02\x022\u015C\x03\x02\x02\x024\u015E" +
		"\x03\x02\x02\x026\u0160\x03\x02\x02\x028\u0162\x03\x02\x02\x02:\u0164" +
		"\x03\x02\x02\x02<\u0169\x03\x02\x02\x02>\u0177\x03\x02\x02\x02@\u0184" +
		"\x03\x02\x02\x02B\u0191\x03\x02\x02\x02D\u0193\x03\x02\x02\x02F\u0197" +
		"\x03\x02\x02\x02H\u01A0\x03\x02\x02\x02J\u01A2\x03\x02\x02\x02L\u01AA" +
		"\x03\x02\x02\x02N\u01AC\x03\x02\x02\x02P\u01B0\x03\x02\x02\x02R\u01B2" +
		"\x03\x02\x02\x02T\u01B4\x03\x02\x02\x02V\u01B6\x03\x02\x02\x02X\u01C8" +
		"\x03\x02\x02\x02Z\u01CA\x03\x02\x02\x02\\\u01CC\x03\x02\x02\x02^\u01D0" +
		"\x03\x02\x02\x02`\u01DF\x03\x02\x02\x02b\u01E1\x03\x02\x02\x02d\u01E5" +
		"\x03\x02\x02\x02f\u01E7\x03\x02\x02\x02h\u01ED\x03\x02\x02\x02j\u01EF" +
		"\x03\x02\x02\x02l\u01F3\x03\x02\x02\x02n\u01F5\x03\x02\x02\x02pv\x05\x06" +
		"\x04\x02qr\x07A\x02\x02rw\x05\x04\x03\x02su\x07A\x02\x02ts\x03\x02\x02" +
		"\x02tu\x03\x02\x02\x02uw\x03\x02\x02\x02vq\x03\x02\x02\x02vt\x03\x02\x02" +
		"\x02wx\x03\x02\x02\x02xy\x07\x02\x02\x03y\x03\x03\x02\x02\x02z{\x05\x06" +
		"\x04\x02{|\x07A\x02\x02|~\x03\x02\x02\x02}z\x03\x02\x02\x02~\x7F\x03\x02" +
		"\x02\x02\x7F}\x03\x02\x02\x02\x7F\x80\x03\x02\x02\x02\x80\x05\x03\x02" +
		"\x02\x02\x81\x84\x05\n\x06\x02\x82\x84\x05\x12\n\x02\x83\x81\x03\x02\x02" +
		"\x02\x83\x82\x03\x02\x02\x02\x84\x07\x03\x02\x02\x02\x85\x88\x05\x14\v" +
		"\x02\x86\x88\x05\n\x06\x02\x87\x85\x03\x02\x02\x02\x87\x86\x03\x02\x02" +
		"\x02\x88\t\x03\x02\x02\x02\x89\x90\x05\x16\f\x02\x8A\x8B\x07\x11\x02\x02" +
		"\x8B\x8C\x05\x10\t\x02\x8C\x8D\x07(\x02\x02\x8D\x8E\x05\x16\f\x02\x8E" +
		"\x90\x03\x02\x02\x02\x8F\x89\x03\x02\x02\x02\x8F\x8A\x03\x02\x02\x02\x90" +
		"\v\x03\x02\x02\x02\x91\x92\x07+\x02\x02\x92\r\x03\x02\x02\x02\x93\x94" +
		"\x07,\x02\x02\x94\x0F\x03\x02\x02\x02\x95\x96\x05\f\x07\x02\x96\x97\x05" +
		"P)\x02\x97\x98\x05\x0E\b\x02\x98\x11\x03\x02\x02\x02\x99\x9A\x05n8\x02" +
		"\x9A\x9B\x07\b\x02\x02\x9B\x9C\x05\b\x05\x02\x9C\x13\x03\x02\x02\x02\x9D" +
		"\x9E\x05N(\x02\x9E\x9F\x07\t\x02\x02\x9F\xA0\x05\n\x06\x02\xA0\x15\x03" +
		"\x02\x02\x02\xA1\xA2\b\f\x01\x02\xA2\xA3\x07)\x02\x02\xA3\xA4\x05\x16" +
		"\f\x02\xA4\xA5\x07*\x02\x02\xA5\xBE\x03\x02\x02\x02\xA6\xBE\x05\x1C\x0F" +
		"\x02\xA7\xA8\t\x02\x02\x02\xA8\xBE\x05\x16\f\x11\xA9\xAA\x07\x07\x02\x02" +
		"\xAA\xAB\x07)\x02\x02\xAB\xAC\x05\x16\f\x02\xAC\xAD\x07*\x02\x02\xAD\xBE" +
		"\x03\x02\x02\x02\xAE\xAF\x07/\x02\x02\xAF\xB0\x05\x16\f\x02\xB0\xB1\x07" +
		"1\x02\x02\xB1\xB4\x05\x16\f\x02\xB2\xB3\x072\x02\x02\xB3\xB5\x05\x16\f" +
		"\x02\xB4\xB2\x03\x02\x02\x02\xB4\xB5\x03\x02\x02\x02\xB5\xB6\x03\x02\x02" +
		"\x02\xB6\xB7\x070\x02\x02\xB7\xBE\x03\x02\x02\x02\xB8\xBE\x050\x19\x02" +
		"\xB9\xBE\x05j6\x02\xBA\xBE\x05h5\x02\xBB\xBE\x05f4\x02\xBC\xBE\x05J&\x02" +
		"\xBD\xA1\x03\x02\x02\x02\xBD\xA6\x03\x02\x02\x02\xBD\xA7\x03\x02\x02\x02" +
		"\xBD\xA9\x03\x02\x02\x02\xBD\xAE\x03\x02\x02\x02\xBD\xB8\x03\x02\x02\x02" +
		"\xBD\xB9\x03\x02\x02\x02\xBD\xBA\x03\x02\x02\x02\xBD\xBB\x03\x02\x02\x02" +
		"\xBD\xBC\x03\x02\x02\x02\xBE\xDC\x03\x02\x02\x02\xBF\xC0\f\x0F\x02\x02" +
		"\xC0\xC1\t\x03\x02\x02\xC1\xDB\x05\x16\f\x10\xC2\xC3\f\x0E\x02\x02\xC3" +
		"\xC4\t\x02\x02\x02\xC4\xDB\x05\x16\f\x0F\xC5\xC6\f\r\x02\x02\xC6\xC7\x07" +
		";\x02\x02\xC7\xDB\x05\x16\f\x0E\xC8\xC9\f\f\x02\x02\xC9\xCA\x05d3\x02" +
		"\xCA\xCB\x05\x16\f\r\xCB\xDB\x03\x02\x02\x02\xCC\xCD\f\n\x02\x02\xCD\xCE" +
		"\x07\x04\x02\x02\xCE\xDB\x05\x16\f\v\xCF\xD0\f\t\x02\x02\xD0\xD1\t\x04" +
		"\x02\x02\xD1\xDB\x05\x16\f\n\xD2\xD3\f\x12\x02\x02\xD3\xD4\x07-\x02\x02" +
		"\xD4\xD5\x05`1\x02\xD5\xD6\x07.\x02\x02\xD6\xDB\x03\x02\x02\x02\xD7\xD8" +
		"\f\v\x02\x02\xD8\xD9\x07&\x02\x02\xD9\xDB\x05\x18\r\x02\xDA\xBF\x03\x02" +
		"\x02\x02\xDA\xC2\x03\x02\x02\x02\xDA\xC5\x03\x02\x02\x02\xDA\xC8\x03\x02" +
		"\x02\x02\xDA\xCC\x03\x02\x02\x02\xDA\xCF\x03\x02\x02\x02\xDA\xD2\x03\x02" +
		"\x02\x02\xDA\xD7\x03\x02\x02\x02\xDB\xDE\x03\x02\x02\x02\xDC\xDA\x03\x02" +
		"\x02\x02\xDC\xDD\x03\x02\x02\x02\xDD\x17\x03\x02\x02\x02\xDE\xDC\x03\x02" +
		"\x02\x02\xDF\xE0\x05\f\x07\x02\xE0\xE1\x05\x1A\x0E\x02\xE1\xE2\x05\x0E" +
		"\b\x02\xE2\x19\x03\x02\x02\x02\xE3\xE8\x050\x19\x02\xE4\xE5\x07\'\x02" +
		"\x02\xE5\xE7\x050\x19\x02\xE6\xE4\x03\x02\x02\x02\xE7\xEA\x03\x02\x02" +
		"\x02\xE8\xE6\x03\x02\x02\x02\xE8\xE9\x03\x02\x02\x02\xE9\xF4\x03\x02\x02" +
		"\x02\xEA\xE8\x03\x02\x02\x02\xEB\xF0\x05f4\x02\xEC\xED\x07\'\x02\x02\xED" +
		"\xEF\x05f4\x02\xEE\xEC\x03\x02\x02\x02\xEF\xF2\x03\x02\x02\x02\xF0\xEE" +
		"\x03\x02\x02\x02\xF0\xF1\x03\x02\x02\x02\xF1\xF4\x03\x02\x02\x02\xF2\xF0" +
		"\x03\x02\x02\x02\xF3\xE3\x03\x02\x02\x02\xF3\xEB\x03\x02\x02\x02\xF4\x1B" +
		"\x03\x02\x02\x02\xF5\xFD\x05*\x16\x02\xF6\xFD\x05\x1E\x10\x02\xF7\xFD" +
		"\x05 \x11\x02\xF8\xFD\x05\"\x12\x02\xF9\xFD\x05&\x14\x02\xFA\xFD\x05$" +
		"\x13\x02\xFB\xFD\x05(\x15\x02\xFC\xF5\x03\x02\x02\x02\xFC\xF6\x03\x02" +
		"\x02\x02\xFC\xF7\x03\x02\x02\x02\xFC\xF8\x03\x02\x02\x02\xFC\xF9\x03\x02" +
		"\x02\x02\xFC\xFA\x03\x02\x02\x02\xFC\xFB\x03\x02\x02\x02\xFD\x1D\x03\x02" +
		"\x02\x02\xFE\xFF\t\x05\x02\x02\xFF\u0100\x07)\x02\x02\u0100\u0101\x05" +
		"\x16\f\x02\u0101\u0102\x07*\x02\x02\u0102\u0116\x03\x02\x02\x02\u0103" +
		"\u0104\t\x06\x02\x02\u0104\u0105\x07)\x02\x02\u0105\u0106\x05\x16\f\x02" +
		"\u0106\u0107\x07\'\x02\x02\u0107\u0108\x05\x16\f\x02\u0108\u0109\x07*" +
		"\x02\x02\u0109\u0116\x03\x02\x02\x02\u010A\u010B\t\x07\x02\x02\u010B\u010C" +
		"\x07)\x02\x02\u010C\u010F\x05\x16\f\x02\u010D\u010E\x07\'\x02\x02\u010E" +
		"\u0110\x05\x16\f\x02\u010F\u010D\x03\x02\x02\x02\u0110\u0111\x03\x02\x02" +
		"\x02\u0111\u010F\x03\x02\x02\x02\u0111\u0112\x03\x02\x02\x02\u0112\u0113" +
		"\x03\x02\x02\x02\u0113\u0114\x07*\x02\x02\u0114\u0116\x03\x02\x02\x02" +
		"\u0115\xFE\x03\x02\x02\x02\u0115\u0103\x03\x02\x02\x02\u0115\u010A\x03" +
		"\x02\x02\x02\u0116\x1F\x03\x02\x02\x02\u0117\u0118\x07\x10\x02\x02\u0118" +
		"\u0119\x07)\x02\x02\u0119\u011A\x05\x16\f\x02\u011A\u011B\x07\'\x02\x02" +
		"\u011B\u011C\x05f4\x02\u011C\u011D\x07*\x02\x02\u011D\u0124\x03\x02\x02" +
		"\x02\u011E\u011F\x07\x1E\x02\x02\u011F\u0120\x07)\x02\x02\u0120\u0121" +
		"\x05\x16\f\x02\u0121\u0122\x07*\x02\x02\u0122\u0124\x03\x02\x02\x02\u0123" +
		"\u0117\x03\x02\x02\x02\u0123\u011E\x03\x02\x02\x02\u0124!\x03\x02\x02" +
		"\x02\u0125\u0126\x074\x02\x02\u0126\u0127\x07)\x02\x02\u0127\u0128\x05" +
		"\x16\f\x02\u0128\u0129\x07\'\x02\x02\u0129\u012A\x05\x16\f\x02\u012A\u012B" +
		"\x07*\x02\x02\u012B#\x03\x02\x02\x02\u012C\u012D\x079\x02\x02\u012D\u012E" +
		"\x07)\x02\x02\u012E\u012F\x05\x16\f\x02\u012F\u0130\x07\'\x02\x02\u0130" +
		"\u0131\x07?\x02\x02\u0131\u0132\x07\'\x02\x02\u0132\u0135\x07B\x02\x02" +
		"\u0133\u0134\x07\'\x02\x02\u0134\u0136\x05l7\x02\u0135\u0133\x03\x02\x02" +
		"\x02\u0135\u0136\x03\x02\x02\x02\u0136\u0137\x03\x02\x02\x02\u0137\u0138" +
		"\x07*\x02\x02\u0138%\x03\x02\x02\x02\u0139\u013A\x073\x02\x02\u013A\u013B" +
		"\x07)\x02\x02\u013B\u013C\x05\x16\f\x02\u013C\u013D\x07\'\x02\x02\u013D" +
		"\u013E\x05\x16\f\x02\u013E\u013F\x07*\x02\x02\u013F\'\x03\x02\x02\x02" +
		"\u0140\u0141\x07:\x02\x02\u0141\u0142\x07)\x02\x02\u0142\u0143\x05\x16" +
		"\f\x02\u0143\u0144\x07*\x02\x02\u0144)\x03\x02\x02\x02\u0145\u0146\t\b" +
		"\x02\x02\u0146\u0147\x07)\x02\x02\u0147\u0149\x05\x16\f\x02\u0148\u014A" +
		"\x05,\x17\x02\u0149\u0148\x03\x02\x02\x02\u0149\u014A\x03\x02\x02\x02" +
		"\u014A\u014B\x03\x02\x02\x02\u014B\u014C\x07*\x02\x02\u014C+\x03\x02\x02" +
		"\x02\u014D\u014E\x07\x1C\x02\x02\u014E\u0153\x05h5\x02\u014F\u0150\x07" +
		"\'\x02\x02\u0150\u0152\x05h5\x02\u0151\u014F\x03\x02\x02\x02\u0152\u0155" +
		"\x03\x02\x02\x02\u0153\u0151\x03\x02\x02\x02\u0153\u0154\x03\x02\x02\x02" +
		"\u0154-\x03\x02\x02\x02\u0155\u0153\x03\x02\x02\x02\u0156\u0157\x07@\x02" +
		"\x02\u0157/\x03\x02\x02\x02\u0158\u0159\x07-\x02\x02\u0159\u015A\x05." +
		"\x18\x02\u015A\u015B\x07.\x02\x02\u015B1\x03\x02\x02\x02\u015C\u015D\t" +
		"\t\x02\x02\u015D3\x03\x02\x02\x02\u015E\u015F\t\n\x02\x02\u015F5\x03\x02" +
		"\x02\x02\u0160\u0161\x07S\x02\x02\u01617\x03\x02\x02\x02\u0162\u0163\x07" +
		"T\x02\x02\u01639\x03\x02\x02\x02\u0164\u0165\x07U\x02\x02\u0165;\x03\x02" +
		"\x02\x02\u0166\u016A\x056\x1C\x02\u0167\u016A\x058\x1D\x02\u0168\u016A" +
		"\x05:\x1E\x02\u0169\u0166\x03\x02\x02\x02\u0169\u0167\x03\x02\x02\x02" +
		"\u0169\u0168\x03\x02\x02\x02\u016A=\x03\x02\x02\x02\u016B\u0178\x05<\x1F" +
		"\x02\u016C\u016D\x07)\x02\x02\u016D\u0172\x056\x1C\x02\u016E\u016F\x07" +
		"\'\x02\x02\u016F\u0171\x056\x1C\x02\u0170\u016E\x03\x02\x02\x02\u0171" +
		"\u0174\x03\x02\x02\x02\u0172\u0170\x03\x02\x02\x02\u0172\u0173\x03\x02" +
		"\x02\x02\u0173\u0175\x03\x02\x02\x02\u0174\u0172\x03\x02\x02\x02\u0175" +
		"\u0176\x07*\x02\x02\u0176\u0178\x03\x02\x02\x02\u0177\u016B\x03\x02\x02" +
		"\x02\u0177\u016C\x03\x02\x02\x02\u0178?\x03\x02\x02\x02\u0179\u0185\x05" +
		"2\x1A\x02\u017A\u017B\x07)\x02\x02\u017B\u0180\x07V\x02\x02\u017C\u017D" +
		"\x07\'\x02\x02\u017D\u017F\x07V\x02\x02\u017E\u017C\x03\x02\x02\x02\u017F" +
		"\u0182\x03\x02\x02\x02\u0180\u017E\x03\x02\x02\x02\u0180\u0181\x03\x02" +
		"\x02\x02\u0181\u0183\x03\x02\x02\x02\u0182\u0180\x03\x02\x02\x02\u0183" +
		"\u0185\x07*\x02\x02\u0184\u0179\x03\x02\x02\x02\u0184\u017A\x03\x02\x02" +
		"\x02\u0185A\x03\x02\x02\x02\u0186\u0192\x054\x1B\x02\u0187\u0188\x07)" +
		"\x02\x02\u0188\u018D\x07Y\x02\x02\u0189\u018A\x07\'\x02\x02\u018A\u018C" +
		"\x07Y\x02\x02\u018B\u0189\x03\x02\x02\x02\u018C\u018F\x03\x02\x02\x02" +
		"\u018D\u018B\x03\x02\x02\x02\u018D\u018E\x03\x02\x02\x02\u018E\u0190\x03" +
		"\x02\x02\x02\u018F\u018D\x03\x02\x02\x02\u0190\u0192\x07*\x02\x02\u0191" +
		"\u0186\x03\x02\x02\x02\u0191\u0187\x03\x02\x02\x02\u0192C\x03\x02\x02" +
		"\x02\u0193\u0194\x07L\x02\x02\u0194\u0195\x07(\x02\x02\u0195\u0196\x07" +
		"\x03\x02\x02\u0196E\x03\x02\x02\x02\u0197\u0198\x07M\x02\x02\u0198\u0199" +
		"\x07(\x02\x02\u0199\u019A\x05f4\x02\u019AG\x03\x02\x02\x02\u019B\u01A1" +
		"\x05> \x02\u019C\u01A1\x05@!\x02\u019D\u01A1\x05B\"\x02\u019E\u01A1\x05" +
		"D#\x02\u019F\u01A1\x05F$\x02\u01A0\u019B\x03\x02\x02\x02\u01A0\u019C\x03" +
		"\x02\x02\x02\u01A0\u019D\x03\x02\x02\x02\u01A0\u019E\x03\x02\x02\x02\u01A0" +
		"\u019F\x03\x02\x02\x02\u01A1I\x03\x02\x02\x02\u01A2\u01A3\x05\f\x07\x02" +
		"\u01A3\u01A4\x05L\'\x02\u01A4\u01A5\x05\x0E\b\x02\u01A5K\x03\x02\x02\x02" +
		"\u01A6\u01AB\x05P)\x02\u01A7\u01AB\x05T+\x02\u01A8\u01AB\x05V,\x02\u01A9" +
		"\u01AB\x05R*\x02\u01AA\u01A6\x03\x02\x02\x02\u01AA\u01A7\x03\x02\x02\x02" +
		"\u01AA\u01A8\x03\x02\x02\x02\u01AA\u01A9\x03\x02\x02\x02\u01ABM\x03\x02" +
		"\x02\x02\u01AC\u01AD\x05\f\x07\x02\u01AD\u01AE\x05T+\x02\u01AE\u01AF\x05" +
		"\x0E\b\x02\u01AFO\x03\x02\x02\x02\u01B0\u01B1\x05X-\x02\u01B1Q\x03\x02" +
		"\x02\x02\u01B2\u01B3\x07`\x02\x02\u01B3S\x03\x02\x02\x02\u01B4\u01B5\x07" +
		"^\x02\x02\u01B5U\x03\x02\x02\x02\u01B6\u01B7\x07_\x02\x02\u01B7W\x03\x02" +
		"\x02\x02\u01B8\u01BD\x05^0\x02\u01B9\u01BA\x07\'\x02\x02\u01BA\u01BC\x05" +
		"H%\x02\u01BB\u01B9\x03\x02\x02\x02\u01BC\u01BF\x03\x02\x02\x02\u01BD\u01BB" +
		"\x03\x02\x02\x02\u01BD\u01BE\x03\x02\x02\x02\u01BE\u01C9\x03\x02\x02\x02" +
		"\u01BF\u01BD\x03\x02\x02\x02\u01C0\u01C5\x05H%\x02\u01C1\u01C2\x07\'\x02" +
		"\x02\u01C2\u01C4\x05H%\x02\u01C3\u01C1\x03\x02\x02\x02\u01C4\u01C7\x03" +
		"\x02\x02\x02\u01C5\u01C3\x03\x02\x02\x02\u01C5\u01C6\x03\x02\x02\x02\u01C6" +
		"\u01C9\x03\x02\x02\x02\u01C7\u01C5\x03\x02\x02\x02\u01C8\u01B8\x03\x02" +
		"\x02\x02\u01C8\u01C0\x03\x02\x02\x02\u01C9Y\x03\x02\x02\x02\u01CA\u01CB" +
		"\x07\\\x02\x02\u01CB[\x03\x02\x02\x02\u01CC\u01CD\x07]\x02\x02\u01CD]" +
		"\x03\x02\x02\x02\u01CE\u01D1\x05Z.\x02\u01CF\u01D1\x05\\/\x02\u01D0\u01CE" +
		"\x03\x02\x02\x02\u01D0\u01CF\x03\x02\x02\x02\u01D1_\x03\x02\x02\x02\u01D2" +
		"\u01D3\x075\x02\x02\u01D3\u01E0\x05\x16\f\x02\u01D4\u01D5\x076\x02\x02" +
		"\u01D5\u01E0\x05h5\x02\u01D6\u01D7\x077\x02\x02\u01D7\u01DC\x05b2\x02" +
		"\u01D8\u01D9\x07\'\x02\x02\u01D9\u01DB\x05b2\x02\u01DA\u01D8\x03\x02\x02" +
		"\x02\u01DB\u01DE\x03\x02\x02\x02\u01DC\u01DA\x03\x02\x02\x02\u01DC\u01DD" +
		"\x03\x02\x02\x02\u01DD\u01E0\x03\x02\x02\x02\u01DE\u01DC\x03\x02\x02\x02" +
		"\u01DF\u01D2\x03\x02\x02\x02\u01DF\u01D4\x03\x02\x02\x02\u01DF\u01D6\x03" +
		"\x02\x02\x02\u01E0a\x03\x02\x02\x02\u01E1\u01E2\x05h5\x02\u01E2\u01E3" +
		"\x078\x02\x02\u01E3\u01E4\x05h5\x02\u01E4c\x03\x02\x02\x02\u01E5\u01E6" +
		"\t\v\x02\x02\u01E6e\x03\x02\x02\x02\u01E7\u01E8\t\f\x02\x02\u01E8g\x03" +
		"\x02\x02\x02\u01E9\u01EE\x07<\x02\x02\u01EA\u01EE\x07=\x02\x02\u01EB\u01EE" +
		"\x07>\x02\x02\u01EC\u01EE\x05l7\x02\u01ED\u01E9\x03\x02\x02\x02\u01ED" +
		"\u01EA\x03\x02\x02\x02\u01ED\u01EB\x03\x02\x02\x02\u01ED\u01EC\x03\x02" +
		"\x02\x02\u01EEi\x03\x02\x02\x02\u01EF\u01F0\x07-\x02\x02\u01F0\u01F1\x05" +
		"l7\x02\u01F1\u01F2\x07.\x02\x02\u01F2k\x03\x02\x02\x02\u01F3\u01F4\x07" +
		"G\x02\x02\u01F4m\x03\x02\x02\x02\u01F5\u01F6\x07G\x02\x02\u01F6o\x03\x02" +
		"\x02\x02&tv\x7F\x83\x87\x8F\xB4\xBD\xDA\xDC\xE8\xF0\xF3\xFC\u0111\u0115" +
		"\u0123\u0135\u0149\u0153\u0169\u0172\u0177\u0180\u0184\u018D\u0191\u01A0" +
		"\u01AA\u01BD\u01C5\u01C8\u01D0\u01DC\u01DF\u01ED";
	public static __ATN: ATN;
	public static get _ATN(): ATN {
		if (!DpmXlParser.__ATN) {
			DpmXlParser.__ATN = new ATNDeserializer().deserialize(Utils.toCharArray(DpmXlParser._serializedATN));
		}

		return DpmXlParser.__ATN;
	}

}

export class RootContext extends ParserRuleContext {
	public statement(): StatementContext {
		return this.getRuleContext(0, StatementContext);
	}
	public EOF(): TerminalNode { return this.getToken(DpmXlParser.EOF, 0); }
	public EOL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.EOL, 0); }
	public statements(): StatementsContext | undefined {
		return this.tryGetRuleContext(0, StatementsContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_root; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRoot) {
			listener.enterRoot(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRoot) {
			listener.exitRoot(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRoot) {
			return visitor.visitRoot(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class StatementsContext extends ParserRuleContext {
	public statement(): StatementContext[];
	public statement(i: number): StatementContext;
	public statement(i?: number): StatementContext | StatementContext[] {
		if (i === undefined) {
			return this.getRuleContexts(StatementContext);
		} else {
			return this.getRuleContext(i, StatementContext);
		}
	}
	public EOL(): TerminalNode[];
	public EOL(i: number): TerminalNode;
	public EOL(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.EOL);
		} else {
			return this.getToken(DpmXlParser.EOL, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_statements; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterStatements) {
			listener.enterStatements(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitStatements) {
			listener.exitStatements(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitStatements) {
			return visitor.visitStatements(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class StatementContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_statement; }
	public copyFrom(ctx: StatementContext): void {
		super.copyFrom(ctx);
	}
}
export class ExprWithoutAssignmentContext extends StatementContext {
	public expressionWithoutAssignment(): ExpressionWithoutAssignmentContext {
		return this.getRuleContext(0, ExpressionWithoutAssignmentContext);
	}
	constructor(ctx: StatementContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterExprWithoutAssignment) {
			listener.enterExprWithoutAssignment(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitExprWithoutAssignment) {
			listener.exitExprWithoutAssignment(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitExprWithoutAssignment) {
			return visitor.visitExprWithoutAssignment(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class AssignmentExprContext extends StatementContext {
	public temporaryAssignmentExpression(): TemporaryAssignmentExpressionContext {
		return this.getRuleContext(0, TemporaryAssignmentExpressionContext);
	}
	constructor(ctx: StatementContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterAssignmentExpr) {
			listener.enterAssignmentExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitAssignmentExpr) {
			listener.exitAssignmentExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitAssignmentExpr) {
			return visitor.visitAssignmentExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PersistentExpressionContext extends ParserRuleContext {
	public persistentAssignmentExpression(): PersistentAssignmentExpressionContext | undefined {
		return this.tryGetRuleContext(0, PersistentAssignmentExpressionContext);
	}
	public expressionWithoutAssignment(): ExpressionWithoutAssignmentContext | undefined {
		return this.tryGetRuleContext(0, ExpressionWithoutAssignmentContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_persistentExpression; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPersistentExpression) {
			listener.enterPersistentExpression(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPersistentExpression) {
			listener.exitPersistentExpression(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPersistentExpression) {
			return visitor.visitPersistentExpression(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExpressionWithoutAssignmentContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_expressionWithoutAssignment; }
	public copyFrom(ctx: ExpressionWithoutAssignmentContext): void {
		super.copyFrom(ctx);
	}
}
export class ExprWithoutPartialSelectionContext extends ExpressionWithoutAssignmentContext {
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	constructor(ctx: ExpressionWithoutAssignmentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterExprWithoutPartialSelection) {
			listener.enterExprWithoutPartialSelection(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitExprWithoutPartialSelection) {
			listener.exitExprWithoutPartialSelection(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitExprWithoutPartialSelection) {
			return visitor.visitExprWithoutPartialSelection(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ExprWithSelectionContext extends ExpressionWithoutAssignmentContext {
	public WITH(): TerminalNode { return this.getToken(DpmXlParser.WITH, 0); }
	public partialSelection(): PartialSelectionContext {
		return this.getRuleContext(0, PartialSelectionContext);
	}
	public COLON(): TerminalNode { return this.getToken(DpmXlParser.COLON, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	constructor(ctx: ExpressionWithoutAssignmentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterExprWithSelection) {
			listener.enterExprWithSelection(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitExprWithSelection) {
			listener.exitExprWithSelection(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitExprWithSelection) {
			return visitor.visitExprWithSelection(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SelectionStartContext extends ParserRuleContext {
	public CURLY_BRACKET_LEFT(): TerminalNode { return this.getToken(DpmXlParser.CURLY_BRACKET_LEFT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_selectionStart; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSelectionStart) {
			listener.enterSelectionStart(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSelectionStart) {
			listener.exitSelectionStart(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSelectionStart) {
			return visitor.visitSelectionStart(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SelectionEndContext extends ParserRuleContext {
	public CURLY_BRACKET_RIGHT(): TerminalNode { return this.getToken(DpmXlParser.CURLY_BRACKET_RIGHT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_selectionEnd; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSelectionEnd) {
			listener.enterSelectionEnd(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSelectionEnd) {
			listener.exitSelectionEnd(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSelectionEnd) {
			return visitor.visitSelectionEnd(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PartialSelectionContext extends ParserRuleContext {
	public selectionStart(): SelectionStartContext {
		return this.getRuleContext(0, SelectionStartContext);
	}
	public cellRef(): CellRefContext {
		return this.getRuleContext(0, CellRefContext);
	}
	public selectionEnd(): SelectionEndContext {
		return this.getRuleContext(0, SelectionEndContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_partialSelection; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPartialSelection) {
			listener.enterPartialSelection(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPartialSelection) {
			listener.exitPartialSelection(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPartialSelection) {
			return visitor.visitPartialSelection(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemporaryAssignmentExpressionContext extends ParserRuleContext {
	public temporaryIdentifier(): TemporaryIdentifierContext {
		return this.getRuleContext(0, TemporaryIdentifierContext);
	}
	public ASSIGN(): TerminalNode { return this.getToken(DpmXlParser.ASSIGN, 0); }
	public persistentExpression(): PersistentExpressionContext {
		return this.getRuleContext(0, PersistentExpressionContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_temporaryAssignmentExpression; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTemporaryAssignmentExpression) {
			listener.enterTemporaryAssignmentExpression(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTemporaryAssignmentExpression) {
			listener.exitTemporaryAssignmentExpression(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTemporaryAssignmentExpression) {
			return visitor.visitTemporaryAssignmentExpression(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PersistentAssignmentExpressionContext extends ParserRuleContext {
	public varID(): VarIDContext {
		return this.getRuleContext(0, VarIDContext);
	}
	public PERSISTENT_ASSIGN(): TerminalNode { return this.getToken(DpmXlParser.PERSISTENT_ASSIGN, 0); }
	public expressionWithoutAssignment(): ExpressionWithoutAssignmentContext {
		return this.getRuleContext(0, ExpressionWithoutAssignmentContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_persistentAssignmentExpression; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPersistentAssignmentExpression) {
			listener.enterPersistentAssignmentExpression(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPersistentAssignmentExpression) {
			listener.exitPersistentAssignmentExpression(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPersistentAssignmentExpression) {
			return visitor.visitPersistentAssignmentExpression(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExpressionContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_expression; }
	public copyFrom(ctx: ExpressionContext): void {
		super.copyFrom(ctx);
	}
}
export class ParExprContext extends ExpressionContext {
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterParExpr) {
			listener.enterParExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitParExpr) {
			listener.exitParExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitParExpr) {
			return visitor.visitParExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class FuncExprContext extends ExpressionContext {
	public functions(): FunctionsContext {
		return this.getRuleContext(0, FunctionsContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterFuncExpr) {
			listener.enterFuncExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitFuncExpr) {
			listener.exitFuncExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitFuncExpr) {
			return visitor.visitFuncExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ClauseExprContext extends ExpressionContext {
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public SQUARE_BRACKET_LEFT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_LEFT, 0); }
	public clauseOperators(): ClauseOperatorsContext {
		return this.getRuleContext(0, ClauseOperatorsContext);
	}
	public SQUARE_BRACKET_RIGHT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_RIGHT, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterClauseExpr) {
			listener.enterClauseExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitClauseExpr) {
			listener.exitClauseExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitClauseExpr) {
			return visitor.visitClauseExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class UnaryExprContext extends ExpressionContext {
	public _op!: Token;
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public PLUS(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.PLUS, 0); }
	public MINUS(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MINUS, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterUnaryExpr) {
			listener.enterUnaryExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitUnaryExpr) {
			listener.exitUnaryExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitUnaryExpr) {
			return visitor.visitUnaryExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class NotExprContext extends ExpressionContext {
	public _op!: Token;
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public NOT(): TerminalNode { return this.getToken(DpmXlParser.NOT, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterNotExpr) {
			listener.enterNotExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitNotExpr) {
			listener.exitNotExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitNotExpr) {
			return visitor.visitNotExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class NumericExprContext extends ExpressionContext {
	public _left!: ExpressionContext;
	public _op!: Token;
	public _right!: ExpressionContext;
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public MULT(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MULT, 0); }
	public DIV(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.DIV, 0); }
	public PLUS(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.PLUS, 0); }
	public MINUS(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MINUS, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterNumericExpr) {
			listener.enterNumericExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitNumericExpr) {
			listener.exitNumericExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitNumericExpr) {
			return visitor.visitNumericExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ConcatExprContext extends ExpressionContext {
	public _left!: ExpressionContext;
	public _op!: Token;
	public _right!: ExpressionContext;
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public CONCAT(): TerminalNode { return this.getToken(DpmXlParser.CONCAT, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterConcatExpr) {
			listener.enterConcatExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitConcatExpr) {
			listener.exitConcatExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitConcatExpr) {
			return visitor.visitConcatExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class CompExprContext extends ExpressionContext {
	public _left!: ExpressionContext;
	public _op!: ComparisonOperatorsContext;
	public _right!: ExpressionContext;
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public comparisonOperators(): ComparisonOperatorsContext {
		return this.getRuleContext(0, ComparisonOperatorsContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterCompExpr) {
			listener.enterCompExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitCompExpr) {
			listener.exitCompExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitCompExpr) {
			return visitor.visitCompExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class InExprContext extends ExpressionContext {
	public _left!: ExpressionContext;
	public _op!: Token;
	public setOperand(): SetOperandContext {
		return this.getRuleContext(0, SetOperandContext);
	}
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public IN(): TerminalNode { return this.getToken(DpmXlParser.IN, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterInExpr) {
			listener.enterInExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitInExpr) {
			listener.exitInExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitInExpr) {
			return visitor.visitInExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class BoolExprContext extends ExpressionContext {
	public _left!: ExpressionContext;
	public _op!: Token;
	public _right!: ExpressionContext;
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public AND(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.AND, 0); }
	public OR(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.OR, 0); }
	public XOR(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.XOR, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterBoolExpr) {
			listener.enterBoolExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitBoolExpr) {
			listener.exitBoolExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitBoolExpr) {
			return visitor.visitBoolExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class IfExprContext extends ExpressionContext {
	public _conditionalExpr!: ExpressionContext;
	public _thenExpr!: ExpressionContext;
	public _elseExpr!: ExpressionContext;
	public IF(): TerminalNode { return this.getToken(DpmXlParser.IF, 0); }
	public THEN(): TerminalNode { return this.getToken(DpmXlParser.THEN, 0); }
	public ENDIF(): TerminalNode { return this.getToken(DpmXlParser.ENDIF, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public ELSE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.ELSE, 0); }
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterIfExpr) {
			listener.enterIfExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitIfExpr) {
			listener.exitIfExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitIfExpr) {
			return visitor.visitIfExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ItemReferenceExprContext extends ExpressionContext {
	public itemReference(): ItemReferenceContext {
		return this.getRuleContext(0, ItemReferenceContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterItemReferenceExpr) {
			listener.enterItemReferenceExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitItemReferenceExpr) {
			listener.exitItemReferenceExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitItemReferenceExpr) {
			return visitor.visitItemReferenceExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class PropertyReferenceExprContext extends ExpressionContext {
	public propertyReference(): PropertyReferenceContext {
		return this.getRuleContext(0, PropertyReferenceContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPropertyReferenceExpr) {
			listener.enterPropertyReferenceExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPropertyReferenceExpr) {
			listener.exitPropertyReferenceExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPropertyReferenceExpr) {
			return visitor.visitPropertyReferenceExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class KeyNamesExprContext extends ExpressionContext {
	public keyNames(): KeyNamesContext {
		return this.getRuleContext(0, KeyNamesContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterKeyNamesExpr) {
			listener.enterKeyNamesExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitKeyNamesExpr) {
			listener.exitKeyNamesExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitKeyNamesExpr) {
			return visitor.visitKeyNamesExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class LiteralExprContext extends ExpressionContext {
	public literal(): LiteralContext {
		return this.getRuleContext(0, LiteralContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterLiteralExpr) {
			listener.enterLiteralExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitLiteralExpr) {
			listener.exitLiteralExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitLiteralExpr) {
			return visitor.visitLiteralExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class SelectExprContext extends ExpressionContext {
	public select(): SelectContext {
		return this.getRuleContext(0, SelectContext);
	}
	constructor(ctx: ExpressionContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSelectExpr) {
			listener.enterSelectExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSelectExpr) {
			listener.exitSelectExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSelectExpr) {
			return visitor.visitSelectExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SetOperandContext extends ParserRuleContext {
	public selectionStart(): SelectionStartContext {
		return this.getRuleContext(0, SelectionStartContext);
	}
	public setElements(): SetElementsContext {
		return this.getRuleContext(0, SetElementsContext);
	}
	public selectionEnd(): SelectionEndContext {
		return this.getRuleContext(0, SelectionEndContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_setOperand; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSetOperand) {
			listener.enterSetOperand(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSetOperand) {
			listener.exitSetOperand(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSetOperand) {
			return visitor.visitSetOperand(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SetElementsContext extends ParserRuleContext {
	public itemReference(): ItemReferenceContext[];
	public itemReference(i: number): ItemReferenceContext;
	public itemReference(i?: number): ItemReferenceContext | ItemReferenceContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ItemReferenceContext);
		} else {
			return this.getRuleContext(i, ItemReferenceContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	public literal(): LiteralContext[];
	public literal(i: number): LiteralContext;
	public literal(i?: number): LiteralContext | LiteralContext[] {
		if (i === undefined) {
			return this.getRuleContexts(LiteralContext);
		} else {
			return this.getRuleContext(i, LiteralContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_setElements; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSetElements) {
			listener.enterSetElements(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSetElements) {
			listener.exitSetElements(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSetElements) {
			return visitor.visitSetElements(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class FunctionsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_functions; }
	public copyFrom(ctx: FunctionsContext): void {
		super.copyFrom(ctx);
	}
}
export class AggregateFunctionsContext extends FunctionsContext {
	public aggregateOperators(): AggregateOperatorsContext {
		return this.getRuleContext(0, AggregateOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterAggregateFunctions) {
			listener.enterAggregateFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitAggregateFunctions) {
			listener.exitAggregateFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitAggregateFunctions) {
			return visitor.visitAggregateFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class NumericFunctionsContext extends FunctionsContext {
	public numericOperators(): NumericOperatorsContext {
		return this.getRuleContext(0, NumericOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterNumericFunctions) {
			listener.enterNumericFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitNumericFunctions) {
			listener.exitNumericFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitNumericFunctions) {
			return visitor.visitNumericFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ComparisonFunctionsContext extends FunctionsContext {
	public comparisonFunctionOperators(): ComparisonFunctionOperatorsContext {
		return this.getRuleContext(0, ComparisonFunctionOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterComparisonFunctions) {
			listener.enterComparisonFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitComparisonFunctions) {
			listener.exitComparisonFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitComparisonFunctions) {
			return visitor.visitComparisonFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class FilterFunctionsContext extends FunctionsContext {
	public filterOperators(): FilterOperatorsContext {
		return this.getRuleContext(0, FilterOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterFilterFunctions) {
			listener.enterFilterFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitFilterFunctions) {
			listener.exitFilterFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitFilterFunctions) {
			return visitor.visitFilterFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ConditionalFunctionsContext extends FunctionsContext {
	public conditionalOperators(): ConditionalOperatorsContext {
		return this.getRuleContext(0, ConditionalOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterConditionalFunctions) {
			listener.enterConditionalFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitConditionalFunctions) {
			listener.exitConditionalFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitConditionalFunctions) {
			return visitor.visitConditionalFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class TimeFunctionsContext extends FunctionsContext {
	public timeOperators(): TimeOperatorsContext {
		return this.getRuleContext(0, TimeOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTimeFunctions) {
			listener.enterTimeFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTimeFunctions) {
			listener.exitTimeFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTimeFunctions) {
			return visitor.visitTimeFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class StringFunctionsContext extends FunctionsContext {
	public stringOperators(): StringOperatorsContext {
		return this.getRuleContext(0, StringOperatorsContext);
	}
	constructor(ctx: FunctionsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterStringFunctions) {
			listener.enterStringFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitStringFunctions) {
			listener.exitStringFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitStringFunctions) {
			return visitor.visitStringFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class NumericOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_numericOperators; }
	public copyFrom(ctx: NumericOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class UnaryNumericFunctionsContext extends NumericOperatorsContext {
	public _op!: Token;
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public ABS(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.ABS, 0); }
	public EXP(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.EXP, 0); }
	public LN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LN, 0); }
	public SQRT(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SQRT, 0); }
	constructor(ctx: NumericOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterUnaryNumericFunctions) {
			listener.enterUnaryNumericFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitUnaryNumericFunctions) {
			listener.exitUnaryNumericFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitUnaryNumericFunctions) {
			return visitor.visitUnaryNumericFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class BinaryNumericFunctionsContext extends NumericOperatorsContext {
	public _op!: Token;
	public _left!: ExpressionContext;
	public _right!: ExpressionContext;
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public COMMA(): TerminalNode { return this.getToken(DpmXlParser.COMMA, 0); }
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public POWER(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.POWER, 0); }
	public LOG(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LOG, 0); }
	constructor(ctx: NumericOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterBinaryNumericFunctions) {
			listener.enterBinaryNumericFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitBinaryNumericFunctions) {
			listener.exitBinaryNumericFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitBinaryNumericFunctions) {
			return visitor.visitBinaryNumericFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ComplexNumericFunctionsContext extends NumericOperatorsContext {
	public _op!: Token;
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public MAX(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MAX, 0); }
	public MIN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MIN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(ctx: NumericOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterComplexNumericFunctions) {
			listener.enterComplexNumericFunctions(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitComplexNumericFunctions) {
			listener.exitComplexNumericFunctions(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitComplexNumericFunctions) {
			return visitor.visitComplexNumericFunctions(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ComparisonFunctionOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_comparisonFunctionOperators; }
	public copyFrom(ctx: ComparisonFunctionOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class MatchExprContext extends ComparisonFunctionOperatorsContext {
	public MATCH(): TerminalNode { return this.getToken(DpmXlParser.MATCH, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public COMMA(): TerminalNode { return this.getToken(DpmXlParser.COMMA, 0); }
	public literal(): LiteralContext {
		return this.getRuleContext(0, LiteralContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(ctx: ComparisonFunctionOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterMatchExpr) {
			listener.enterMatchExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitMatchExpr) {
			listener.exitMatchExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitMatchExpr) {
			return visitor.visitMatchExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class IsnullExprContext extends ComparisonFunctionOperatorsContext {
	public ISNULL(): TerminalNode { return this.getToken(DpmXlParser.ISNULL, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(ctx: ComparisonFunctionOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterIsnullExpr) {
			listener.enterIsnullExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitIsnullExpr) {
			listener.exitIsnullExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitIsnullExpr) {
			return visitor.visitIsnullExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class FilterOperatorsContext extends ParserRuleContext {
	public FILTER(): TerminalNode { return this.getToken(DpmXlParser.FILTER, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public COMMA(): TerminalNode { return this.getToken(DpmXlParser.COMMA, 0); }
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_filterOperators; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterFilterOperators) {
			listener.enterFilterOperators(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitFilterOperators) {
			listener.exitFilterOperators(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitFilterOperators) {
			return visitor.visitFilterOperators(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TimeOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_timeOperators; }
	public copyFrom(ctx: TimeOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class TimeShiftFunctionContext extends TimeOperatorsContext {
	public TIME_SHIFT(): TerminalNode { return this.getToken(DpmXlParser.TIME_SHIFT, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	public TIME_PERIOD(): TerminalNode { return this.getToken(DpmXlParser.TIME_PERIOD, 0); }
	public INTEGER_LITERAL(): TerminalNode { return this.getToken(DpmXlParser.INTEGER_LITERAL, 0); }
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public propertyCode(): PropertyCodeContext | undefined {
		return this.tryGetRuleContext(0, PropertyCodeContext);
	}
	constructor(ctx: TimeOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTimeShiftFunction) {
			listener.enterTimeShiftFunction(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTimeShiftFunction) {
			listener.exitTimeShiftFunction(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTimeShiftFunction) {
			return visitor.visitTimeShiftFunction(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ConditionalOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_conditionalOperators; }
	public copyFrom(ctx: ConditionalOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class NvlFunctionContext extends ConditionalOperatorsContext {
	public NVL(): TerminalNode { return this.getToken(DpmXlParser.NVL, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public COMMA(): TerminalNode { return this.getToken(DpmXlParser.COMMA, 0); }
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(ctx: ConditionalOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterNvlFunction) {
			listener.enterNvlFunction(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitNvlFunction) {
			listener.exitNvlFunction(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitNvlFunction) {
			return visitor.visitNvlFunction(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class StringOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_stringOperators; }
	public copyFrom(ctx: StringOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class UnaryStringFunctionContext extends StringOperatorsContext {
	public LEN(): TerminalNode { return this.getToken(DpmXlParser.LEN, 0); }
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	constructor(ctx: StringOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterUnaryStringFunction) {
			listener.enterUnaryStringFunction(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitUnaryStringFunction) {
			listener.exitUnaryStringFunction(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitUnaryStringFunction) {
			return visitor.visitUnaryStringFunction(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AggregateOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_aggregateOperators; }
	public copyFrom(ctx: AggregateOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class CommonAggrOpContext extends AggregateOperatorsContext {
	public _op!: Token;
	public LPAREN(): TerminalNode { return this.getToken(DpmXlParser.LPAREN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public RPAREN(): TerminalNode { return this.getToken(DpmXlParser.RPAREN, 0); }
	public MAX_AGGR(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MAX_AGGR, 0); }
	public MIN_AGGR(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MIN_AGGR, 0); }
	public SUM(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SUM, 0); }
	public COUNT(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.COUNT, 0); }
	public AVG(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.AVG, 0); }
	public MEDIAN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.MEDIAN, 0); }
	public groupingClause(): GroupingClauseContext | undefined {
		return this.tryGetRuleContext(0, GroupingClauseContext);
	}
	constructor(ctx: AggregateOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterCommonAggrOp) {
			listener.enterCommonAggrOp(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitCommonAggrOp) {
			listener.exitCommonAggrOp(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitCommonAggrOp) {
			return visitor.visitCommonAggrOp(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class GroupingClauseContext extends ParserRuleContext {
	public GROUP_BY(): TerminalNode { return this.getToken(DpmXlParser.GROUP_BY, 0); }
	public keyNames(): KeyNamesContext[];
	public keyNames(i: number): KeyNamesContext;
	public keyNames(i?: number): KeyNamesContext | KeyNamesContext[] {
		if (i === undefined) {
			return this.getRuleContexts(KeyNamesContext);
		} else {
			return this.getRuleContext(i, KeyNamesContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_groupingClause; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterGroupingClause) {
			listener.enterGroupingClause(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitGroupingClause) {
			listener.exitGroupingClause(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitGroupingClause) {
			return visitor.visitGroupingClause(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ItemSignatureContext extends ParserRuleContext {
	public ITEM_SIGNATURE(): TerminalNode { return this.getToken(DpmXlParser.ITEM_SIGNATURE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_itemSignature; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterItemSignature) {
			listener.enterItemSignature(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitItemSignature) {
			listener.exitItemSignature(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitItemSignature) {
			return visitor.visitItemSignature(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ItemReferenceContext extends ParserRuleContext {
	public SQUARE_BRACKET_LEFT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_LEFT, 0); }
	public itemSignature(): ItemSignatureContext {
		return this.getRuleContext(0, ItemSignatureContext);
	}
	public SQUARE_BRACKET_RIGHT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_RIGHT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_itemReference; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterItemReference) {
			listener.enterItemReference(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitItemReference) {
			listener.exitItemReference(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitItemReference) {
			return visitor.visitItemReference(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ColElemContext extends ParserRuleContext {
	public COL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.COL, 0); }
	public COL_RANGE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.COL_RANGE, 0); }
	public COL_ALL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.COL_ALL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_colElem; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterColElem) {
			listener.enterColElem(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitColElem) {
			listener.exitColElem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitColElem) {
			return visitor.visitColElem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SheetElemContext extends ParserRuleContext {
	public SHEET(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SHEET, 0); }
	public SHEET_RANGE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SHEET_RANGE, 0); }
	public SHEET_ALL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SHEET_ALL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_sheetElem; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSheetElem) {
			listener.enterSheetElem(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSheetElem) {
			listener.exitSheetElem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSheetElem) {
			return visitor.visitSheetElem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RowElemSingleContext extends ParserRuleContext {
	public ROW(): TerminalNode { return this.getToken(DpmXlParser.ROW, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_rowElemSingle; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowElemSingle) {
			listener.enterRowElemSingle(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowElemSingle) {
			listener.exitRowElemSingle(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowElemSingle) {
			return visitor.visitRowElemSingle(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RowElemRangeContext extends ParserRuleContext {
	public ROW_RANGE(): TerminalNode { return this.getToken(DpmXlParser.ROW_RANGE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_rowElemRange; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowElemRange) {
			listener.enterRowElemRange(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowElemRange) {
			listener.exitRowElemRange(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowElemRange) {
			return visitor.visitRowElemRange(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RowElemAllContext extends ParserRuleContext {
	public ROW_ALL(): TerminalNode { return this.getToken(DpmXlParser.ROW_ALL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_rowElemAll; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowElemAll) {
			listener.enterRowElemAll(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowElemAll) {
			listener.exitRowElemAll(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowElemAll) {
			return visitor.visitRowElemAll(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RowElemContext extends ParserRuleContext {
	public rowElemSingle(): RowElemSingleContext | undefined {
		return this.tryGetRuleContext(0, RowElemSingleContext);
	}
	public rowElemRange(): RowElemRangeContext | undefined {
		return this.tryGetRuleContext(0, RowElemRangeContext);
	}
	public rowElemAll(): RowElemAllContext | undefined {
		return this.tryGetRuleContext(0, RowElemAllContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_rowElem; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowElem) {
			listener.enterRowElem(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowElem) {
			listener.exitRowElem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowElem) {
			return visitor.visitRowElem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RowHandlerContext extends ParserRuleContext {
	public rowElem(): RowElemContext | undefined {
		return this.tryGetRuleContext(0, RowElemContext);
	}
	public LPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LPAREN, 0); }
	public rowElemSingle(): RowElemSingleContext[];
	public rowElemSingle(i: number): RowElemSingleContext;
	public rowElemSingle(i?: number): RowElemSingleContext | RowElemSingleContext[] {
		if (i === undefined) {
			return this.getRuleContexts(RowElemSingleContext);
		} else {
			return this.getRuleContext(i, RowElemSingleContext);
		}
	}
	public RPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.RPAREN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_rowHandler; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowHandler) {
			listener.enterRowHandler(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowHandler) {
			listener.exitRowHandler(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowHandler) {
			return visitor.visitRowHandler(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ColHandlerContext extends ParserRuleContext {
	public colElem(): ColElemContext | undefined {
		return this.tryGetRuleContext(0, ColElemContext);
	}
	public LPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LPAREN, 0); }
	public COL(): TerminalNode[];
	public COL(i: number): TerminalNode;
	public COL(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COL);
		} else {
			return this.getToken(DpmXlParser.COL, i);
		}
	}
	public RPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.RPAREN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_colHandler; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterColHandler) {
			listener.enterColHandler(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitColHandler) {
			listener.exitColHandler(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitColHandler) {
			return visitor.visitColHandler(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SheetHandlerContext extends ParserRuleContext {
	public sheetElem(): SheetElemContext | undefined {
		return this.tryGetRuleContext(0, SheetElemContext);
	}
	public LPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LPAREN, 0); }
	public SHEET(): TerminalNode[];
	public SHEET(i: number): TerminalNode;
	public SHEET(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.SHEET);
		} else {
			return this.getToken(DpmXlParser.SHEET, i);
		}
	}
	public RPAREN(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.RPAREN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_sheetHandler; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSheetHandler) {
			listener.enterSheetHandler(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSheetHandler) {
			listener.exitSheetHandler(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSheetHandler) {
			return visitor.visitSheetHandler(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class IntervalContext extends ParserRuleContext {
	public INTERVAL(): TerminalNode { return this.getToken(DpmXlParser.INTERVAL, 0); }
	public COLON(): TerminalNode { return this.getToken(DpmXlParser.COLON, 0); }
	public BOOLEAN_LITERAL(): TerminalNode { return this.getToken(DpmXlParser.BOOLEAN_LITERAL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_interval; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterInterval) {
			listener.enterInterval(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitInterval) {
			listener.exitInterval(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitInterval) {
			return visitor.visitInterval(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class DefaultWithLiteralContext extends ParserRuleContext {
	public DEFAULT(): TerminalNode { return this.getToken(DpmXlParser.DEFAULT, 0); }
	public COLON(): TerminalNode { return this.getToken(DpmXlParser.COLON, 0); }
	public literal(): LiteralContext {
		return this.getRuleContext(0, LiteralContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_defaultWithLiteral; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterDefaultWithLiteral) {
			listener.enterDefaultWithLiteral(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitDefaultWithLiteral) {
			listener.exitDefaultWithLiteral(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitDefaultWithLiteral) {
			return visitor.visitDefaultWithLiteral(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ArgumentContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_argument; }
	public copyFrom(ctx: ArgumentContext): void {
		super.copyFrom(ctx);
	}
}
export class RowArgContext extends ArgumentContext {
	public rowHandler(): RowHandlerContext {
		return this.getRuleContext(0, RowHandlerContext);
	}
	constructor(ctx: ArgumentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRowArg) {
			listener.enterRowArg(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRowArg) {
			listener.exitRowArg(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRowArg) {
			return visitor.visitRowArg(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class ColArgContext extends ArgumentContext {
	public colHandler(): ColHandlerContext {
		return this.getRuleContext(0, ColHandlerContext);
	}
	constructor(ctx: ArgumentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterColArg) {
			listener.enterColArg(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitColArg) {
			listener.exitColArg(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitColArg) {
			return visitor.visitColArg(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class SheetArgContext extends ArgumentContext {
	public sheetHandler(): SheetHandlerContext {
		return this.getRuleContext(0, SheetHandlerContext);
	}
	constructor(ctx: ArgumentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSheetArg) {
			listener.enterSheetArg(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSheetArg) {
			listener.exitSheetArg(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSheetArg) {
			return visitor.visitSheetArg(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class IntervalArgContext extends ArgumentContext {
	public interval(): IntervalContext {
		return this.getRuleContext(0, IntervalContext);
	}
	constructor(ctx: ArgumentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterIntervalArg) {
			listener.enterIntervalArg(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitIntervalArg) {
			listener.exitIntervalArg(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitIntervalArg) {
			return visitor.visitIntervalArg(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class DefaultArgContext extends ArgumentContext {
	public defaultWithLiteral(): DefaultWithLiteralContext {
		return this.getRuleContext(0, DefaultWithLiteralContext);
	}
	constructor(ctx: ArgumentContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterDefaultArg) {
			listener.enterDefaultArg(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitDefaultArg) {
			listener.exitDefaultArg(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitDefaultArg) {
			return visitor.visitDefaultArg(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SelectContext extends ParserRuleContext {
	public selectionStart(): SelectionStartContext {
		return this.getRuleContext(0, SelectionStartContext);
	}
	public selectOperand(): SelectOperandContext {
		return this.getRuleContext(0, SelectOperandContext);
	}
	public selectionEnd(): SelectionEndContext {
		return this.getRuleContext(0, SelectionEndContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_select; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSelect) {
			listener.enterSelect(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSelect) {
			listener.exitSelect(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSelect) {
			return visitor.visitSelect(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SelectOperandContext extends ParserRuleContext {
	public cellRef(): CellRefContext | undefined {
		return this.tryGetRuleContext(0, CellRefContext);
	}
	public varRef(): VarRefContext | undefined {
		return this.tryGetRuleContext(0, VarRefContext);
	}
	public operationRef(): OperationRefContext | undefined {
		return this.tryGetRuleContext(0, OperationRefContext);
	}
	public preconditionElem(): PreconditionElemContext | undefined {
		return this.tryGetRuleContext(0, PreconditionElemContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_selectOperand; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterSelectOperand) {
			listener.enterSelectOperand(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitSelectOperand) {
			listener.exitSelectOperand(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitSelectOperand) {
			return visitor.visitSelectOperand(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarIDContext extends ParserRuleContext {
	public selectionStart(): SelectionStartContext {
		return this.getRuleContext(0, SelectionStartContext);
	}
	public varRef(): VarRefContext {
		return this.getRuleContext(0, VarRefContext);
	}
	public selectionEnd(): SelectionEndContext {
		return this.getRuleContext(0, SelectionEndContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_varID; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterVarID) {
			listener.enterVarID(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitVarID) {
			listener.exitVarID(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitVarID) {
			return visitor.visitVarID(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class CellRefContext extends ParserRuleContext {
	public _address!: CellAddressContext;
	public cellAddress(): CellAddressContext {
		return this.getRuleContext(0, CellAddressContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_cellRef; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterCellRef) {
			listener.enterCellRef(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitCellRef) {
			listener.exitCellRef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitCellRef) {
			return visitor.visitCellRef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PreconditionElemContext extends ParserRuleContext {
	public PRECONDITION_ELEMENT(): TerminalNode { return this.getToken(DpmXlParser.PRECONDITION_ELEMENT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_preconditionElem; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPreconditionElem) {
			listener.enterPreconditionElem(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPreconditionElem) {
			listener.exitPreconditionElem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPreconditionElem) {
			return visitor.visitPreconditionElem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarRefContext extends ParserRuleContext {
	public VAR_REFERENCE(): TerminalNode { return this.getToken(DpmXlParser.VAR_REFERENCE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_varRef; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterVarRef) {
			listener.enterVarRef(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitVarRef) {
			listener.exitVarRef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitVarRef) {
			return visitor.visitVarRef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class OperationRefContext extends ParserRuleContext {
	public OPERATION_REFERENCE(): TerminalNode { return this.getToken(DpmXlParser.OPERATION_REFERENCE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_operationRef; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterOperationRef) {
			listener.enterOperationRef(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitOperationRef) {
			listener.exitOperationRef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitOperationRef) {
			return visitor.visitOperationRef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class CellAddressContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_cellAddress; }
	public copyFrom(ctx: CellAddressContext): void {
		super.copyFrom(ctx);
	}
}
export class TableRefContext extends CellAddressContext {
	public tableReference(): TableReferenceContext {
		return this.getRuleContext(0, TableReferenceContext);
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	public argument(): ArgumentContext[];
	public argument(i: number): ArgumentContext;
	public argument(i?: number): ArgumentContext | ArgumentContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ArgumentContext);
		} else {
			return this.getRuleContext(i, ArgumentContext);
		}
	}
	constructor(ctx: CellAddressContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTableRef) {
			listener.enterTableRef(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTableRef) {
			listener.exitTableRef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTableRef) {
			return visitor.visitTableRef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class CompRefContext extends CellAddressContext {
	public argument(): ArgumentContext[];
	public argument(i: number): ArgumentContext;
	public argument(i?: number): ArgumentContext | ArgumentContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ArgumentContext);
		} else {
			return this.getRuleContext(i, ArgumentContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(ctx: CellAddressContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterCompRef) {
			listener.enterCompRef(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitCompRef) {
			listener.exitCompRef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitCompRef) {
			return visitor.visitCompRef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TableReferenceSingleContext extends ParserRuleContext {
	public TABLE_REFERENCE(): TerminalNode { return this.getToken(DpmXlParser.TABLE_REFERENCE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_tableReferenceSingle; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTableReferenceSingle) {
			listener.enterTableReferenceSingle(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTableReferenceSingle) {
			listener.exitTableReferenceSingle(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTableReferenceSingle) {
			return visitor.visitTableReferenceSingle(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TableGroupReferenceContext extends ParserRuleContext {
	public TABLE_GROUP_REFERENCE(): TerminalNode { return this.getToken(DpmXlParser.TABLE_GROUP_REFERENCE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_tableGroupReference; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTableGroupReference) {
			listener.enterTableGroupReference(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTableGroupReference) {
			listener.exitTableGroupReference(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTableGroupReference) {
			return visitor.visitTableGroupReference(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TableReferenceContext extends ParserRuleContext {
	public tableReferenceSingle(): TableReferenceSingleContext | undefined {
		return this.tryGetRuleContext(0, TableReferenceSingleContext);
	}
	public tableGroupReference(): TableGroupReferenceContext | undefined {
		return this.tryGetRuleContext(0, TableGroupReferenceContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_tableReference; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTableReference) {
			listener.enterTableReference(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTableReference) {
			listener.exitTableReference(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTableReference) {
			return visitor.visitTableReference(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ClauseOperatorsContext extends ParserRuleContext {
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_clauseOperators; }
	public copyFrom(ctx: ClauseOperatorsContext): void {
		super.copyFrom(ctx);
	}
}
export class WhereExprContext extends ClauseOperatorsContext {
	public WHERE(): TerminalNode { return this.getToken(DpmXlParser.WHERE, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	constructor(ctx: ClauseOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterWhereExpr) {
			listener.enterWhereExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitWhereExpr) {
			listener.exitWhereExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitWhereExpr) {
			return visitor.visitWhereExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class GetExprContext extends ClauseOperatorsContext {
	public GET(): TerminalNode { return this.getToken(DpmXlParser.GET, 0); }
	public keyNames(): KeyNamesContext {
		return this.getRuleContext(0, KeyNamesContext);
	}
	constructor(ctx: ClauseOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterGetExpr) {
			listener.enterGetExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitGetExpr) {
			listener.exitGetExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitGetExpr) {
			return visitor.visitGetExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}
export class RenameExprContext extends ClauseOperatorsContext {
	public RENAME(): TerminalNode { return this.getToken(DpmXlParser.RENAME, 0); }
	public renameClause(): RenameClauseContext[];
	public renameClause(i: number): RenameClauseContext;
	public renameClause(i?: number): RenameClauseContext | RenameClauseContext[] {
		if (i === undefined) {
			return this.getRuleContexts(RenameClauseContext);
		} else {
			return this.getRuleContext(i, RenameClauseContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(DpmXlParser.COMMA);
		} else {
			return this.getToken(DpmXlParser.COMMA, i);
		}
	}
	constructor(ctx: ClauseOperatorsContext) {
		super(ctx.parent, ctx.invokingState);
		this.copyFrom(ctx);
	}
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRenameExpr) {
			listener.enterRenameExpr(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRenameExpr) {
			listener.exitRenameExpr(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRenameExpr) {
			return visitor.visitRenameExpr(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class RenameClauseContext extends ParserRuleContext {
	public keyNames(): KeyNamesContext[];
	public keyNames(i: number): KeyNamesContext;
	public keyNames(i?: number): KeyNamesContext | KeyNamesContext[] {
		if (i === undefined) {
			return this.getRuleContexts(KeyNamesContext);
		} else {
			return this.getRuleContext(i, KeyNamesContext);
		}
	}
	public TO(): TerminalNode { return this.getToken(DpmXlParser.TO, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_renameClause; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterRenameClause) {
			listener.enterRenameClause(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitRenameClause) {
			listener.exitRenameClause(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitRenameClause) {
			return visitor.visitRenameClause(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ComparisonOperatorsContext extends ParserRuleContext {
	public EQ(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.EQ, 0); }
	public NE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.NE, 0); }
	public GT(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.GT, 0); }
	public LT(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LT, 0); }
	public GE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.GE, 0); }
	public LE(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.LE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_comparisonOperators; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterComparisonOperators) {
			listener.enterComparisonOperators(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitComparisonOperators) {
			listener.exitComparisonOperators(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitComparisonOperators) {
			return visitor.visitComparisonOperators(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class LiteralContext extends ParserRuleContext {
	public INTEGER_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.INTEGER_LITERAL, 0); }
	public DECIMAL_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.DECIMAL_LITERAL, 0); }
	public PERCENT_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.PERCENT_LITERAL, 0); }
	public STRING_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.STRING_LITERAL, 0); }
	public BOOLEAN_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.BOOLEAN_LITERAL, 0); }
	public DATE_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.DATE_LITERAL, 0); }
	public TIME_INTERVAL_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.TIME_INTERVAL_LITERAL, 0); }
	public TIME_PERIOD_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.TIME_PERIOD_LITERAL, 0); }
	public EMPTY_LITERAL(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.EMPTY_LITERAL, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_literal; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterLiteral) {
			listener.enterLiteral(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitLiteral) {
			listener.exitLiteral(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitLiteral) {
			return visitor.visitLiteral(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class KeyNamesContext extends ParserRuleContext {
	public ROW_HEADING(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.ROW_HEADING, 0); }
	public COL_HEADING(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.COL_HEADING, 0); }
	public SHEET_HEADING(): TerminalNode | undefined { return this.tryGetToken(DpmXlParser.SHEET_HEADING, 0); }
	public propertyCode(): PropertyCodeContext | undefined {
		return this.tryGetRuleContext(0, PropertyCodeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_keyNames; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterKeyNames) {
			listener.enterKeyNames(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitKeyNames) {
			listener.exitKeyNames(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitKeyNames) {
			return visitor.visitKeyNames(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PropertyReferenceContext extends ParserRuleContext {
	public SQUARE_BRACKET_LEFT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_LEFT, 0); }
	public propertyCode(): PropertyCodeContext {
		return this.getRuleContext(0, PropertyCodeContext);
	}
	public SQUARE_BRACKET_RIGHT(): TerminalNode { return this.getToken(DpmXlParser.SQUARE_BRACKET_RIGHT, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_propertyReference; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPropertyReference) {
			listener.enterPropertyReference(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPropertyReference) {
			listener.exitPropertyReference(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPropertyReference) {
			return visitor.visitPropertyReference(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PropertyCodeContext extends ParserRuleContext {
	public CODE(): TerminalNode { return this.getToken(DpmXlParser.CODE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_propertyCode; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterPropertyCode) {
			listener.enterPropertyCode(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitPropertyCode) {
			listener.exitPropertyCode(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitPropertyCode) {
			return visitor.visitPropertyCode(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemporaryIdentifierContext extends ParserRuleContext {
	public CODE(): TerminalNode { return this.getToken(DpmXlParser.CODE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return DpmXlParser.RULE_temporaryIdentifier; }
	// @Override
	public enterRule(listener: DpmXlParserListener): void {
		if (listener.enterTemporaryIdentifier) {
			listener.enterTemporaryIdentifier(this);
		}
	}
	// @Override
	public exitRule(listener: DpmXlParserListener): void {
		if (listener.exitTemporaryIdentifier) {
			listener.exitTemporaryIdentifier(this);
		}
	}
	// @Override
	public accept<Result>(visitor: DpmXlParserVisitor<Result>): Result {
		if (visitor.visitTemporaryIdentifier) {
			return visitor.visitTemporaryIdentifier(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


