parser grammar DpmXlParser;

options { tokenVocab=DpmXlLexer ;}

// Added rule for expr management and EOF
root:
    statement ((EOL statements) | EOL?) EOF
    ;

statements:
    (statement EOL)+
    ;

statement:
    expressionWithoutAssignment                                                                      #exprWithoutAssignment
    | temporaryAssignmentExpression                                                                  #assignmentExpr
    ;

persistentExpression:
    persistentAssignmentExpression
    | expressionWithoutAssignment
    ;

expressionWithoutAssignment:
    expression                                                                  #exprWithoutPartialSelection
    | WITH partialSelection COLON expression                                    #exprWithSelection
    ;

selectionStart:
    CURLY_BRACKET_LEFT
    ;

selectionEnd:
    CURLY_BRACKET_RIGHT
    ;

partialSelection:
    selectionStart cellRef selectionEnd
    ;

temporaryAssignmentExpression:
    temporaryIdentifier ASSIGN persistentExpression
    ;

persistentAssignmentExpression:
    varID PERSISTENT_ASSIGN expressionWithoutAssignment
    ;

expression:
    LPAREN expression RPAREN                                                                            #parExpr
    | functions                                                                                         #funcExpr
    | expression SQUARE_BRACKET_LEFT clauseOperators SQUARE_BRACKET_RIGHT                               #clauseExpr
    | op=(PLUS|MINUS) expression                                                                        #unaryExpr
    | op=NOT LPAREN expression RPAREN                                                                   #notExpr
    | left=expression op=(MULT|DIV) right=expression                                                    #numericExpr
    | left=expression op=(PLUS|MINUS) right=expression                                                  #numericExpr
    | left=expression op=CONCAT right=expression                                                        #concatExpr
    | left=expression op=comparisonOperators right=expression                                           #compExpr
    | left=expression op=IN setOperand                                                                  #inExpr
    | left=expression op=AND right=expression                                                           #boolExpr
    | left=expression op=(OR|XOR) right=expression                                                      #boolExpr
    | IF conditionalExpr=expression THEN thenExpr=expression (ELSE elseExpr=expression)? ENDIF          #ifExpr
    | itemReference                                                                                     #itemReferenceExpr
    | propertyReference                                                                                 #propertyReferenceExpr
    | keyNames                                                                                          #keyNamesExpr
    | literal                                                                                           #literalExpr
    | select                                                                                            #selectExpr
    ;

setOperand:
    selectionStart setElements selectionEnd
    ;

setElements:
    itemReference (COMMA itemReference)*
    | literal (COMMA literal)*
    ;

functions:
    aggregateOperators                                              #aggregateFunctions
    | numericOperators                                              #numericFunctions
    | comparisonFunctionOperators                                   #comparisonFunctions
    | filterOperators                                               #filterFunctions
    | conditionalOperators                                          #conditionalFunctions
    | timeOperators                                                 #timeFunctions
    | stringOperators                                               #stringFunctions
;

numericOperators:
    op=(ABS|EXP|LN|SQRT) LPAREN expression RPAREN                                 #unaryNumericFunctions
    | op=(POWER|LOG) LPAREN left=expression COMMA right=expression RPAREN         #binaryNumericFunctions
    | op=(MAX|MIN) LPAREN expression (COMMA expression)+ RPAREN                   #complexNumericFunctions
    ;

comparisonFunctionOperators:
    MATCH LPAREN expression COMMA literal RPAREN                    #matchExpr
    | ISNULL LPAREN expression RPAREN                               #isnullExpr
;

filterOperators:
    FILTER LPAREN expression COMMA expression RPAREN
    ;

timeOperators:
    TIME_SHIFT LPAREN expression COMMA TIME_PERIOD COMMA INTEGER_LITERAL (COMMA propertyCode)? RPAREN #timeShiftFunction
    ;

conditionalOperators:
    NVL LPAREN expression COMMA expression RPAREN           #nvlFunction
    ;

stringOperators:
    LEN LPAREN expression RPAREN          #unaryStringFunction
    ;

aggregateOperators:
    op=(MAX_AGGR
        |MIN_AGGR
        |SUM
        |COUNT
        |AVG
        |MEDIAN) LPAREN expression (groupingClause)? RPAREN        #commonAggrOp
    ;

groupingClause:
    GROUP_BY keyNames (COMMA keyNames)*
;

// Dimension management and members
itemSignature: ITEM_SIGNATURE;
itemReference: SQUARE_BRACKET_LEFT itemSignature SQUARE_BRACKET_RIGHT;

// Cell Address and table management

colElem
    : COL
    | COL_RANGE
    | COL_ALL
    ;

sheetElem
    : SHEET
    | SHEET_RANGE
    | SHEET_ALL
    ;

rowElemSingle: ROW;
rowElemRange: ROW_RANGE;
rowElemAll: ROW_ALL;

rowElem
   : rowElemSingle
   | rowElemRange
   | rowElemAll
   ;

rowHandler
   : rowElem
   | LPAREN rowElemSingle (COMMA rowElemSingle)* RPAREN
   ;

colHandler:
    colElem
    | LPAREN COL (COMMA COL)* RPAREN;

sheetHandler:
    sheetElem
    | LPAREN SHEET (COMMA SHEET)* RPAREN
;

interval:
    INTERVAL COLON BOOLEAN_LITERAL
;

defaultWithLiteral:
    DEFAULT COLON literal
;

argument
    : rowHandler                        #rowArg
    | colHandler                        #colArg
    | sheetHandler                      #sheetArg
    | interval                          #intervalArg
    | defaultWithLiteral                #defaultArg
    ;

select:
    selectionStart selectOperand selectionEnd
    ;

selectOperand:
    cellRef
    | varRef
    | operationRef
    | preconditionElem
    ;

varID:
    selectionStart varRef selectionEnd
    ;

cellRef:
    address = cellAddress
    ;

preconditionElem:
    PRECONDITION_ELEMENT
    ;

varRef:
    VAR_REFERENCE
    ;

operationRef:
    OPERATION_REFERENCE
    ;

// TODO: table can be ignored when there is a with
// FIXME: allow only 1 row, 1 column, etc.
cellAddress
    : tableReference (COMMA argument)*             #tableRef
    | argument (COMMA argument)*                   #compRef;

tableReferenceSingle: TABLE_REFERENCE;
tableGroupReference: TABLE_GROUP_REFERENCE;

tableReference
    : tableReferenceSingle
    | tableGroupReference
    ;

clauseOperators:
    WHERE expression                                    #whereExpr
    | GET keyNames                                      #getExpr
    | RENAME renameClause (COMMA renameClause)*         #renameExpr
    ;

// Always on grammar, not on tokens. Order is important (top ones should be the enclosing ones)

// TODO: do not allow renaming a key to the same key?
renameClause:
    keyNames TO keyNames
    ;

comparisonOperators:
    EQ
    |NE
    |GT
    |LT
    |GE
    |LE;

literal:
    INTEGER_LITERAL
    | DECIMAL_LITERAL
    | PERCENT_LITERAL
    | STRING_LITERAL
    | BOOLEAN_LITERAL
    | DATE_LITERAL
    | TIME_INTERVAL_LITERAL
    | TIME_PERIOD_LITERAL
    | EMPTY_LITERAL
;

keyNames:
    ROW_HEADING
    |COL_HEADING
    |SHEET_HEADING
    | propertyCode
;

propertyReference:
    SQUARE_BRACKET_LEFT propertyCode SQUARE_BRACKET_RIGHT;

propertyCode:
    CODE
    ;

temporaryIdentifier:
    CODE
    ;
