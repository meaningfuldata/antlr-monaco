/// <reference types="vitest" />
import { defineConfig } from 'vite'
import { isAbsolute, relative, resolve } from 'node:path'
import vitePluginEnvironment from 'vite-plugin-environment'
import type { OutputOptions } from 'rollup'

const output: OutputOptions = {
  name: 'antlr_monaco',
  inlineDynamicImports: true,
  exports: 'named'
}

export default defineConfig({
  plugins: [
    // Used to expose `process.env.NODE_DEBUG` to antlr4ts
    vitePluginEnvironment({ NODE_DEBUG: 'production' })
  ],

  build: {
    lib: {
      entry: resolve(__dirname, './src/index.ts')
    },
    rollupOptions: {
      external: ['antlr4ts', 'antlr4ts/tree', 'antlr4-c3', 'monaco-editor'],
      output: [
        {
          ...output,
          format: 'es',
          esModule: true
        },
        {
          ...output,
          format: 'umd',
          interop: 'esModule'
        }
      ]
    }
  },

  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      '!examples': resolve(__dirname, './examples/index.ts'),
      '!tests': resolve(__dirname, './tests/index.ts')
    }
  },

  test: {
    cache: false,
    globals: true,
    environment: 'jsdom',
    alias: [
      {
        find: /^monaco-editor$/,
        replacement: resolve(
          __dirname,
          './node_modules/monaco-editor/esm/vs/editor/editor.api'
        )
      }
    ],
    resolveSnapshotPath: (testPath, snapshotExtension) => {
      const relativePath = isAbsolute(testPath)
        ? relative(resolve(testPath, __dirname, './src'), testPath)
        : testPath

      return `tests/unit/snapshots/${relativePath}`.replace(
        /\.ts$/,
        `${snapshotExtension}.js`
      )
    }
  }
})
