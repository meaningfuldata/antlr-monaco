import { Grammar } from '../../'
import type { CaretPosition, CompletionSuggestion } from '../../'
import type { IRange, Position, editor } from 'monaco-editor'
import { languages } from 'monaco-editor'

const inferRange = (model: editor.ITextModel, position: Position): IRange => {
  const word = model.getWordUntilPosition(position)

  return {
    startLineNumber: position.lineNumber,
    endLineNumber: position.lineNumber,
    startColumn: word.startColumn,
    endColumn: word.endColumn
  }
}

/**
 *
 * NOTE: suggestions can be triggered with:
 * ```
 * editor.trigger('', 'editor.action.triggerSuggest', {});
 * ```
 */
export class CompletionItemProvider
  implements languages.CompletionItemProvider
{
  constructor(protected grammar: Grammar) {}

  /**
   * Convert Monaco `Position` to `CaretPosition`
   */
  static toCaretPosition(position: Position): CaretPosition {
    return {
      line: position.lineNumber,
      column: position.column
    }
  }

  provideCompletionItems(
    model: editor.ITextModel,
    position: Position,
    _context: languages.CompletionContext,
    _token: any
  ): languages.ProviderResult<languages.CompletionList> {
    const range = inferRange(model, position)
    const code = model.getValue()
    const caretPosition = CompletionItemProvider.toCaretPosition(position)

    return this.grammar
      .suggestCompletions(code, caretPosition)
      .then(engineSuggestions => {
        const suggestions = engineSuggestions.map(
          (suggestion: CompletionSuggestion) =>
            this.transformSuggestion(suggestion, range)
        )

        return { suggestions }
      })
  }

  /**
   * Transform the suggestions provided by the completion engine into the `languages.CompletionItem` expected by Monaco.
   * By default all suggestions are considered just text (`languages.CompletionItemKind.Text`).
   */
  protected transformSuggestion(
    suggestion: CompletionSuggestion,
    range: IRange
  ): languages.CompletionItem {
    return {
      label: suggestion.text,
      kind: languages.CompletionItemKind.Text,
      insertText: suggestion.text,
      range,
      ...suggestion.completionItem
    }
  }
}
