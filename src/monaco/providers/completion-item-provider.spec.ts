import { parse } from '@/test/'
import { CompletionItemProvider } from './completion-item-provider'
import type { CaretPosition } from '@/grammar/'
import { dpmXl } from '!tests'
import { editor, languages, Position } from 'monaco-editor'

const buildModel = (expression: string): editor.ITextModel => {
  return editor.createModel(expression)
}
const buildPosition = (caretPosition: CaretPosition): Position => {
  return new Position(caretPosition.line, caretPosition.column)
}

describe('CompletionItemProvider', () => {
  let provider: CompletionItemProvider

  beforeEach(() => {
    provider = new CompletionItemProvider(dpmXl)
  })

  it('should be created', () => {
    expect(provider).toBeTruthy()
  })

  it.skip('works', async () => {
    const { code, caretPosition } = parse(dpmXl, `with {c0010, |`)
    const model = buildModel(code)
    const position = buildPosition(caretPosition)

    const items = await provider.provideCompletionItems(
      model,
      position,
      {} as languages.CompletionContext,
      {}
    )
    expect(items?.suggestions.length).toEqual(12)
  })

  // describe('#provideCompletionItems', () => {
  //   describe('when the expression is empty', () => {})
  //
  //   describe('when the expression is not empty', () => {
  //     describe('when the cursor is after a curly bracket', () => {
  //       describe('when the cursor is after a "t"', () => {})
  //     })
  //   })
  // })
})
