import { CompletionSuggestion, Grammar } from '../grammar/'
import type { CaretPosition, CompletionOptions } from '../grammar/'
import { inferCaretNode } from '../grammar/completion/positions'

export interface Options {
  caret: string
}

export const defaultOptions: Options = {
  caret: '|'
}

/**
 * Parse the code.
 *
 * It allows including a symbol (or symbols) to represent the caret position.
 * The default caret symbol is defined in `defaultOptions.caret`.
 */
export const parse = (
  grammar: Grammar,
  codeWithCaret: string,
  options: Partial<Options> = {}
) => {
  const config: Options = {
    ...options,
    ...defaultOptions
  }

  const lines = codeWithCaret.split('\n')
  let line = 0,
    column = -1
  for (; line < lines.length; line++) {
    column = lines[line].indexOf(config.caret)
    if (column != -1) {
      break
    }
  }
  if (column === -1) {
    throw new Error(
      `The code does not include the caret symbol (\`${config.caret}\`)`
    )
  }

  const caretPosition: CaretPosition = {
    line: line + 1,
    column: column + 1
  }
  const code = codeWithCaret.replace(config.caret, '')
  const parseResult = grammar.parse(code)
  const {caretNode} = inferCaretNode(parseResult.parseTree, caretPosition, parseResult.tokenStream)
  return {
    ...parseResult,
    code,
    caretPosition,
    caretNode
  }
}

interface Suggest {
  suggestions: CompletionSuggestion[]
  length: number
  texts: string[]
}

/**
 * Parse the code and return suggestions.
 *
 * It allows including a symbol (or symbols) to represent the caret position.
 * The default caret symbol is defined in `defaultOptions.caret`.
 */
export const suggest = async (
  grammar: Grammar,
  codeWithCaret: string,
  options: Partial<Options & CompletionOptions> = {}
): Promise<Suggest> => {
  const { code, caretPosition } = parse(grammar, codeWithCaret, options)

  const suggestions = await grammar.suggestCompletions(code, caretPosition)

  return {
    suggestions,
    length: suggestions.length,
    texts: suggestions.map(({ text }) => text)
  }
}

// TODO
// export const provideCompletionItems = (
//   provider: any,
//   codeWithCaret: string,
//   options: Partial<TestParseOptions> = {}
// ) => {
//     const { code, caretPosition } = testParse(codeWithCaret, options)
//
//     const model = editor.createModel(code)
//     const position = new Position(caretPosition.line, caretPosition.column)
//
//     return await provider.provideCompletionItems(
//       model,
//       position,
//       {} as CompletionContext,
//       {}
//     )
// }
