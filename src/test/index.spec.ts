import { parse } from './index'
import type { CaretPosition } from '../'
import { dpmXl } from '!tests'

const suite = (codeWithCaret: string, expectedCaretPosition: CaretPosition) => {
  it('returns the code without the caret', () => {
    const { code } = parse(dpmXl, codeWithCaret)
    expect(code).not.toContain('|')
  })

  it('returns the correct caret position', () => {
    const { caretPosition } = parse(dpmXl, codeWithCaret)
    expect(caretPosition).toEqual(expectedCaretPosition)
  })
}

describe('parse', () => {
  describe('code with 1 line only', () => {
    const code = `{tS.02.01, r0800, c0010} = |{tS.02.02, r0150, c0020};`

    suite(code, { line: 1, column: 28 })
  })

  describe('empty code', () => {
    const code = `|`

    suite(code, { line: 1, column: 1 })
  })

  describe('code with multiple lines', () => {
    const code = `{tS.02.01, r0800, c0010}\n = \n|{tS.02.02, r0150, c0020};`

    suite(code, { line: 3, column: 1 })
  })
})
