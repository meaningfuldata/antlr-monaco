import {
  CharStreams,
  CodePointCharStream,
  CommonTokenStream,
  Lexer as AbstractLexer,
  Parser as AbstractParser,
  ParserRuleContext
} from 'antlr4ts'
import { ParseTree, ParseTreeVisitor } from 'antlr4ts/tree'
import {
  CompletionEngine,
  Options as CompletionOptions,
  Suggestion
} from './completion/engine'
import { CaretPosition } from './completion/positions'
import { ErrorListener } from './error-listener'
import { SymbolTable } from 'antlr4-c3'

export interface Options {
  completion?: CompletionOptions
}

export abstract class Grammar {
  public abstract Lexer: new (input: CodePointCharStream) => AbstractLexer
  public abstract Parser: new (input: CommonTokenStream) => AbstractParser
  public abstract Visitor: new () => ParseTreeVisitor<SymbolTable>

  /**
   * Default options of the grammar.
   *
   * Usually the completion options (tokens, rules, etc.) are defined here to specify
   * all the configuration declaratively.
   * In case something more dynamic is required, the constructor can be used.
   */
  public static defaultOptions: Options = {}

  /**
   * @argument options Default options for this instance of the grammar.
   */
  constructor(protected options: Options = Grammar.defaultOptions) {}

  /**
   * Parse the code
   *
   * @argument code The code to parse
   */
  parse(code: string) {
    const input = CharStreams.fromString(code)
    const lexer = new this.Lexer(input)

    const tokenStream = new CommonTokenStream(lexer)
    const parser = new this.Parser(tokenStream)

    // Remove all error listeners (basically `ConsoleErrorListener`)
    lexer.removeErrorListeners()
    parser.removeErrorListeners()

    // Use the custom error listener
    const errorListener = new ErrorListener()
    lexer.addErrorListener(errorListener)
    parser.addErrorListener(errorListener)

    let parseTree: ParseTree
    try {
      const initialRuleName = parser.ruleNames[0] as keyof typeof parser
      parseTree = (parser[initialRuleName] as () => ParserRuleContext)()
    } catch (_error) {
      throw new Error('Cannot find the initial rule of the parser')
    }

    return {
      input,
      tokenStream,
      lexer,
      parser,
      parseTree
    }
  }

  /**
   * Parse the expression and return the suggestions.
   *
   * @argument code Code to provide suggestions.
   * @argument caret Position of the caret in the code.
   * @argument options Options to override the defaults.
   */
  async suggestCompletions(
    code: string,
    caret: CaretPosition,
    options: Partial<CompletionOptions> = {}
  ): Promise<Suggestion[]> {
    const { parser, parseTree, tokenStream } = this.parse(code)
    const visitor = new this.Visitor()
    const completionOptions = {
      ...this.options?.completion,
      ...options
    }
    const engine = new CompletionEngine(parser, visitor, completionOptions)
    return engine.suggest(parseTree, caret, tokenStream)
  }
}
