import { ErrorListener } from './error-listener'

describe('ErrorListener', () => {
  describe('#length', () => {
    it('returns the number of errors', () => {
      const listener = new ErrorListener()
      expect(listener.length).toBe(0)

      listener.syntaxError({} as any, {}, 1, 1, 'message', undefined)
      expect(listener.length).toBe(1)
    })
  })
})
