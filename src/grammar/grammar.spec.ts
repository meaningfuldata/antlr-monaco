import { Grammar } from './grammar'
import { DpmXlLexer, DpmXlParser, DpmXlSymbolTableVisitor } from '!examples'

class TestGrammar extends Grammar {
  Lexer = DpmXlLexer
  Parser = DpmXlParser
  Visitor = DpmXlSymbolTableVisitor
}

describe('Grammar', () => {
  const grammar = new TestGrammar()

  const suiteValid = (label: string, code: string) => {
    return describe(label, () => {
      it('is tokenized correctly', () => {
        const { input } = grammar.parse(code)
        expect(input.index).toEqual(input.size)
      })

      it('is parsed correctly', () => {
        const { parser } = grammar.parse(code)
        expect(parser.numberOfSyntaxErrors).toEqual(0)
      })
    })
  }

  describe('#parse', () => {
    describe('empty expression', () => {
      const code = ``

      it('is tokenized correctly', () => {
        const { input } = grammar.parse(code)
        expect(input.index).toEqual(input.size)
      })

      it('is parsed with errors', () => {
        const { parser } = grammar.parse(code)
        expect(parser.numberOfSyntaxErrors).toEqual(1)
      })
    })

    suiteValid(
      'valid expression',
      `{tS.02.01, r0800, c0010} = {tS.02.02, r0150, c0020};`
    )

    const complexExpressions: Record<string, string> = {
      '`with` with `interval`': `with {c0010, default: 0, interval: true}: {tC 09.01.a, r0095} [ where CEG = [eba_GA:x1] ] = {tC 07.00.a, r0020, s0010}`,
      '`with` with `match`': `with {tS.06.02.04.02, default: ''}: isnull ( filter ({cC0270}, match ({cC0290}, "^..((71)|(75)|(9.))$")) )`,
      '`default` without `with`': `{tSE.02.01.16.01, rR0190, cC0010, default: ''} - {tSE.02.01.16.01, rR0790, cC0010, default: 0} = sum ( filter ({tS.08.01.01.01, cC0240, default: 0}, {tS.08.01.01.01, cC0080, default: ''} = [s2c_LB:x91]) )`,
      '`default` without `with` 2': `{tS.02.01.01.01, rR0510, cC0010, default: ''} + {tS.02.01.01.01, rR0600, cC0010, default: 0} + {tS.02.01.01.01, rR0690, cC0010, default: 0} = sum ( filter ({tS.35.01.04.01, cC0060, default: 0}, {tS.35.01.04.01, cC0040, default: ''} = [s2c_CS:x2]) )`,
      '`with` with `not`': `with {tS.06.03.04.01}: not ( isnull ( filter ({cC0040, default: ''}, {cC0030, default: ''} != [s2c_MC:x81] and {cC0030} != [s2c_MC:x115] and {cC0030} != [s2c_MC:x186]) ) )`,
      '`with` with `default` and `interval`': `with {tF 46.00, c0110, default: 0, interval: true}: {r0010} = time_shift({r0210}, Q, 1, refPeriod)`,
      '`with` with numeric `default`': `with {rR0430, default: 0}: {tS.26.01.01.02, cC0060} = max( 0 , ({tS.26.01.01.01, cC0020} - {tS.26.01.01.01, cC0030}) - {tS.26.01.01.01, cC0040} - {tS.26.01.01.01, cC0050} )`,
      '`filter`': `filter ({tS.06.02.01.01, cC0060}, {tS.06.02.01.01, cC0060} in { [s2c_PU:x96], [s2c_PU:x57] }) [get NF] != ""`,
      '`match`': `match ( filter ({tS.06.02.04.02, cC0290}, match ({tS.06.02.04.02, cC0290} [get UI] , "^CAU/.*") and not ( match ({tS.06.02.04.02, cC0290} [get UI] , "^CAU/(ISIN/.*)|(INDEX/.*)") )) , "^((XL)|(XT))..$")`,
      '`if`': `with {tS.02.01.01.01, cC0020}: if ( not ( isnull ({rR0520, default: 0}) ) or not ( isnull ({rR0560, default: ''}) ) ) then ( {rR0510, default: ''} = {rR0520} + {rR0560} ) endif`
    }

    for (const label in complexExpressions) {
      suiteValid(
        `valid complex expression (${label})`,
        complexExpressions[label]
      )
    }

    describe('invalid expression', () => {
      const code = `with {c0010, `

      it('is tokenized correctly', () => {
        const { input } = grammar.parse(code)
        expect(input.index).toEqual(input.size)
      })

      it('is parsed with errors', () => {
        const { parser } = grammar.parse(code)
        expect(parser.numberOfSyntaxErrors).toEqual(1)
      })
    })
  })
})
