import { ANTLRErrorListener, RecognitionException, Recognizer } from 'antlr4ts'

/**
 * An error detected by the `ErrorListener`.
 */
export interface ErrorListenerItem<T> {
  offendingSymbol: T | undefined
  line: number
  column: number
  message: string
}

/**
 * Error listener for the lexer and the parser.
 */
export class ErrorListener<T> implements ANTLRErrorListener<T> {
  constructor(public errors: ErrorListenerItem<T>[] = []) { }

  /**
   * Number of errors.
   */
  get length(): number {
    return this.errors.length
  }

  /**
   * Add an error that is not associated with any `Recognizer`.
   */
  push(error: ErrorListenerItem<T>): void {
    this.errors.push(error)
  }

  /**
   * NOTE: errors without the offending symbol are ignroed
   */
  syntaxError(
    _recognizer: Recognizer<T, any>,
    offendingSymbol: T | undefined,
    line: number,
    column: number,
    message: string,
    _exception: RecognitionException | undefined
  ): void {
    if (offendingSymbol) {
      this.push({
        offendingSymbol,
        line,
        column,
        message
      })
    }
  }
}
