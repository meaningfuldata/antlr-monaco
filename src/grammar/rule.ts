import { Parser } from 'antlr4ts'

/**
 * Alias to `number` that states more clearly that a number is used as the rule type.
 */
export type RuleType = number

export class Rule {
  static from<P extends Parser>(parser: P, ruleType: RuleType): Rule {
    const name = parser.ruleNames[ruleType]
    return new Rule(ruleType, name)
  }

  constructor(public type: RuleType, public name?: string) { }
}
