import { ParseTree, TerminalNode } from 'antlr4ts/tree'
import { ParserRuleContext, CommonTokenStream } from 'antlr4ts'
import type { Token } from 'antlr4ts'

/**
 * The position where the text would be inserted.
 */
export interface CaretPosition {
  /**
   *  Lines start at 1.
   */
  readonly line: number
  /**
   *  Columns start at 1.
   */
  readonly column: number
}

/**
 * The node at the caret position.
 * TODO: to class?
 */
export interface CaretNode {
  /**
   * Position inside the parse tree.
   */
  readonly index: number
  /**
   * Parse tree associated to this node.
   */
  readonly node: TerminalNode
  /**
   * The node text until the caret.
   */
  readonly text: string
}

/**
 * Infer which token holds the caret in the tree.
 */
export const inferCaretNode = (
  parseTree: ParseTree,
  caretPosition: CaretPosition,
  tokenStream: CommonTokenStream
): { caretNode: CaretNode | null, index: number } => {
  checkCaretPosition(parseTree, caretPosition)
  const temp = { line: caretPosition.line, column: caretPosition.column }
  let caretNode = null
  //Find the caret node (that's made spaces don't count)
  while(true) {
    caretNode = visit(parseTree, temp)
    if (caretNode || temp.column == 0) {
      break
    }
    temp.column--
  }
  if(!caretNode){
    return {caretNode: null, index: -1}
  }
  //Revise index to find correct
  caretNode = buildTokenIndex(caretNode, caretPosition) as any
  if (caretNode.index != -1) {
    if(caretNode.text.match(/^[a-z0-9]+$/i)){
      return {caretNode, index: caretNode.index}
    }
  }

  const tokens = tokenStream.getTokens()
  const token = tokens.find(token => isCaretAtToken(token, caretPosition))
  const tokenIndex = token ? token.tokenIndex+1 : -1
  return {caretNode, index: tokenIndex}
}


// TODO: use the `SymbolTableVisitor`
/**
 * Visit the parse tree to find the terminal node which holds the caret.
 */
const visit = (
  parseTree: ParseTree,
  caretPosition: CaretPosition
): TerminalNode | null => {
  if (parseTree instanceof TerminalNode) {
    const token = parseTree.symbol
    if (isCaretAtToken(token, caretPosition)) {
      return parseTree
    }
  } else {
    for (let i = 0; i < parseTree.childCount; i++) {
      const caretNode = visit(parseTree.getChild(i), caretPosition)
      if (caretNode) {
        return caretNode
      }
    }
  }

  return null
}

/**
 * Is the caret inside the token text?
 */
const isCaretAtToken = (token: Token, caret: CaretPosition): boolean => {
  const start = tokenBeginColumn(token)
  const end = tokenEndColumn(token)
  return (
    token.line == caret.line && start <= caret.column && end >= caret.column
  )
}

/**
 * Build the `TokenIndex` of a `Token`.
 */
const buildTokenIndex = (
  node: TerminalNode,
  caret: CaretPosition
): CaretNode => {
  const token = node.symbol
  return {
    index: token.tokenIndex,
    node,
    text: token.text?.substring(0, caret.column - tokenBeginColumn(token)) || ''
  }
}

/**
 * Check that the caret position is inside the parse tree.
 */
const checkCaretPosition = (
  parseTree: ParseTree,
  caret: CaretPosition
): void => {
  const display = (position: CaretPosition) =>
    `line ${position.line}, column ${position.column}`

  const { begin, end } = tokenPositions(parseTree)
  // if (
  //   begin.line > caret.line ||
  //   (begin.line == caret.line && begin.column > caret.column)
  // ) {
  //   // prettier-ignore
  //   throw new Error(`The caret position (${display(caret)}) is before the first token character (${display(begin)})`)
  // }

  if (end) {
    // NOTE: the end column must be ignored because the parser may have parsed
    // the valid part of an incomplete expression and stopped before its end.
    // Ie: in `{default =|` cannot parse more than `=` successfully.
    if (end.line < caret.line) {
      // prettier-ignore
      throw new Error(`The caret position (${display(caret)}) is after the last token character (${display(end)})`)
    }
  }
}

/**
 * Infers the column at which the token starts.
 * Normalizes `charPositionInLine`, which starts at 0, to start at 1, like `CaretPosition`
 */
const tokenBeginColumn = (token: Token): number => token.charPositionInLine + 1

/**
 * Infers the column at which the token ends.
 */
const tokenEndColumn = (token: Token): number =>
  tokenBeginColumn(token) + (token.text?.length || 0)

interface TokenPositions {
  begin: CaretPosition
  end?: CaretPosition
}

/**
 * Return the position of the first character of the token and the last character of the last token of the parse tree.
 *
 * NOTE: Assumes that tokens cannot span more than 1 line.
 */
const tokenPositions = (parseTree: ParseTree): TokenPositions => {
  if (parseTree instanceof TerminalNode) {
    const token = parseTree.symbol
    return {
      begin: { line: token.line, column: tokenBeginColumn(token) },
      end: {
        line: token.line,
        column: tokenEndColumn(token)
      }
    }
  } else {
    const { start, stop } = parseTree as ParserRuleContext

    const result: TokenPositions = {
      begin: {
        line: start.line,
        column: tokenBeginColumn(start)
      }
    }
    if (stop) {
      result.end = {
        line: stop.line,
        column: tokenEndColumn(stop)
      }
    }
    return result
  }
}
