import { inferCaretNode } from './positions'
import type { CaretPosition } from './positions'
import { dpmXl } from '!tests'
import { parse } from '@/test/'

const suite = (label: string, code: string, index: number, text: string) => {
  const { parseTree, caretPosition, tokenStream } = parse(dpmXl, code)
  const {caretNode, index: tokenIndex} = inferCaretNode(parseTree, caretPosition, tokenStream)

  return describe(label, () => {
    it('has the correct index', () => {
      expect(tokenIndex).toEqual(index)
    })

    it('includes the text until the caret', () => {
      expect(caretNode?.text).toEqual(text)
    })
  })
}

const suiteCaretError = (
  label: string,
  caretPosition: CaretPosition,
  limit: 'first' | 'last'
) => {
  const code = '{tC23, interval = |'
  const { parseTree, tokenStream } = parse(dpmXl, code)
  const regexp = new RegExp(`caret.*${limit}`)

  return describe(label, () => {
    it('throws an `Error`', () => {
      expect(() => {
        inferCaretNode(parseTree, caretPosition, tokenStream)
      }).toThrowError(regexp)
    })
  })
}

const suiteIgnoreCaretColumn = (
  label: string,
  caretPosition: CaretPosition
) => {
  const code = '{tC23, interval = |'
  const { parseTree, tokenStream } = parse(dpmXl, code)

  return describe(label, () => {
    it('does not throw an `Error`', () => {
      expect(() => {
        inferCaretNode(parseTree, caretPosition, tokenStream)
      }).not.toThrowError()
    })
  })
}

// prettier-ignore
describe('#inferCaretNode', () => {
  suite('valid expression', `{tS.0|2.01, r0800, c0010} = {tS.02.02, r0150, c0020};`, 2, 'tS.0')
  suite('empty expression', `|`, 1, '')
  suite('before first expression', `|{tS.02.02, r0150, c0020};`, 1, '')
  suite('spaces before ', `{tS.02.02, |};`, 4, '')
  suite('spaces before and after ', `{tS.02.02,  |    };`, 4, ',')
  // FIXME
  // suite('invalid expression (cursor at end)', `with {c0010, |`, 6, '')
  // suite('invalid expression (cursor inside)', `{tS.0|2.01, r0800, c0010`, 1, 'tS.0')

  suiteCaretError('when the caret is in a line after the expression', { line: 4, column: 1 }, 'last')

  suiteIgnoreCaretColumn('when the caret is in the same line than the expression, but after the last column', { line: 1, column: 100 })
})
