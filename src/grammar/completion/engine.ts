import {
  CandidatesCollection,
  CodeCompletionCore,
  SymbolTable
} from 'antlr4-c3'
import { CaretNode, inferCaretNode } from './positions'
import type { CaretPosition } from './positions'
import { Parser, CommonTokenStream } from 'antlr4ts'
import { ParseTreeVisitor, ParseTree, RuleNode } from 'antlr4ts/tree'
// import { difference } from '@/utils'
import { Token } from '../token'
import { Rule } from '../rule'
import type { TokenType } from '../token'
import type { RuleType } from '../rule'
import { languages } from 'monaco-editor'

type Variant = Token | Rule
type VariantType = TokenType | RuleType

export interface Suggestion<T extends Variant = Variant> {
  context: T
  text: string
  /**
   * Lowest priority is 0.
   */
  priority: number
  /**
   * Additional data to pass to the `CompletionItemProvider`.
   */
  completionItem?: Partial<languages.CompletionItem>
}

type SuggestionHandlerResult<T extends Variant = Variant> =
  | Suggestion<T>
  | Suggestion<T>[]
  | null

/**
 * Function to ignore or customise the suggestion.
 */
export type SuggestionHandler<T extends Variant = Variant> = (
  suggestion: Suggestion<T>,
  symbolTable: SymbolTable,
  caret: CaretNode
) => SuggestionHandlerResult<T> | Promise<SuggestionHandlerResult<T>>

interface Config<H extends Variant, T extends VariantType> {
  type: T
  /**
   * Lowest priority is 0.
   */
  priority?: number
  handler?: SuggestionHandler<H>
}

export type TokenConfig = Config<Token, TokenType>
export type RuleConfig = Config<Rule, RuleType>

/**
 * Options to configure the C3 engine.
 */
export type C3Options = Partial<
  Pick<
    CodeCompletionCore,
    | 'showResult'
    | 'showDebugOutput'
    | 'debugOutputWithTransitions'
    | 'showRuleStack'
    | 'translateRulesTopDown'
  >
>

/**
 * Options of the `CompletionEngine`.
 */
export interface Options {
  c3?: C3Options
  /**
   * Default function that decide which suggestions are included.
   */
  defaultHandler?: SuggestionHandler<Variant>
  /**
   * Tokens to consider for suggestions.
   */
  tokens?: TokenConfig[]
  /**
   * Rules to consider for suggestions.
   */
  rules?: RuleConfig[]
}

export class CompletionEngine {
  private options!: Required<Options>
  private c3!: CodeCompletionCore
  private caretNode!: CaretNode | null
  private parseTree!: ParseTree
  private symbolTable!: SymbolTable

  public readonly defaultOptions: Required<Options> = {
    c3: {},
    defaultHandler: (_suggestion, _symbolTable, _caretNode) => null,
    tokens: [],
    rules: []
  }

  constructor(
    private parser: Parser,
    private visitor: ParseTreeVisitor<SymbolTable>,
    options: Options = {}
  ) {
    this.options = {
      ...this.defaultOptions,
      ...options
    }

    this.c3 = new CodeCompletionCore(this.parser)

    for (const key in this.options.c3) {
      const option = key as keyof C3Options
      if (this.options.c3[option]) {
        this.c3[option] = this.options.c3[option] as boolean
      }
    }
  }

  async suggest(
    parseTree: ParseTree,
    caret: CaretPosition,
    tokenStream: CommonTokenStream
  ): Promise<Suggestion[]> {
    this.parseTree = parseTree
    const {caretNode, index} = inferCaretNode(this.parseTree, caret, tokenStream)
    this.caretNode = caretNode
    if (!this.caretNode) {
      return []
    }

    // TODO
    const node = this.caretNode.node.parent!.parent as RuleNode

    if(node){
      this.symbolTable = this.visitor.visit(parseTree)
    }

    // FIXME
    // this.c3.ignoredTokens = this.ignoredTokens
    this.c3.preferredRules = this.preferredRules

    const candidates = this.c3.collectCandidates(index)

    const suggestions = await Promise.all([
      this.suggestTokens(candidates),
      this.suggestRules(candidates)
    ])
    // TODO: sort by priority
    return [...suggestions[0], ...suggestions[1]]
  }

  // TODO: move to grammar, so it can be used for other purposes
  // TODO: return `Token`
  // private get allTokens(): Set<TokenType> {
  //   const allTokens: TokenType[] = []
  //   for (let i = 1; i <= this.parser.vocabulary.maxTokenType; i++) {
  //     allTokens.push(i)
  //   }
  //   return new Set(allTokens)
  // }

  // private get ignoredTokens(): Set<TokenType> {
  //   const includedTokens = this.options.tokens.map(token => token.type)
  //   return difference(this.allTokens, new Set(includedTokens))
  // }

  private get preferredRules(): Set<RuleType> {
    const rules = this.options.rules.map(rule => rule.type)
    return new Set(rules)
  }

  private suggestTokens(
    candidates: CandidatesCollection
  ): Promise<Suggestion<Token>[]> {
    const { tokens: configs } = this.options

    return this.suggestCandidates<Token, TokenType>(
      candidates.tokens,
      (tokenType: TokenType) => ({
        config: configs.find(({ type }) => type === tokenType) || null,
        context: Token.from(this.parser, tokenType)
      })
    )
  }

  private suggestRules(
    candidates: CandidatesCollection
  ): Promise<Suggestion<Rule>[]> {
    const { rules: configs } = this.options

    return this.suggestCandidates<Rule, RuleType>(
      candidates.rules,
      (ruleType: RuleType) => ({
        config: configs.find(({ type }) => type === ruleType) || null,
        context: Rule.from(this.parser, ruleType)
      })
    )
  }

  private async suggestCandidates<T extends Variant, Ty extends VariantType>(
    candidates: Map<Ty, any>,
    find: (type: Ty) => { context: T; config: Config<T, Ty> | null }
  ): Promise<Suggestion<T>[]> {
    const { defaultHandler } = this.options
    let suggestions: Suggestion<T>[] = []

    for (const [type, _] of candidates) {
      const { context, config } = find(type)
      const handler = config?.handler ?? defaultHandler

      const suggestion = {
        context,
        text: this.caretNode!.text.trim(),
        priority: config?.priority ?? 0
      }

      const candidateSuggestions = [
        (await handler(suggestion, this.symbolTable, this.caretNode!)) || []
      ].flat() as Suggestion<T>[]

      if (candidateSuggestions.length) {
        suggestions = [...suggestions, ...candidateSuggestions]
      }
    }

    return suggestions
  }
}
