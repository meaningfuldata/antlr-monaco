import { suggest } from '@/test/'
import { dpmXl } from '!tests'
import { DpmXlLexer } from '!examples'
import type { Options } from './engine'

// TODO: refactor to reduce duplication of tests that check incomplete expressions
// TODO: refactor to reduce duplication of tests that check partial matches

describe('CompletionEngine', () => {
  describe('tokens', () => {
    describe('in incomplete expression', () => {
      it.skip('are suggested', async () => {
        const { length, texts } = await suggest(dpmXl, `with {c0010, |`)

        expect(length).toEqual(12)
        expect(texts.indexOf('default')).toBeGreaterThan(-1)
      })

      // FIXME: should match only `default` or tokens that could start by "def"
      it.skip('are suggested with partial match', async () => {
        const { length, texts } = await suggest(dpmXl, `with {c0010, def|`)

        expect(length).toEqual(1)
        expect(texts.indexOf('default')).toBe(0)
      })
    })
  })

  describe.skip('rules', () => {
    describe('in incomplete expression', () => {
      it('are suggested', async () => {
        const { length } = await suggest(dpmXl, `with {r|`)

        expect(length).toEqual(1)

        // const tokenTypes = suggestions.map(({ tokenType }) => tokenType)
        // expect(tokenTypes.indexOf(DpmXlLexer.TABLE_CODE)).toBeGreaterThan(-1)
      })

      // it('are suggested 2', () => {
      //   const { length } = suggest(dpmXl, `with {r 0|`)
      //   expect(length).toEqual(1)
      // })
      //
      // it('are suggested 3', () => {
      //   const { length } = suggest(dpmXl, `with {c 0|`)
      //   expect(length).toEqual(1)
      // })

      // it('are suggested 4', () => {
      //   const { length } = suggest(dpmXl, `with {r01-0|`)
      //   expect(length).toEqual(1)
      // })

      it('are suggested 5', async () => {
        const { length } = await suggest(dpmXl, `with {c0}: {r01} = {r0|2}`)
        expect(length).toEqual(1)
      })
    })
  })

  // describe('priority', () => {
  //   it('default priority is 0', () => {
  //   })
  //
  //   it('is set from the config', () => {
  //   })
  //
  //   it('suggestions are sorted by priority', () => {
  //   })
  // })

  // describe('no token handler', () => {
  //   it('uses the internal default handler', () => {})
  // })

  describe.skip('default token handler', () => {
    // FIXME: should match only `default` or tokens that could start by "def"
    it('is applied to all token candidates', async () => {
      const options: Options = {
        defaultHandler: (suggestion, _caretNode) => ({
          ...suggestion,
          text: `SUGGESTION-${suggestion.text}`
        })
      }

      const { length, texts } = await suggest(
        dpmXl,
        `with {c0010, def|`,
        options
      )

      expect(length).toEqual(1)
      expect(texts.indexOf('SUGGESTION-default')).toBe(0)
    })
  })

  describe('per token handler', () => {
    // FIXME: should match only `default` or tokens that could start by "def"
    it.skip('is applied to only to specific token candidates', async () => {
      const options: Options = {
        tokens: [
          {
            type: DpmXlLexer.DEFAULT,
            handler: (suggestion, _caretNode) => ({
              ...suggestion,
              text: `SUGGESTION-${suggestion.text}`
            })
          }
        ]
      }

      const { length, texts } = await suggest(
        dpmXl,
        `with {c0010, def|`,
        options
      )

      expect(length).toEqual(1)
      expect(texts.indexOf('SUGGESTION-default')).toBe(0)
    })
  })

  // describe('ignored tokens', () => {})

  // describe('no rule handler', () => {})
  //
  // describe('default rule handler', () => {})
  //
  // describe('per rule handler', () => {})

  // describe('ignored rules', () => {})
})
