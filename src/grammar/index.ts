import { Grammar } from './grammar'
import type { Options as GrammarOptions } from './grammar'
import { ErrorListener } from './error-listener'
import type { ErrorListenerItem } from './error-listener'
import { Token } from './token'
import { CompletionEngine } from './completion/engine'
import type { CaretPosition, CaretNode } from './completion/positions'
import type {
  Options as CompletionOptions,
  Suggestion as CompletionSuggestion,
  RuleConfig as CompletionRuleConfig,
  TokenConfig as CompletionTokenConfig
} from './completion/engine'

export { Grammar, CompletionEngine, ErrorListener, Token }

export type {
  CaretPosition,
  CaretNode,
  CompletionOptions,
  CompletionSuggestion,
  CompletionRuleConfig,
  CompletionTokenConfig,
  ErrorListenerItem,
  GrammarOptions
}
