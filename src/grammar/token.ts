import { Lexer, Parser } from 'antlr4ts'

/**
 * Alias to `number` that states more clearly that that a number is used as the token type.
 */
export type TokenType = number

export class Token {
  static from<T extends Lexer | Parser>(
    { vocabulary }: T,
    tokenType: TokenType
  ): Token {
    const name = vocabulary.getSymbolicName(tokenType)
    return new Token(tokenType, name)
  }

  constructor(public type: TokenType, public name?: string) {}
}
