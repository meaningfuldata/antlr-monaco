export const difference = <T>(a: Set<T>, b: Set<T>): Set<T> => {
  const result = new Set(a)
  for (const element of b) {
    result.delete(element)
  }
  return result
}
